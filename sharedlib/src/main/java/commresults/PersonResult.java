package commresults;

import model.Person;

/**
 * A result class that contains the result of a Person service request.
 *
 * Contains a boolean that indicates whether the request was successful or not.
 *
 * If the request was successful, it contains a list of all the Persons associated with the ID contained in the request.
 * If the request was not successful, it contains an error message.
 */
public class PersonResult
{
    Person[] data;
    String message;

    public Person[] getData() {
        return data;
    }

    public void setData(Person[] data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
