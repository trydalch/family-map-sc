package commresults;

import model.Event;

/**
 * A result class that contains the result of a Event service request.
 *
 * Contains a boolean that indicates whether the request was successful or not.
 *
 * If the request was successful, it contains a list of all the Events associated with the ID contained in the request.
 * If the request was not successful, it contains an error message.
 */
public class EventResult
{
    Event[] data;
    String message;

    public Event[] getData() {
        return data;
    }

    public void setData(Event[] data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
