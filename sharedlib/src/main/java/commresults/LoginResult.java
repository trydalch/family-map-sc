package commresults;

/**
 * A result class that contains the result of a Login service request.
 *
 * Contains a boolean that indicates whether the request was successful or not, and a String
 * with a message if the request failed.
 */
public class LoginResult
{
    private String authToken;
    private String userName;
    private String personId;
    String message;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }

    public String getPersonId() { return personId; }

    public void setPersonId(String personId) { this.personId = personId; }

    public String getMessage() {
        return message;
    }

    public void setMessage(String m_sError) {
        this.message = m_sError;
    }

}
