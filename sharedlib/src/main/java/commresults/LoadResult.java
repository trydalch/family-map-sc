package commresults;

/**
 * A result class that contains the result of a Load service request.
 *
 * Contains a boolean that indicates whether the request was successful or not, and a String
 * with a message if the request failed.
 */
public class LoadResult
{
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
