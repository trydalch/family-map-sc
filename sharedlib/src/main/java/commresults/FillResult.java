package commresults;

/**
 * A result class that contains the result of a Fill service request.
 *
 * Contains a boolean that indicates whether the request was successful or not, and a String
 * with a message if the request failed.
 */
public class FillResult
{
    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
