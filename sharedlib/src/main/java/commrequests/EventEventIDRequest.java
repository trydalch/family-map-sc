package commrequests;

/**
 * A Request class used by the EventEventIDHandler and the EventEventIDService that
 * contains the ID of a specific event.
 */

public class EventEventIDRequest
{
    String authToken;
    String eventID;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }
}
