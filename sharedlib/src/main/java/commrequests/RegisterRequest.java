package commrequests;

import model.FemaleNameData;
import model.LastNameData;
import model.LocationData;
import model.MaleNameData;

/**
 * A Request class used by the RegisterHandler and RegisterService that contains information
 * needed to create a user.
 */
public class RegisterRequest
{
    String userName;
    String password;
    String email;
    String firstName;
    String lastName;
    String gender;

    FemaleNameData m_FNames = new FemaleNameData();
    MaleNameData m_MNames = new MaleNameData();
    LastNameData m_LNames = new LastNameData();
    LocationData m_Locations = new LocationData();
//    EventTypeData m_EventTypes = new EventTypeData();

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public FemaleNameData getFNames() {
        return m_FNames;
    }

    public void setFNames(FemaleNameData m_FNames) {
        this.m_FNames = m_FNames;
    }

    public MaleNameData getMNames() {
        return m_MNames;
    }

    public void setMNames(MaleNameData m_MNames) {
        this.m_MNames = m_MNames;
    }

    public LastNameData getLNames() {
        return m_LNames;
    }

    public void setLNames(LastNameData m_LNames) {
        this.m_LNames = m_LNames;
    }

    public LocationData getLocations() {
        return m_Locations;
    }

    public void setLocations(LocationData m_Locations) {
        this.m_Locations = m_Locations;
    }

//    public EventTypeData getEventTypes() {
//        return m_EventTypes;
//    }
//
//    public void setEventTypes(EventTypeData m_EventTypes) {
//        this.m_EventTypes = m_EventTypes;
//    }
}
