package commrequests;

import model.FemaleNameData;
import model.LastNameData;
import model.LocationData;
import model.MaleNameData;


/**
 * A Request class used by the FillHandler and the FillService that is used to generate generations
 * of data for a specified user.
 */

public class FillRequest
{
    String m_sUserName;
    int m_iGenerations;
    FemaleNameData m_FNames = new FemaleNameData();
    MaleNameData m_MNames = new MaleNameData();
    LastNameData m_LNames = new LastNameData();
    LocationData m_Locations = new LocationData();
//    EventTypeData m_EventTypes = new EventTypeData();

    public String getUserName() {
        return m_sUserName;
    }

    public void setUserName(String m_sUserName) {
        this.m_sUserName = m_sUserName;
    }

    public int getGenerations() {
        return m_iGenerations;
    }

    public void setGenerations(int m_iGenerations) {
        this.m_iGenerations = m_iGenerations;
    }

    public FemaleNameData getFNames() {
        return m_FNames;
    }

    public void setFNames(FemaleNameData m_FNames) {
        this.m_FNames = m_FNames;
    }

    public MaleNameData getMNames() {
        return m_MNames;
    }

    public void setMNames(MaleNameData m_MNames) {
        this.m_MNames = m_MNames;
    }

    public LastNameData getLNames() {
        return m_LNames;
    }

    public void setLNames(LastNameData m_LNames) {
        this.m_LNames = m_LNames;
    }

    public LocationData getLocations() {
        return m_Locations;
    }

    public void setLocations(LocationData m_Locations) {
        this.m_Locations = m_Locations;
    }

//    public EventTypeData getEventTypes() {
//        return m_EventTypes;
//    }
//
//    public void setEventTypes(EventTypeData m_EventTypes) {
//        this.m_EventTypes = m_EventTypes;
//    }
}
