package commrequests;

/**
 * A Request class used by the LoginHandler and LoginService that contains information needed to
 * login to the server.
 */

public class LoginRequest
{
    String userName;
    String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LoginRequest() {};
}
