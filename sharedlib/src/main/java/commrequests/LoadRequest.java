package commrequests;

import model.Event;
import model.Person;
import model.User;

/**
 * A Request class used by the LoadHandler and LoadService that represents the request to
 * clear the database and load the contained user, person, and event data into the database.
 */

public class LoadRequest
{
    User[] users;
    Person[] persons;
    Event[] events;

    public User[] getUsers() {
        return users;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }

    public Person[] getPersons() {
        return persons;
    }

    public void setPersons(Person[] persons) {
        this.persons = persons;
    }

    public Event[] getEvents() {
        return events;
    }

    public void setEvents(Event[] events) {
        this.events = events;
    }
}
