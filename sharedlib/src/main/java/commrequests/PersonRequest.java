package commrequests;

/**
 * A Request class used by the PersonHandler and PersonService that contains an AuthToken needed to find all people associated with a desired Person.
 */
public class PersonRequest
{
    String authToken;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
