package commrequests;

/**
 * A Request class used by the ClearHandler and ClearService used to clear the database of all
 * records.
 */

public class ClearRequest
{
    String usrName;

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }
}
