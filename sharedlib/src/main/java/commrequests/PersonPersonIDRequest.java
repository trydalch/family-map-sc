package commrequests;

/**
 * A Request class used by the PersonPersonIDHandler and PersonPersonIDService that contains
 * the ID of a specific Person.
 */

public class PersonPersonIDRequest
{
    String personID;
    String authToken;

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
