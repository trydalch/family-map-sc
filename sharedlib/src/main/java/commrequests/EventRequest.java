package commrequests;

/**
 * A Request class used by the EventHandler and the EventService that contains the ID necessary to
 * find all events relevant to a specific person.
 */

public class EventRequest
{
    String authToken;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
