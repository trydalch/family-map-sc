package model;

import java.util.UUID;

/**
 * An Authorization Token consisting of a unique AuthTok String
 * <pre>
 *     <b>Domain</b>
 *          m_sAuthTok  : String
 *          m_sPersondId : String
 * </pre>
 */
public class AuthToken
{
    /**
     * Default constructor for an AuthToken object
     */
    public AuthToken()
    {
        m_iTimeCreated = System.currentTimeMillis();
    }
    private String m_sAuthTok;
    private String m_sPersondId;
    private long m_iTimeCreated;

    public String getAuthId() {
        return m_sAuthTok;
    }

    public void setAuthId(String token) {
        this.m_sAuthTok = token;
    }

    public String getPersId() { return m_sPersondId; }

    public void setPersId(String m_sUserName) { this.m_sPersondId = m_sUserName; }

    public long getTimeCreated() {
        return m_iTimeCreated;
    }

    public void setTimeCreated(long m_iTimeCreated) {
        this.m_iTimeCreated = m_iTimeCreated;
    }

    public void genRandomAuthToken()
    {
        m_sAuthTok = UUID.randomUUID().toString();
    }

}
