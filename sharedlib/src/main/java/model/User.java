package model;

import java.util.UUID;

/**
 * A User object consisting of a username, password, email, first name, last name, gender, and unique personID
 * <pre>
 *     <b>Domain</b>
 *          userName : String
 *          password : String
 *          email : String
 *          firstName : String
 *          lastName : String
 *          gender : String
 *          personID : String
 * </pre>
 */

public class User
{

    /**
     * Default empty constructor for a User w/o initialized members
     */
    public User() {}

    /**
     * The constructor for a new User.
     * @param uname unique username
     * @param pword password
     * @param email unique email
     * @param fname first name of user
     * @param lname last name of user
     */
    public User(String uname, String pword, String email, String fname, String lname)
    {
        this.userName = uname;
        this.password = pword;
        this.email = email;
        this.firstName = fname;
        this.lastName = lname;
    }
    public User(String uname, String pword, String email, String fname, String lname, String gender, String personID)
    {
        this(uname, pword, email, fname, lname, gender);
        this.personID = personID;
    }
    public User(String uname, String pword, String email, String fname, String lname, String gender)
    {
        this(uname, pword, email, fname, lname);
        this.gender = gender;
        genRandomID();
    }

    private String userName;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private String gender;
    private String personID;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String m_sUserName) {
        this.userName = m_sUserName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String m_sPassword) {
        this.password = m_sPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }


    public void genRandomID()
    {
        personID = UUID.randomUUID().toString();
    }
}
