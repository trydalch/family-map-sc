package model;

/**
 * Created by Trevor.Rydalch on 6/1/2017.
 */

public class LastNameData
{
    String[] data;

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) {
        this.data = data;
    }
}
