package model;

import java.util.HashSet;
import java.util.Random;
import java.util.logging.Logger;


/**
 * Created by trevor.rydalch on 6/2/17.
 */

public class FamilyTreeGeneration
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }


    public HashSet<Person> generateTree(FemaleNameData fNames, MaleNameData mNames, LastNameData lNames, int generationsToGenerate, Person person, String usrName)
    {
//        logger.info("Generating tree for " + generationsToGenerate + " generations");
        HashSet<Person> persons = new HashSet<>();
        if (person.getGeneration() == 0)
        {
            persons.add(person);
        }
        if (person.getGeneration() < generationsToGenerate)
        {
            int nextGen = person.getGeneration()+ 1;
            // generate father
            Person father = generateFather(mNames, lNames, nextGen, usrName);
            persons.add(father);
            persons.addAll(generateTree(fNames, mNames, lNames, generationsToGenerate, father, usrName));

            // generate mother
            Person mother = generateMother(fNames, lNames, nextGen, usrName);
            persons.add(mother);
            persons.addAll(generateTree(fNames, mNames, lNames, generationsToGenerate, mother, usrName));

            // add as parent of current person
            person.setFather(father.getPersonID());
            person.setMother(mother.getPersonID());

            // add each as spouse of other
            father.setSpouse(mother.getPersonID());
            mother.setSpouse(father.getPersonID());
        }
        return persons;
    }

    public HashSet<Event> generateEvents(HashSet<Person> persons, LocationData locations)
    {
//        logger.info("Generating events");
        HashSet<Event> events =  new HashSet<>();
        final int iBaptismAge = 8;
        final int iMarriageAge = 21;
        final int iDeathAge = 80;
        for (Person p : persons)
        {
            int iGen = p.getGeneration();
            int birthYear = genBirthYear(iGen);
            Event birth = genEvent(birthYear, "Birth", p, locations);
            Event christening = genEvent(birthYear, "Christening", p, locations);
            Event baptism = genEvent(birthYear + iBaptismAge, "Baptism", p, locations);

            if (p.getGeneration() > 0)
            {
                Event death = genEvent(birthYear + iDeathAge, "Death", p, locations);
                events.add(death);

//                logger.info("Person Spouse ID = " + p.getSpouse());
                Person spouse = findSpouse(persons, p);
                assert spouse.getFirstName() != null;
                if (!marriageEventExists(events, spouse)) {
                    Event marriage = genEvent(birthYear + iMarriageAge, "Marriage", p, locations);
                    Event marriageSpouse = genEvent(birthYear + iMarriageAge, "Marriage", spouse, locations);
                    marriageSpouse.setLatitude(marriage.getLatitude());
                    marriageSpouse.setLongitude(marriage.getLongitude());
                    marriageSpouse.setCity(marriage.getCity());
                    marriageSpouse.setCountry(marriage.getCountry());

                    events.add(marriage);
                    events.add(marriageSpouse);
                }
            }
            events.add(birth);
            events.add(christening);
            events.add(baptism);
            if (birth.getDescendant() == null || christening.getDescendant() == null || baptism.getDescendant() == null)
            {
                int newInt = 0;
            }
        }
        return events;
    }

    public Person genRandomRootPerson(FemaleNameData fNames, MaleNameData mNames, LastNameData lNames, String usrName)
    {
        Person person = new Person();
        Random numberGenerator = new Random();
        if (numberGenerator.nextInt() % 2 == 0)
        {
            person = generateFather(mNames, lNames, 0, usrName);
        }
        else
        {
            person = generateMother(fNames, lNames, 0, usrName);
        }
        return person;
    }

    private Person findSpouse(HashSet<Person> persons, Person pers)
    {
        int i = 0;
        for (Person potentialSpouse : persons)
        {
//            logger.info("i = " + i);
//            i++;
            if (potentialSpouse.getGeneration() == pers.getGeneration())
            {
                if (potentialSpouse.getSpouse().equals(pers.getPersonID()))
                {
//                    logger.info("Found spouse");
                    Person spouse = potentialSpouse;
                    return spouse;
                }
            }
        }
        return new Person();
    }

    private boolean marriageEventExists(HashSet<Event> events, Person spouse)
    {
        for (Event possibleMarriage : events)
        {
            if (possibleMarriage.getEventType().equals("Marriage") && possibleMarriage.getPersonID().equals(spouse.getPersonID()))
            {
                return true;
            }
        }
        return false;
    }

    private int genBirthYear(int iGen)
    {
        final int iBaseYear = 1993;
        int generationGap = 25;
        int birthYear = iBaseYear - ((iGen +1) * generationGap);
        return birthYear;
    }

    private Event genEvent(int year, String type, Person pers, LocationData locations)
    {
        Event event = new Event();
        event.setYear(year);
        event.setEventType(type);
        event.genRandomID();
        event.setPersonID(pers.getPersonID());
        if (pers.getDescendant() != null) {
            event.setDescendant(pers.getDescendant());
        }


        Location location = selectRandomLocation(locations);

        event.setCity(location.getCity());
        event.setCountry(location.getCountry());
        event.setLatitude(Double.parseDouble(location.getLatitude()));
        event.setLongitude(Double.parseDouble(location.getLongitude()));

        return event;
    }

    private Location selectRandomLocation(LocationData locations)
    {
        Location loc = new Location();
        Random numberGenerator = new Random();
        loc = locations.data[Math.abs(numberGenerator.nextInt() % locations.data.length)];
        return loc;
    }

    private Person generateFather(MaleNameData mNames, LastNameData lNames, int generation, String descendant)
    {
        Random numberGenerator = new Random();
        Person father = new Person();
        int index = Math.abs(numberGenerator.nextInt() % mNames.getData().length);
        father.setFirstName(mNames.getData()[index]);
        index = Math.abs(numberGenerator.nextInt() % lNames.getData().length);
        father.setLastName(lNames.getData()[index]);
        father.genRandomID();
        father.setGender("m");
        father.setDescendant(descendant);
        father.setGeneration(generation);
        return father;
    }

    private Person generateMother(FemaleNameData fNames, LastNameData lNames, int generation, String descendant)
    {
        Random numberGenerator = new Random();
        Person mother = new Person();
        mother.setFirstName(fNames.getData()[Math.abs(numberGenerator.nextInt() % fNames.getData().length)]);
        mother.setLastName(lNames.getData()[Math.abs(numberGenerator.nextInt() % lNames.getData().length)]);
        mother.genRandomID();
        mother.setGender("f");
        mother.setDescendant(descendant);
        mother.setGeneration(generation);
        return mother;
    }
}
