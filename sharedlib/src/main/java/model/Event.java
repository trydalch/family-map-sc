package model;

import java.util.UUID;

/**
 * An Event consisting of a unique eventID, the ID of the Descendant, the ID of the person to whom the event pertains,
 * the latitude and longitude coordinates, city, and country where the event happened, and the event type.
 * <pre>
 *     <b>Domain</b>
 *          eventID : String
 *          descendant : String
 *          personID : String
 *          latitude : double
 *          longitude : double
 *          country : String
 *          city : String
 *          eventType : String
 * </pre>
 */
public class Event
{
    private String eventID;
    private String descendant;
    private String personID;
    private double latitude;
    private double longitude;
    private String country;
    private String city;
    private String eventType;
    private int year;

    public String getEventID() { return eventID; }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getDescendant() {
        return descendant;
    }

    public void setDescendant(String descendant) {
        this.descendant = descendant;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int Year) {
        this.year = Year;
    }

    public void genRandomID()
    {
        eventID = UUID.randomUUID().toString();
    }

}
