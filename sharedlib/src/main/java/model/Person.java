package model;

import java.util.UUID;

/**
 *A Person object consisting of a unique ID, the ID of their Descendant,
 * first name, last name, gender, IDs for their father, mother, and spouse
 * <pre>
 *     <b>Domain</b>
 *          personID : String
 *          descendant : String
 *          firstName : String
 *          lastName : String
 *          gender : String
 *          father : String
 *          mother : String
 *          spouse : String
 *
 * </pre>
 */
public class Person
{
    private String personID;
    private String descendant;
    private String firstName;
    private String lastName;
    private String gender;
    private String father;
    private String mother;
    private String spouse;
    private int m_iGeneration;
    private String m_relation;

    /**
     * The constructor for a Person
     * <pre>
     *     <b>Constraints on the input</b>:
     *     invariant(personID, fname, lname)
     *
     *     <b>Result:</b>"
     *     this.personID = personID AND this.firstName = fname AND this.lastName = lname
     * </pre>
     * @param personID the unique ID for the new person
     * @param fname the person's first name
     * @param lname the person's last name
     */
    public Person(String personID, String fname, String lname)
    {
        this.personID = personID;
        firstName = fname;
        lastName = lname;
    }

    public Person() {}

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getDescendant() {
        return descendant;
    }

    public void setDescendant(String descendant) {
        this.descendant = descendant;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public int getGeneration() {
        return m_iGeneration;
    }

    public void setGeneration(int m_iGeneration) {
        this.m_iGeneration = m_iGeneration;
    }

    public void genRandomID()
    {
        personID = UUID.randomUUID().toString();
    }

    public String getRelation() {
        return m_relation;
    }

    public void setRelation(String relation) {
        m_relation = relation;
    }
}
