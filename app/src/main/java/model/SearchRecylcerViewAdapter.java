package model;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.trevorrydalch.familymap.R;

import ui.MapActivity;
import ui.PersonActivity;
import ui.ViewHolder;

/**
 * Created by trevor.rydalch on 6/19/17.
 */

public class SearchRecylcerViewAdapter extends RecyclerView.Adapter<ViewHolder>
{
    private static final String SELECTED_PERSON = "PRSN";
    private static final String TAG_EVNT = "EVNT";
    Object[] m_dataSet;

    public SearchRecylcerViewAdapter(Object[] objects)
    {
        m_dataSet = objects;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_list_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (m_dataSet[position] instanceof Person)
        {
            final Person persToDisplay = (Person) m_dataSet[position];
            if (persToDisplay.getGender().toLowerCase().equals("m"))
            {
                holder.m_imageView.setImageResource(R.mipmap.ic_pers_male);
            }
            else
            {
                holder.m_imageView.setImageResource(R.mipmap.ic_pers_female);
            }
            holder.m_TopTextView.setText(persToDisplay.getFirstName() + " " + persToDisplay.getLastName());
            holder.m_BottomTextView.setText(persToDisplay.getRelation());
            holder.m_relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), PersonActivity.class);
                    intent.putExtra(SELECTED_PERSON, persToDisplay.getPersonID());
                    v.getContext().startActivity(intent);
                }
            });
        }
        else if (m_dataSet[position] instanceof Event)
        {
            final Event eventToDisplay = (Event) m_dataSet[position];
            holder.m_imageView.setImageResource(R.mipmap.ic_pin_event);
            holder.m_TopTextView.setText(eventToDisplay.getEventType() + ": " +
                    eventToDisplay.getCity() + ", " + eventToDisplay.getCountry() +
                    "(" + eventToDisplay.getYear() + ")");
            Person personOfEvent = Model.SINGLETON.getPersonFromID(eventToDisplay.getPersonID());
            holder.m_BottomTextView.setText(personOfEvent.getFirstName() + " " + personOfEvent.getLastName());
            holder.m_relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), MapActivity.class);
                    intent.putExtra(TAG_EVNT, eventToDisplay.getEventID());
                    v.getContext().startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return m_dataSet.length;
    }

    public void clear()
    {
        int size = m_dataSet.length;
        if (size > 0)
        {
            m_dataSet = new Object[0];
            notifyItemRangeRemoved(0, size);
        }
    }
}
