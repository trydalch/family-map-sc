package model;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.example.trevorrydalch.familymap.R;

import java.util.List;

import ui.MapActivity;
import ui.PersonActivity;
import ui.ViewHolder;

/**
 * This class is used in my PersonActivity view to provide an expandable list of people and events.
 */

public class ExpandableRecyclerViewAdapter extends ExpandableRecyclerAdapter <ui.ParentViewHolder, ViewHolder> {

    private LayoutInflater m_Inflater;

    private static final String SELECTED_PERSON = "PRSN";
    private static final String TAG_EVNT = "EVNT";

    public ExpandableRecyclerViewAdapter(Context context, List parentItemList) {
        super(context, parentItemList);
        m_Inflater = LayoutInflater.from(context);
    }

    @Override
    public ui.ParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        View view = m_Inflater.inflate(R.layout.person_parent_list_item, viewGroup, false);
        return new ui.ParentViewHolder(view);
    }

    @Override
    public ViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = m_Inflater.inflate(R.layout.child_list_item, viewGroup, false);
        return new ViewHolder(view);
}

    @Override
    public void onBindParentViewHolder(ui.ParentViewHolder parentViewHolder, int i, Object obj)
    {
        ParentObject parent = (ParentObject) obj;
        parentViewHolder.m_ExpandableTitle.setText(parent.getTitle());
    }

    @Override
    public void onBindChildViewHolder(ViewHolder viewHolder, int i, Object childObject) {
        if (childObject instanceof Person)
        {
            final Person persToDisplay = (Person) childObject;
            if (persToDisplay.getGender().toLowerCase().equals("m"))
            {
                viewHolder.m_imageView.setImageResource(R.mipmap.ic_pers_male);
            }
            else
            {
                viewHolder.m_imageView.setImageResource(R.mipmap.ic_pers_female);
            }
            viewHolder.m_TopTextView.setText(persToDisplay.getFirstName() + " " + persToDisplay.getLastName());
            viewHolder.m_BottomTextView.setText(persToDisplay.getRelation());
            viewHolder.m_relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), PersonActivity.class);
                    intent.putExtra(SELECTED_PERSON, persToDisplay.getPersonID());
                    v.getContext().startActivity(intent);
                }
            });
        }
        else if (childObject instanceof Event)
        {
            final Event eventToDisplay = (Event) childObject;
            viewHolder.m_imageView.setImageResource(R.mipmap.ic_pin_event);
            viewHolder.m_TopTextView.setText(eventToDisplay.getEventType() + ": " +
                    eventToDisplay.getCity() + ", " + eventToDisplay.getCountry() +
                    "(" + eventToDisplay.getYear() + ")");
            Person personOfEvent = Model.SINGLETON.getPersonFromID(eventToDisplay.getPersonID());
            viewHolder.m_BottomTextView.setText(personOfEvent.getFirstName() + " " + personOfEvent.getLastName());
            viewHolder.m_relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), MapActivity.class);
                    intent.putExtra(TAG_EVNT, eventToDisplay.getEventID());
                    v.getContext().startActivity(intent);
                }
            });
        }
    }

}
