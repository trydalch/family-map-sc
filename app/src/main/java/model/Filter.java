package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by trevor.rydalch on 6/8/17.
 */

public class Filter
{

    private boolean m_ShowPaternal;
    private boolean m_ShowMaternal;
    private boolean m_ShowMale;
    private boolean m_ShowFemale;
    private List<EventTypeData> m_typeFilters;

    public Filter()
    {
        m_ShowPaternal = true;
        m_ShowMaternal = true;
        m_ShowMale = true;
        m_ShowFemale = true;
        m_typeFilters = new ArrayList<>();
    }

    public boolean isShowPaternal() {
        return m_ShowPaternal;
    }

    public void setShowPaternal(boolean showPaternal) {
        m_ShowPaternal = showPaternal;
    }

    public boolean isShowMaternal() {
        return m_ShowMaternal;
    }

    public void setShowMaternal(boolean showMaternal) {
        m_ShowMaternal = showMaternal;
    }

    public boolean isShowMale() {
        return m_ShowMale;
    }

    public void setShowMale(boolean showMale) {
        m_ShowMale = showMale;
    }

    public boolean isShowFemale() {
        return m_ShowFemale;
    }

    public void setShowFemale(boolean showFemale) {
        m_ShowFemale = showFemale;
    }


    public List<EventTypeData> getTypeFilters() {
        return m_typeFilters;
    }

    public void setTypeFilters(List<EventTypeData> typeFilters) {
        m_typeFilters = typeFilters;
    }
}
