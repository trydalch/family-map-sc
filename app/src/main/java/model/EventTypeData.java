package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trevor.Rydalch on 6/1/2017.
 */

public class EventTypeData
{
    private String m_type;
    private boolean m_show;

    public EventTypeData(String type)
    {
        m_type = type;
        m_show = true;
    }

    public String getType() {
        return m_type;
    }

    public void setType(String type) {
        m_type = type;
    }

    public boolean isShow() {
        return m_show;
    }

    public void setShow(boolean show) {
        m_show = show;
    }
}
