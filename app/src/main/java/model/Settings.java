package model;

/**
 * Created by trevor.rydalch on 6/8/17.
 */

public class Settings
{

    public enum Color
    {
        GREEN, BLUE, RED, YELLOW, WHITE
    }

    public enum MapType
    {
        NORMAL, HYBRID, SATELLITE, TERRAIN
    }

    public Settings()
    {
        m_LifeStoryLines = Color.GREEN;
        m_SpouseLines = Color.RED;
        m_FamilyTreeColor = Color.BLUE;
        m_ShowFamTreeLines = true;
        m_ShowLifeStoryLines = true;
        m_ShowSpouseLines = true;
        m_mapType = MapType.NORMAL;
    }

    private Color m_LifeStoryLines;
    private Color m_SpouseLines;
    private Color m_FamilyTreeColor;
    private MapType m_mapType;

    private boolean m_ShowLifeStoryLines;
    private boolean m_ShowSpouseLines;
    private boolean m_ShowFamTreeLines;

    public Color getSpouseLinesColor() {
        return m_SpouseLines;
    }

    public void setSpouseLinesColor(Color spouseLines) {
        m_SpouseLines = spouseLines;
    }

    public boolean isShowSpouseLines() {
        return m_ShowSpouseLines;
    }

    public void setShowSpouseLines(boolean showSpouseLines) {
        m_ShowSpouseLines = showSpouseLines;
    }

    public Color getLifeStoryLinesColor() {
        return m_LifeStoryLines;
    }

    public void setLifeStoryLines(Color lifeStoryLines) {
        m_LifeStoryLines = lifeStoryLines;
    }

    public Color getFamilyTreeColor() {
        return m_FamilyTreeColor;
    }

    public void setFamilyTreeColor(Color familyTreeColor) {
        m_FamilyTreeColor = familyTreeColor;
    }

    public MapType getMapType() {
        return m_mapType;
    }

    public void setMapType(MapType mapType) {
        m_mapType = mapType;
    }

    public boolean isShowLifeStoryLines() {
        return m_ShowLifeStoryLines;
    }

    public void setShowLifeStoryLines(boolean showLifeStoryLines) {
        m_ShowLifeStoryLines = showLifeStoryLines;
    }

    public boolean isShowFamTreeLines() {
        return m_ShowFamTreeLines;
    }

    public void setShowFamTreeLines(boolean showFamTreeLines) {
        m_ShowFamTreeLines = showFamTreeLines;
    }
}
