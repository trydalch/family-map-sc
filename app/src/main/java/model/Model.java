package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by trevor.rydalch on 6/8/17.
 */

public class Model
{
    public static final Model SINGLETON = new Model();
    private String m_authToken;

    public static Model getInstance()
    {
        return SINGLETON;
    }

    // Contstructor used when instantiating the SINGLETON. Only called the one time so that all data is static.
    private Model()
    {
        m_people = new HashMap<>();
        m_events = new HashMap<>();
        m_personEvents = new HashMap<>();
        m_settings = new Settings();
        m_filter = new Filter();
        m_eventTypes = new ArrayList<>();
        m_eventTypeColors = new HashMap<>();
        m_user = new Person();
        m_userSpouse = new Person();
        m_paternalAncestors = new HashSet<>();
        m_maternalAncestors = new HashSet<>();
        m_personChildren = new HashMap<>();
        m_FilteredPersons = new TreeSet<>();
    }

    private Map<String, Person> m_people;   // Key is personID, list contains all people in the family tree
    private Map<String, Event> m_events;    // Key is eventID, list contains all events of people in the family tree
    private Map<String, List<Event>> m_personEvents; // Key is personID, list contains events for just that person.
    private Settings m_settings;            // Contains the current settings. Edited and saved in the Settings Activity and loaded in the mapFragment.
    private Filter m_filter;                // Contains the current filters. Edited and updated in the FilterActivity (and FilterViewHolder)
    private List<String> m_eventTypes;      // Contains strings of event types.
    private Map<String, Integer> m_eventTypeColors;
    private Person m_user;                  // The root person, the user signed in.

    private Person m_userSpouse;            // The spouse of the person signed in, if there is one.
    private Set<String> m_paternalAncestors;    // All paternal ancestors.
    private Set<String> m_maternalAncestors;    // All maternal ancestors.
    private Map<String, List<Person>> m_personChildren; //Children, organized by the id of the parent.
    private Set<String> m_FilteredPersons;      // The personIDs of the persons that meet the current filters.

    private String m_host;                  // The IP address of the host, entered in the LoginFragment
    private String m_port;                  // The Port on the host, entered in the LoginFragment.

    /*
    Returns a list of all the events.
     */
    public List<Event> getAllEvents()
    {
        List<Event> events = new ArrayList<>();
        for (Map.Entry<String, Event> entry : m_events.entrySet())
        {
            events.add(entry.getValue());
        }
        return events;
    }

    /*
    Returns a list of all the persons in the tree
     */
    public List<Person> getAllPersons()
    {
        List<Person> persons = new ArrayList<>();
        for (Map.Entry<String, Person> entry : m_people.entrySet())
        {
            persons.add(entry.getValue());
        }
        return persons;
    }


    /*
    Returns a the person object to whom the event belongs.
     */
    public Person getPersonFromEvent(Event event)
    {
        for (Map.Entry<String, Person> entry : m_people.entrySet())
        {
            if (entry.getValue().getPersonID().equals(event.getPersonID()))
            {
                return entry.getValue();
            }
        }
        return null;
    }

    /*
    Gets a desired event for a specified person.
     */
    public Event getEventFromPerson(Person person, String type)
    {
        for (Map.Entry<String, Event> entry : m_events.entrySet())
        {
            if (entry.getValue().getPersonID().equals(person.getPersonID()) && entry.getValue().getEventType().equals(type))
            {
                return entry.getValue();
            }
        }
        return null;
    }

    /*
    Gets a person object from their unique id
     */
    public Person getPersonFromID(String id)
    {
        return m_people.get(id);
    }

    public Person getUser() {
        return m_user;
    }

    public void setUser(Person user) {
        this.m_user = user;
    }

    /*
    Parses an array of Person objects and populates datamembers with data.

    Preliminary sorting of the user and the spouse into their individual data members

    Calls PartitionPaternalMaternalAncestors which stores soft copies of the persons on each side of the family
     */
    public void addPersons(Person[] data)
    {
        for (Person p : data)
        {
            m_people.put(p.getPersonID(), p);
            if (p.getPersonID().equals(m_user.getPersonID()))
            {
                m_user = p;
            }
            else if (p.getPersonID().equals(m_user.getSpouse()))
            {
                m_userSpouse = p;
            }
        }
        PartitionPaternalMaternalAncestors();
    }

    /*
        This funciton stores soft copies of the persons on each side of the family.

        After finding both the mother and father, it calls other private methods that partition each side.
     */
    private void PartitionPaternalMaternalAncestors()
    {
        assert m_user != null;
        Person father = new Person();
        Person mother = new Person();
        for (Map.Entry<String, Person> entry : m_people.entrySet())
        {
            if (entry.getValue().getPersonID().equals(m_user.getFather()))
            {
                father = entry.getValue();
                m_paternalAncestors.add(father.getPersonID());
            }
            if (entry.getValue().getPersonID().equals(m_user.getMother()))
            {
                mother = entry.getValue();
                m_maternalAncestors.add(mother.getPersonID());
            }
        }
        PartitionPaternal(father);
        PartitionMaternal(mother);
    }

    private void PartitionPaternal(Person person)
    {
        // If the person has no parents.
        if (person.getFather() == null)
        {
            return;
        }
        else if (person.getFather() != null)
        {
            Person father = getPersonFromID(person.getFather());
            Person mother = getPersonFromID(person.getMother());
            m_paternalAncestors.add(father.getPersonID());
            m_paternalAncestors.add(mother.getPersonID());
            PartitionPaternal(father);
            PartitionPaternal(mother);
        }
    }

    private void PartitionMaternal(Person person)
    {
        if (person.getFather() == null)
        {
            return;
        }
        else if (person.getFather() != null)
        {
            Person father = getPersonFromID(person.getFather());
            Person mother = getPersonFromID(person.getMother());
            m_maternalAncestors.add(father.getPersonID());
            m_maternalAncestors.add(mother.getPersonID());
            PartitionMaternal(father);
            PartitionPaternal(mother);
        }
    }

    /*
    Parses an array of Events and stores them in a list.

    The different types are also stored in their respective data member
     */
    public void addEvents(Event[] data)
    {
        for (Event e : data)
        {
            m_events.put(e.getEventID(), e);
            AddNewEventType(e);
            if (m_personEvents.containsKey(e.getPersonID()))
            {
                m_personEvents.get(e.getPersonID()).add(e);
            }
            else
            {
                List<Event> events = new ArrayList<>();
                events.add(e);
                m_personEvents.put(e.getPersonID(), events);
            }
        }
    }

    // Adds event types to the filter member.
    // Generates a color at random for each event type.
    private void AddNewEventType(Event event)
    {
        for (Map.Entry<String, Integer> entry : m_eventTypeColors.entrySet())
        {
            if (entry.getKey().equals(event.getEventType()))
            {
                return;
            }
        }
        m_eventTypes.add(event.getEventType());
        m_filter.getTypeFilters().add(new EventTypeData(event.getEventType()));
        m_eventTypeColors.put(event.getEventType(), new Random().nextInt(360));
    }


    // Returns the color for that specific event.
    public Integer GetEventColor(Event event)
    {
        Integer bitmap = m_eventTypeColors.get(event.getEventType());
        return bitmap;
    }

    // Populates the rest of the fields for the user. Prior to call, only the PersonID is known.
    public void UpdateUserInformation() {
        for (Map.Entry<String, Person> entry : m_people.entrySet())
        {
            if (entry.getKey().equals(m_user.getPersonID()))
            {
                m_user = entry.getValue();
                break;
            }
        }
    }

    public Settings getSettings() {
        return m_settings;
    }

    public void setSettings(Settings settings) {
        m_settings = settings;
    }

    public void setPaternalAncestors(Set<String> paternalAncestors) {
        m_paternalAncestors = paternalAncestors;
    }

    public void setMaternalAncestors(Set<String> maternalAncestors) {
        m_maternalAncestors = maternalAncestors;
    }

    public Map<String, List<Person>> getPersonChildren() {
        return m_personChildren;
    }

    public void setPersonChildren(Map<String, List<Person>> personChildren) {
        m_personChildren = personChildren;
    }

    public Map<String, Person> getPeople() {

        return m_people;
    }

    public void setPeople(Map<String, Person> people) {
        m_people = people;
    }

    public Map<String, Event> getEvents() {
        return m_events;
    }

    public void setEvents(Map<String, Event> events) {
        m_events = events;
    }

    public Map<String, List<Event>> getPersonEvents() {
        return m_personEvents;
    }

    public void setPersonEvents(Map<String, List<Event>> personEvents) {
        m_personEvents = personEvents;
    }

    public Filter getFilter() {
        return m_filter;
    }

    public void setFilter(Filter filter) {
        m_filter = filter;
    }

    public List<String> getEventTypes() {
        return m_eventTypes;
    }

    public void setEventTypes(List<String> eventTypes) {
        m_eventTypes = eventTypes;
    }

    public String getHost() {
        return m_host;
    }

    public void setHost(String host) {
        m_host = host;
    }

    public String getPort() {
        return m_port;
    }

    public void setPort(String port) {
        m_port = port;
    }

    public void setAuthToken(String authToken) {
        m_authToken = authToken;
    }

    public String getAuthToken() {
        return m_authToken;
    }

    public Set<String> getPaternalAncestors() {
        return m_paternalAncestors;
    }

    public Set<String> getMaternalAncestors() {
        return m_maternalAncestors;
    }

    // Returns relatives of a person. Finds children if they have them, finds ancestors.

    public List<Person> getRelatives(Person person)
    {
        List<Person> relatives = new ArrayList<>();
        if (hasChildren(person))
        {
            relatives.add(findChild(person));
        }
        if (person.getFather() != null)
        {
            relatives.addAll(getAncestors(person));
        }
        if (person.getSpouse() != null)
        {
            relatives.add(getPersonFromID(person.getSpouse()));
        }
        return relatives;
    }

    private Person findChild(Person person)
    {
        Person child = new Person();
        for (String id : getFilteredPersons())
        {
            if (getPersonFromID(id).getFather() != null &&
                (getPersonFromID(id).getFather().equals(person.getPersonID()) ||
                getPersonFromID(id).getMother().equals(person.getPersonID())))
            {
                child = getPersonFromID(id);
            }
        }
        return child;
    }

    private boolean hasChildren(Person person)
    {
        Set<String> filteredIDs = getFilteredPersons();
        for (String id : filteredIDs)
        {
            Person p = getPersonFromID(id);
            if ((p.getFather() != null && p.getMother() != null) &&
                (p.getFather().equals(person.getPersonID()) ||
                 p.getMother().equals(person.getPersonID())))
            {
                return true;
            }
        }
        return false;
    }

    private List<Person> getAncestors(Person person)
    {
        List<Person> ancestors = new ArrayList<>();
        if (person.getFather() != null)
        {
            ancestors.add(getPersonFromID(person.getFather()));
            ancestors.add(getPersonFromID(person.getMother()));
            ancestors.addAll(getAncestors(getPersonFromID(person.getFather())));
            ancestors.addAll(getAncestors(getPersonFromID(person.getMother())));
        }
        return ancestors;
    }

    /*
    Depricated. Works with a fully populated tree, where the root person has no spouse. Can generate generations of data and calculate direct line relationships.
     */
    private List<Person> getDescendants(Person person)
    {
        List<Person> descendants = new ArrayList<>();
        for (Map.Entry<String, Person> entry : m_people.entrySet())
        {
            if (entry.getValue().getFather() != null)
            {
                if (entry.getValue().getFather().equals(person.getPersonID()) || entry.getValue().getMother().equals(person.getPersonID()))
                {
                    descendants.add(entry.getValue());
                    if (entry.getValue().getGeneration() > 0)
                    {
                        descendants.addAll(getDescendants(entry.getValue()));
                    }
                    return descendants;
                }
            }
        }
        return descendants;
    }


    /*
    Returns the event with the matching eventID
     */
    public Event getEventFromEventID(String id) {
        Event evnt = new Event();
        for (Map.Entry<String, Event> entry : m_events.entrySet())
        {
            if (entry.getValue().getEventID().equals(id))
            {
                evnt = entry.getValue();
            }
        }
        return evnt;
    }

    /*
    Searches all people in the tree and checks if their name contains the query from the Search Activity
     */
    public List<Person> searchPersons(String query)
    {
        List<Person> matchingPersons = new ArrayList<>();
        for (String id : m_FilteredPersons)
        {
            if (getPersonFromID(id).getFirstName().toLowerCase().contains(query))
            {
                matchingPersons.add(getPersonFromID(id));
            }
            else if (getPersonFromID(id).getLastName().toLowerCase().contains(query))
            {
                matchingPersons.add(getPersonFromID(id));
            }
        }

        if (m_user.getFirstName().toLowerCase().contains(query) || m_user.getLastName().toLowerCase().contains(query))
        {
            matchingPersons.add(m_user);
        }
        if (m_userSpouse.getFirstName().toLowerCase().contains(query) || m_userSpouse.getLastName().toLowerCase().contains(query))
        {
            matchingPersons.add(m_userSpouse);
        }

        return matchingPersons;
    }

    /*
    Searches all events to see if the type, city, or country contain the query.
     */
    public List<Event> searchEvents(String query)
    {
        List<Event> matchingEvents = new ArrayList<>();
        for (Map.Entry<String, Event> entry : m_events.entrySet())
        {
            if (entry.getValue().getEventType().toLowerCase().contains(query))
            {
                matchingEvents.add(entry.getValue());
            }
            else if(entry.getValue().getCountry().toLowerCase().contains(query))
            {
                matchingEvents.add(entry.getValue());
            }
            else if (entry.getValue().getCity().toLowerCase().contains(query))
            {
                matchingEvents.add(entry.getValue());
            }
            else if (String.valueOf(entry.getValue().getYear()).contains(query))
            {
                matchingEvents.add(entry.getValue());
            }
        }
        return matchingEvents;
    }

    /*
    Returns the personIDs of the Persons that fit the filter conditions
     */
    public Set<String> getFilteredPersons() {
        return m_FilteredPersons;
    }

    public void setFilteredPersons(Set<String> filteredPersons) {
        m_FilteredPersons = filteredPersons;
    }

    public Person getUserSpouse() {
        return m_userSpouse;
    }

    public void setUserSpouse(Person userSpouse) {
        m_userSpouse = userSpouse;
    }

    /*
    Gets the earliest event for a specified person. Checks for birth because birth, by default,
    is the first event.
     */
    public Event getEarliestEventFromPerson(Person person)
    {
        List<Event> personEvents = getPersonEvents().get(person.getPersonID());
        Event earliestEvent = personEvents.get(0);
        for (Event event : personEvents)
        {
            if (event.getEventType().toLowerCase().equals("birth"))
            {
                return event;
            }
            else if (event.getYear() < earliestEvent.getYear())
            {
                earliestEvent = event;
            }
        }
        return earliestEvent;
    }
}
