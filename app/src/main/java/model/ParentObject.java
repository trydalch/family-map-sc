package model;

import java.util.List;

/**
 * Created by trevor.rydalch on 6/18/17.
 */

public class ParentObject implements com.bignerdranch.expandablerecyclerview.Model.ParentObject {

    String m_title;

    List<Object> m_ChildObjects;

    @Override
    public List<Object> getChildObjectList() {
        return m_ChildObjects;
    }

    @Override
    public void setChildObjectList(List<Object> list) {
        m_ChildObjects = list;
    }

    public String getTitle() {
        return m_title;
    }

    public void setTitle(String title) {
        m_title = title;
    }
}
