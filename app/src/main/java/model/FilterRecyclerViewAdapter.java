package model;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.trevorrydalch.familymap.R;

import java.util.List;

import ui.FilterViewHolder;
import ui.ViewHolder;

/**
 * Created by trevor.rydalch on 6/19/17.
 */

public class FilterRecyclerViewAdapter extends RecyclerView.Adapter<FilterViewHolder>
{

    List<Object> m_dataSet;

    public FilterRecyclerViewAdapter(List<Object> data)
    {
        m_dataSet = data;
    }

    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_list_item, parent, false);
        FilterViewHolder vh = new FilterViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(FilterViewHolder holder, int position)
    {
        if (m_dataSet.get(position) instanceof EventTypeData)
        {
            final EventTypeData eventTypeData = (EventTypeData) m_dataSet.get(position);
            holder.m_TopTextView.setText(eventTypeData.getType() + " Events");
            holder.m_BottomTextView.setText("FILTER BY " + eventTypeData.getType() + " Events");
            holder.m_switch.setChecked(eventTypeData.isShow());
        }
    }

    @Override
    public int getItemCount() {
        return m_dataSet.size();
    }
}
