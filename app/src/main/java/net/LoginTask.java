package net;

import android.os.AsyncTask;

import com.example.trevorrydalch.familymap.R;

import commrequests.EventRequest;
import commrequests.LoginRequest;
import commrequests.PersonRequest;
import commresults.LoginResult;
import model.Model;
import model.Person;

/**
 * Created by trevor.rydalch on 6/8/17.
 */

public class LoginTask  extends AsyncTask<LoginRequest, Void, LoginResult>
{

    private String m_Host;
    private String m_Port;

    private CallBack m_callBack;
    public interface CallBack  {
        void makeToast(int id);
    }

    public LoginTask(CallBack c, String host, String port)
    {
        m_callBack = c;
        m_Host = host;
        m_Port = port;
    }

    @Override
    protected LoginResult doInBackground(LoginRequest... params)
    {
        // Instance of ServerProxy
        ServerProxy server = new ServerProxy();
        // Login
        LoginResult result = server.login(params[0], m_Host, m_Port);
        // Set personID of user. Will update all fields after datasync task completes
        Person usr = new Person();
        usr.setPersonID(result.getPersonId());
        Model.SINGLETON.setUser(usr);
        // Return the loginresult
        return result;
    }

    // Closes the thread and returns to original
    @Override
    protected void onPostExecute(LoginResult loginResult) {
        super.onPostExecute(loginResult);

        // TODO update toasts with strings for invalid username or invalid password.
        int iToastID;
        if (loginResult == null)
        {
            iToastID = R.string.unsuccessful_login;
        }
        else if (loginResult.getMessage() == null)
        {
            // Login was successful. Get ID for toast and save Auth Token
            iToastID = R.string.successful_login;
        }
        else
        {
            iToastID = R.string.unsuccessful_login;
        }
        m_callBack.makeToast(iToastID);

        if (loginResult != null && loginResult.getAuthToken() != null)
        {
            PersonRequest persRequest = new PersonRequest();
            persRequest.setAuthToken(loginResult.getAuthToken());
            EventRequest eventRequest = new EventRequest();
            eventRequest.setAuthToken(loginResult.getAuthToken());

            Model.SINGLETON.setHost(m_Host);
            Model.SINGLETON.setPort(m_Port);
            Model.SINGLETON.setAuthToken(loginResult.getAuthToken());

            DataSyncTask dataSyncTask = new DataSyncTask((DataSyncTask.CallBack) m_callBack, m_Host, m_Port);
            dataSyncTask.execute(persRequest, eventRequest);
        }
    }


    public String getHost() {
        return m_Host;
    }

    public void setHost(String host) {
        m_Host = host;
    }

    public String getPort() {
        return m_Port;
    }

    public void setPort(String port) {
        m_Port = port;
    }

    public CallBack getCallBack() {
        return m_callBack;
    }

    public void setCallBack(CallBack callBack) {
        m_callBack = callBack;
    }
}
