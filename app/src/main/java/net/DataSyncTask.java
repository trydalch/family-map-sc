package net;

import android.os.AsyncTask;

import com.example.trevorrydalch.familymap.R;

import commrequests.EventRequest;
import commrequests.PersonRequest;
import commresults.EventResult;
import commresults.PersonResult;
import model.Model;
import model.Settings;
import ui.SettingsActivity;

/**
 * Created by trevor.rydalch on 6/8/17.
 */

public class DataSyncTask extends AsyncTask<Object, Void, Boolean>
{
    private String m_host;
    private String m_port;

    private PersonResult m_personResult;
    private EventResult m_eventResult;

    private CallBack m_callBack;
    public interface CallBack {
        void makeToast(int id);
        void onDataSync();
    }

    public DataSyncTask(CallBack c, String host, String port)
    {
        m_callBack = c;
        m_host = host;
        m_port = port;
    }

    @Override
    protected Boolean doInBackground(Object... params)
    {
        boolean success = false;

        ServerProxy server = new ServerProxy();

        m_personResult = server.person((PersonRequest)params[0], m_host, m_port);
        m_eventResult = server.event((EventRequest)params[1], m_host, m_port);

        if (m_eventResult.getData().length > 0 && m_personResult.getData().length > 0)
        {
            success = true;
        }

        if (m_callBack.getClass().equals(SettingsActivity.class) && !success)
        {
            m_callBack.makeToast(R.string.get_data_fail);
        }
        else if (m_callBack.getClass().equals(SettingsActivity.class) && success)
        {
            m_callBack.onDataSync();
        }

        return success;
    }

    @Override
    protected void onPostExecute(Boolean b) {
        super.onPostExecute(b);

        if (b)
        {
            Model.SINGLETON.addPersons(m_personResult.getData());
            Model.SINGLETON.UpdateUserInformation();
            Model.SINGLETON.addEvents(m_eventResult.getData());

            m_callBack.onDataSync();
        }
    }
}
