package net;

import android.os.AsyncTask;

import com.example.trevorrydalch.familymap.R;

import commrequests.EventRequest;
import commrequests.PersonRequest;
import commrequests.RegisterRequest;
import commresults.RegisterResult;
import model.Model;
import model.Person;
import ui.LoginFragment;

/**
 * Created by trevor.rydalch on 6/8/17.
 */

public class RegisterTask extends AsyncTask<RegisterRequest, Void, RegisterResult>
{

    private String m_Host;
    private String m_Port;


    private CallBack m_callBack;
    public interface CallBack  {
        void makeToast(int id);
    }

    public RegisterTask(CallBack c, String host, String port)
    {
        m_callBack = c;
        m_Host = host;
        m_Port = port;
    }

    @Override
    protected RegisterResult doInBackground(RegisterRequest... params)
    {
        ServerProxy server = new ServerProxy();

        RegisterResult result = server.register(params[0], m_Host, m_Port);

        Person usr = new Person();
        usr.setPersonID(result.getPersonId());
        Model.SINGLETON.setUser(usr);
        return result;
    }

    @Override
    protected void onPostExecute(RegisterResult registerResult) {
        super.onPostExecute(registerResult);

        int iToastID;
        if (registerResult == null)
        {
            iToastID = R.string.unsuccessful_login;
        }
        else if (registerResult.getMessage() == null)
        {
            // Register was successful. Get ID for toast and save Auth Token
            iToastID = R.string.successful_register;
        }
        else if (registerResult.getMessage().toLowerCase().contains("username"))
        {
            iToastID = R.string.username_taken;
        }
        else
        {
            iToastID = R.string.unsuccessful_register;
        }
        m_callBack.makeToast(iToastID);

        if (registerResult.getAuthToken() != null) {
            PersonRequest persRequest = new PersonRequest();
            persRequest.setAuthToken(registerResult.getAuthToken());
            EventRequest eventRequest = new EventRequest();
            eventRequest.setAuthToken(registerResult.getAuthToken());

            DataSyncTask dataSyncTask = new DataSyncTask((DataSyncTask.CallBack) m_callBack, m_Host, m_Port);
            dataSyncTask.execute(persRequest, eventRequest);
        }
    }
}
