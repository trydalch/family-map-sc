package net;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import commrequests.EventRequest;
import commrequests.LoginRequest;
import commrequests.PersonRequest;
import commrequests.RegisterRequest;
import commresults.EventResult;
import commresults.LoginResult;
import commresults.PersonResult;
import commresults.RegisterResult;

/**
 * Created by trevor.rydalch on 6/7/17.
 */

public class ServerProxy
{
    private String m_host;
    private String m_port;

    public RegisterResult register(RegisterRequest request, String host, String port)
    {
        RegisterResult result = new RegisterResult();
        try
        {
            // Create a URL indicating where the server is running, and which
            // web API operation we want to call
            URL url = new URL("http://" + host + ":" + port + "/user/register");

            // Start constructing our HTTP request
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            connection.setRequestMethod("POST");

            // Indicate that this request will contain an HTTP request body
            connection.setDoOutput(true);

            // Specify that we would like to receive the server's response in JSON
            // format by putting an HTTP "Accept" header on the request (this is not
            // necessary because our server only returns JSON responses, but it
            // provides one more example of how to add a header to an HTTP request).
            connection.addRequestProperty("Accept", "application/json");

            // Connect to the server and send the HTTP request
//            connection.connect();

            Gson gson = new Gson();
            String reqData = gson.toJson(request);

            // Get the output stream containing the HTTP request body
            OutputStream reqBody = connection.getOutputStream();
            // Write the JSON data to the request body
            writeString(reqData, reqBody);
            // Close the request body output stream, indicating that the
            // request is complete
            reqBody.close();

            // By the time we get here, the HTTP response has been received from the server.
            // Check to make sure that the HTTP response from the server contains a 200
            // status code, which means "success".  Treat anything else as a failure.
            int Response = connection.getResponseCode();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // The HTTP response status code indicates success
                // Get the input stream containing the HTTP response body
                InputStream respBody = connection.getInputStream();
                // Extract JSON data from the HTTP response body
                String respData = readString(respBody);
                result = gson.fromJson(respData, RegisterResult.class);
            }
            else {
                // The HTTP response status code indicates an error
                // occurred, so print out the message from the HTTP response
                System.out.println("ERROR: " + connection.getResponseMessage());
            }
        }
        catch(IOException e)
        {

        }
        return result;
    }


    public LoginResult login(LoginRequest request, String host, String port)
    {
        LoginResult result = new LoginResult();
        try
        {
            // Create a URL indicating where the server is running, and which
            // web API operation we want to call
            URL url = new URL("http://" + host + ":" + port + "/user/login");

            // Start constructing our HTTP request
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            connection.setRequestMethod("POST");

            // Indicate that this request will contain an HTTP request body
            connection.setDoOutput(true);

            // Specify that we would like to receive the server's response in JSON
            // format by putting an HTTP "Accept" header on the request (this is not
            // necessary because our server only returns JSON responses, but it
            // provides one more example of how to add a header to an HTTP request).
            connection.addRequestProperty("Accept", "application/json");

            // Connect to the server and send the HTTP request
//            connection.connect();

            Gson gson = new Gson();
            String reqData = gson.toJson(request);

            // Get the output stream containing the HTTP request body
            OutputStream reqBody = connection.getOutputStream();
            // Write the JSON data to the request body
            writeString(reqData, reqBody);
            // Close the request body output stream, indicating that the
            // request is complete
            reqBody.close();

            // By the time we get here, the HTTP response has been received from the server.
            // Check to make sure that the HTTP response from the server contains a 200
            // status code, which means "success".  Treat anything else as a failure.
            int Response = connection.getResponseCode();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // The HTTP response status code indicates success
                // Get the input stream containing the HTTP response body
                InputStream respBody = connection.getInputStream();
                // Extract JSON data from the HTTP response body
                String respData = readString(respBody);
                result = gson.fromJson(respData, LoginResult.class);
            }
            else {
                // The HTTP response status code indicates an error
                // occurred, so print out the message from the HTTP response
                System.out.println("ERROR: " + connection.getResponseMessage());
            }
        }
        catch(IOException e)
        {

            System.out.println(e.getMessage());
        }
        return result;
    }
    public PersonResult person(PersonRequest request, String host, String port)
    {
        PersonResult result = new PersonResult();
        try
        {
            // Create a URL indicating where the server is running, and which
            // web API operation we want to call
            URL url = new URL("http://" + host + ":" + port + "/person");

            // Start constructing our HTTP request
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            connection.setRequestMethod("GET");

            connection.addRequestProperty("Authorization", request.getAuthToken());

            // Indicate that this request will contain an HTTP request body
            connection.setDoOutput(false);

            // Specify that we would like to receive the server's response in JSON
            // format by putting an HTTP "Accept" header on the request (this is not
            // necessary because our server only returns JSON responses, but it
            // provides one more example of how to add a header to an HTTP request).
            connection.addRequestProperty("Accept", "application/json");
            Gson gson = new Gson();
//            String reqData = gson.toJson(request);

            // Get the output stream containing the HTTP request body
//            OutputStream reqBody = connection.getOutputStream();
            // Write the JSON data to the request body
//            writeString(reqData, reqBody);
            // Close the request body output stream, indicating that the
            // request is complete
//            reqBody.close();

            connection.connect();

            // By the time we get here, the HTTP response has been received from the server.
            // Check to make sure that the HTTP response from the server contains a 200
            // status code, which means "success".  Treat anything else as a failure.
//            int Response = connection.getResponseCode();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // The HTTP response status code indicates success
                // Get the input stream containing the HTTP response body
                InputStream respBody = connection.getInputStream();
                // Extract JSON data from the HTTP response body
                String respData = readString(respBody);
                result = gson.fromJson(respData, PersonResult.class);
            }
            else {
                // The HTTP response status code indicates an error
                // occurred, so print out the message from the HTTP response
                System.out.println("ERROR: " + connection.getResponseMessage());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return result;
    }

    public EventResult event(EventRequest request, String host, String port)
    {
        EventResult result = new EventResult();
        try
        {
            // Create a URL indicating where the server is running, and which
            // web API operation we want to call
            URL url = new URL("http://" + host + ":" + port + "/event");

            // Start constructing our HTTP request
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            connection.setRequestMethod("GET");

            connection.addRequestProperty("Authorization", request.getAuthToken());

            // Indicate that this request will contain an HTTP request body
            connection.setDoOutput(false);

            // Specify that we would like to receive the server's response in JSON
            // format by putting an HTTP "Accept" header on the request (this is not
            // necessary because our server only returns JSON responses, but it
            // provides one more example of how to add a header to an HTTP request).
            connection.addRequestProperty("Accept", "application/json");
            Gson gson = new Gson();
//            String reqData = gson.toJson(request);

            // Get the output stream containing the HTTP request body
//            OutputStream reqBody = connection.getOutputStream();
            // Write the JSON data to the request body
//            writeString(reqData, reqBody);
            // Close the request body output stream, indicating that the
            // request is complete
//            reqBody.close();

            // By the time we get here, the HTTP response has been received from the server.
            // Check to make sure that the HTTP response from the server contains a 200
            // status code, which means "success".  Treat anything else as a failure.
            int Response = connection.getResponseCode();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // The HTTP response status code indicates success
                // Get the input stream containing the HTTP response body
                InputStream respBody = connection.getInputStream();
                // Extract JSON data from the HTTP response body
                String respData = readString(respBody);
                result = gson.fromJson(respData, EventResult.class);
            }
            else {
                // The HTTP response status code indicates an error
                // occurred, so print out the message from the HTTP response
                System.out.println("ERROR: " + connection.getResponseMessage());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return result;
    }

    /*
        The readString method shows how to read a String from an InputStream.
    */
    private static String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    /*
        The writeString method shows how to write a String to an OutputStream.
    */
    private static void writeString(String str, OutputStream os) throws IOException {
        OutputStreamWriter sw = new OutputStreamWriter(os);
        sw.write(str);
        sw.flush();
    }

    public void setHost(String host) {
        m_host = host;
    }

    public void setPort(String port) {
        m_port = port;
    }
}
