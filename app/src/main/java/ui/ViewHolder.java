package ui;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.trevorrydalch.familymap.R;

/**
 * Created by trevor.rydalch on 6/18/17.
 */

public class ViewHolder extends com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder{

    public TextView m_TopTextView;
    public TextView m_BottomTextView;
    public ImageView m_imageView;
    public RelativeLayout m_relativeLayout;


    public ViewHolder(View itemView) {
        super(itemView);

        m_TopTextView = (TextView) itemView.findViewById(R.id.view_top_person_child_list_item);
        m_BottomTextView = (TextView) itemView.findViewById(R.id.view_bottom_person_child_list_item);
        m_imageView = (ImageView) itemView.findViewById(R.id.image_child_list_item);
        m_relativeLayout = (RelativeLayout) itemView.findViewById(R.id.pers_rel_layout_child_list_item);
    }
}
