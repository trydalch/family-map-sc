package ui;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;

import com.example.trevorrydalch.familymap.R;

import java.util.ArrayList;
import java.util.List;

import model.Event;
import model.Filter;
import model.Model;
import model.Person;
import model.SearchRecylcerViewAdapter;

public class SearchActivity extends AppCompatActivity {

    private SearchView m_SearchBox;
    private RecyclerView m_recyclerView;
    private SearchRecylcerViewAdapter m_adapter;
    private Context m_context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        m_context = this;
        m_SearchBox = (SearchView) findViewById(R.id.search_box);

        m_recyclerView = (RecyclerView) findViewById(R.id.recycler_search_activity);
        m_recyclerView.setLayoutManager(new LinearLayoutManager(m_context));

        m_SearchBox.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                if(!m_SearchBox.getQuery().toString().isEmpty()) {
                    m_adapter = new SearchRecylcerViewAdapter(findSearchMatches(m_SearchBox.getQuery()));
                }
                else
                {
                    m_adapter.clear();
                }
                m_recyclerView.setAdapter(m_adapter);
                return false;
            }
        });

    }

    private Object[] findSearchMatches(CharSequence sequence)
    {
        String query = sequence.toString();
        List<Person> matchingPersons = Model.SINGLETON.searchPersons(query);
        clearRelations(matchingPersons);
        List<Event> matchingEvents = Model.SINGLETON.searchEvents(query);

        Object[] objects = convertToObjectArray(matchingPersons, matchingEvents);

        return objects;
    }

    private void clearRelations(List<Person> matchingPersons) {
        for (Person person : matchingPersons)
        {
            if (person.getRelation() != null)
            {
                person.setRelation(null);
            }
        }
    }

    private Object[] convertToObjectArray(List<Person> matchingPersons, List<Event> matchingEvents)
    {
        Object[] objects = new Object[matchingEvents.size() + matchingPersons.size()];
        int index = 0;
        for (Person person : matchingPersons)
        {
            objects[index] = person;
            index++;
        }
        for (Event event : matchingEvents)
        {
            objects[index] = event;
            index++;
        }
        return objects;
    }

}
