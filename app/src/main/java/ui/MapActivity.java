package ui;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.trevorrydalch.familymap.R;

/*
Hosts a map fragment and displays a menu with a 'Go-To-Top' button. For this map fragment, it is not possible to filter or change the settings.
 */
public class MapActivity extends AppCompatActivity  implements MapFragment.OnFragmentInteractionListener{

    private static final String TAG_EVNT = "EVNT";
    private static final String TAG_MENU = "UP";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent intent = getIntent();
        String eventID = intent.getStringExtra(TAG_EVNT);

        FragmentManager fragManager = getSupportFragmentManager();
        Fragment fragment = fragManager.findFragmentById(R.id.fragment_container_map_activity);

        fragment = MapFragment.newInstance(eventID, TAG_MENU);

        fragManager.beginTransaction().add(R.id.fragment_container_map_activity, fragment).commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    /*
    Handles when the go-to-top menu button is selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_up)
        {
            Intent upIntent = new Intent(this, MainActivity.class);
            upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(upIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
