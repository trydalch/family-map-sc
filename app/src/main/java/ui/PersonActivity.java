package ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.trevorrydalch.familymap.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.Event;
import model.ExpandableRecyclerViewAdapter;
import model.Model;
import model.ParentObject;
import model.Person;

public class PersonActivity extends AppCompatActivity {

    private static final String SELECTED_PERSON = "PRSN";

    private static final String GRND_PRNT = "Grandparent";
    private static final String GRND_CHLD = "Grandchild";
    private static final String CHLD = "Child";
    private static final String SPOUSE = "Spouse";
    private static final String MOTHER = "Mother";
    private static final String FATHER = "Father";
    private static final String DISTANT_ANSC = "Distant Anscestor";
    private static final String DISTANT_DESC = "Distant Descendant";

    private Person m_personToDisplay;
    TextView m_tvFirstName;
    TextView m_tvLastName;
    TextView m_tvGender;

    RecyclerView m_recyclerView;
    ExpandableRecyclerViewAdapter m_recyclerViewAdapter;
    List<Person> m_relatives;
    List<Event> m_lifeEvents;
    ParentViewHolder m_eventSection;
    ParentViewHolder m_relativeSection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);

        Intent intent = getIntent();
        String persID = intent.getStringExtra(SELECTED_PERSON);

        m_personToDisplay = Model.SINGLETON.getPersonFromID(persID);

        m_tvFirstName = (TextView) findViewById(R.id.view_fname_persAct);
        m_tvLastName = (TextView) findViewById(R.id.view_lname_persAct);
        m_tvGender = (TextView) findViewById(R.id.view_gender_persAct);
        m_recyclerView = (RecyclerView) findViewById(R.id.recycler_person_activity);

        m_tvFirstName.setText(m_personToDisplay.getFirstName());
        m_tvLastName.setText(m_personToDisplay.getLastName());
        if (m_personToDisplay.getGender().toLowerCase().equals("m"))
        {
            m_tvGender.setText("Male");
        }
        else
        {
            m_tvGender.setText("Female");
        }

        m_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        m_recyclerViewAdapter = new ExpandableRecyclerViewAdapter(this, generateRecyclerViewItems());
        m_recyclerViewAdapter.setCustomParentAnimationViewId(R.id.person_parent_list_item_expand_arrow);
        m_recyclerViewAdapter.setParentClickableViewAnimationDefaultDuration();
        m_recyclerViewAdapter.setParentAndIconExpandOnClick(true);

        m_recyclerView.setAdapter(m_recyclerViewAdapter);
    }

    private List<ParentObject> generateRecyclerViewItems()
    {
        List<ParentObject> parentObjects = new ArrayList<>();
        m_relatives = Model.SINGLETON.getRelatives(m_personToDisplay);
        m_lifeEvents = Model.SINGLETON.getPersonEvents().get(m_personToDisplay.getPersonID());

        OrderEventsChronologically();
        ArrayList<Object> eventObjects = new ArrayList<>();
        for (Event e : m_lifeEvents)
        {
            eventObjects.add(e);
        }
        ParentObject events = new ParentObject();
        events.setChildObjectList(eventObjects);
        events.setTitle("LIFE EVENTS");

        ArrayList<Object> personObjects = new ArrayList<>();
        for (Person p : m_relatives)
        {
//            GenerateRelation(p, m_personToDisplay);
            if (p.getPersonID().equals(m_personToDisplay.getFather()))
            {
                p.setRelation(FATHER);
            }
            else if (p.getPersonID().equals(m_personToDisplay.getMother()))
            {
                p.setRelation(MOTHER);
            }
            else if (m_personToDisplay.getPersonID().equals(p.getMother()) || m_personToDisplay.getPersonID().equals(p.getFather()))
            {
                p.setRelation(CHLD);
            }
            else if (m_personToDisplay.getSpouse().equals(p.getPersonID()))
            {
                p.setRelation(SPOUSE);
            }
            personObjects.add(p);
        }

        ParentObject relatives = new ParentObject();
        relatives.setChildObjectList(personObjects);
        relatives.setTitle("FAMILY");
        parentObjects.add(relatives);
        parentObjects.add(events);
        return parentObjects;
    }

    private void GenerateRelation(Person relative, Person selectedPerson) {
        int generationGap = relative.getGeneration() - selectedPerson.getGeneration();
        switch(generationGap)
        {
            case -2:
                relative.setRelation(GRND_CHLD);
                break;
            case -1:
                relative.setRelation(CHLD);
                break;
            case 1:
                if (relative.getGender().toLowerCase().equals("m"))
                {
                    relative.setRelation(FATHER);
                }
                else
                {
                    relative.setRelation(MOTHER);
                }
                break;
            case 2:
                relative.setRelation(GRND_PRNT);
                break;
            default:
                if (generationGap < 0)
                {
                    relative.setRelation(DISTANT_DESC);
                }
                else
                {
                    relative.setRelation(DISTANT_ANSC);
                }
        }
    }

    private void OrderEventsChronologically()
    {
        ArrayList<Integer> years = new ArrayList<>();
        for (Event e : m_lifeEvents)
        {
            years.add(e.getYear());
        }
        Collections.sort(years);
        ArrayList<Event> sortedEvents = new ArrayList<>();
        for (Integer i : years)
        {
            for (Event e : m_lifeEvents)
            {
                if (e.getYear() == i && !sortedEvents.contains(e))
                {
                    if (e.getEventType().toLowerCase().equals("birth"))
                    {
                        sortedEvents.add(0,e);
                    }
                    else
                    {
                        sortedEvents.add(e);
                    }
                }
            }
        }
        m_lifeEvents = sortedEvents;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.up_button_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_up)
        {
            Intent upIntent = new Intent(this, MainActivity.class);
            upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(upIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}
