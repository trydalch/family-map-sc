package ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.trevorrydalch.familymap.R;

import java.util.List;

import model.EventTypeData;
import model.Model;

/**
 * Created by trevor.rydalch on 6/19/17.
 */

public class FilterViewHolder extends RecyclerView.ViewHolder {

    public TextView m_TopTextView;
    public TextView m_BottomTextView;
    public Switch m_switch;
    public RelativeLayout m_relativeLayout;

    public FilterViewHolder(View itemView) {
        super(itemView);

        m_TopTextView = (TextView) itemView.findViewById(R.id.view_event_title_above);
        m_BottomTextView = (TextView) itemView.findViewById(R.id.view_small_below);
        m_relativeLayout = (RelativeLayout) itemView.findViewById(R.id.rel_layout_event);
        m_switch = (Switch) itemView.findViewById(R.id.switch_event_filter);

        m_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                List<EventTypeData> data = Model.SINGLETON.getFilter().getTypeFilters();
                if (data != null) {
                    for (EventTypeData type : data) {
                        if (m_TopTextView.getText().toString().contains(type.getType())) {
                            type.setShow(isChecked);
                        }
                    }
                }
            }
        });
    }


}
