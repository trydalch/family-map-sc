package ui;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Switch;

import com.example.trevorrydalch.familymap.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import model.EventTypeData;
import model.Filter;
import model.FilterRecyclerViewAdapter;
import model.Model;

/*
Dynamically loads a list of eventy types into a recylcer view and allows the user to filter the map by each event type.

Additionally, users can filter by gender, and side of the family.
 */
public class FilterActivity extends AppCompatActivity {

    private Switch m_switchPaternalFilter;
    private Switch m_switchMaternalFilter;
    private Switch m_swithcMaleFilter;
    private Switch m_switchFemaleFilter;
    private RecyclerView m_recyclerView;
    private FilterRecyclerViewAdapter m_adapter;
    private List<EventTypeData> m_eventTypes;

    private Filter m_Filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        m_Filter = Model.SINGLETON.getFilter();
        m_switchPaternalFilter = (Switch) findViewById(R.id.switch_paternal_filter);
        m_switchMaternalFilter = (Switch) findViewById(R.id.switch_maternal_filter);
        m_swithcMaleFilter = (Switch) findViewById(R.id.switch_male_filter);
        m_switchFemaleFilter = (Switch) findViewById(R.id.switch_female_filter);
        m_recyclerView = (RecyclerView) findViewById(R.id.recycler_filter_activity);
        m_eventTypes = new ArrayList<>();
        m_adapter = new FilterRecyclerViewAdapter(GenerateEventTypeData(m_Filter.getTypeFilters()));
        m_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        m_recyclerView.setAdapter(m_adapter);

        LoadSavedFilters();
    }

    /*
    Generates the objects for the RecyclerView ViewHolders.
     */
    private List<Object> GenerateEventTypeData(List<EventTypeData> eventTypes)
    {
        List<Object> eventTypeDataObjects = new ArrayList<>();
        if (eventTypes == null || eventTypes.size() == 0)
        {
            List<String> types = Model.SINGLETON.getEventTypes();
            for (String type : types)
            {
                EventTypeData data = new EventTypeData(type);
                m_eventTypes.add(data);
                eventTypeDataObjects.add((Object) data);
            }
        }
        else
        {
            for (EventTypeData data : eventTypes) {
                m_eventTypes.add(data);
                eventTypeDataObjects.add((Object) data);
            }
        }
        return eventTypeDataObjects;
    }

    /*
    Updates the UI with the previously saved settings.
     */
    private void LoadSavedFilters() {
        m_Filter = Model.SINGLETON.getFilter();
        m_switchPaternalFilter.setChecked(m_Filter.isShowPaternal());
        m_switchMaternalFilter.setChecked(m_Filter.isShowMaternal());
        m_swithcMaleFilter.setChecked(m_Filter.isShowMale());
        m_switchFemaleFilter.setChecked(m_Filter.isShowFemale());
        List<EventTypeData> eventTypes = m_Filter.getTypeFilters();
    }

    /*
    Saves the settings to the Model.SINGLETON for access by the MapFragment.
     */
    @Override
    protected void onPause() {
        super.onPause();
        m_Filter.setShowPaternal(m_switchPaternalFilter.isChecked());
        m_Filter.setShowMaternal(m_switchMaternalFilter.isChecked());
        m_Filter.setShowMale(m_swithcMaleFilter.isChecked());
        m_Filter.setShowFemale(m_switchFemaleFilter.isChecked());
        m_Filter.setTypeFilters(m_eventTypes);

        Model.SINGLETON.setFilter(m_Filter);
    }
}
