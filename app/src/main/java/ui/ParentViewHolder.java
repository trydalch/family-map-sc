package ui;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.trevorrydalch.familymap.R;

/**
 * Created by trevor.rydalch on 6/18/17.
 */

public class ParentViewHolder extends com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder{

    public TextView m_ExpandableTitle;
    public ImageButton m_DropDownArrow;

    public ParentViewHolder(View itemView) {
        super(itemView);

        m_ExpandableTitle = (TextView) itemView.findViewById(R.id.view_parent_list_item_title);
        m_DropDownArrow = (ImageButton) itemView.findViewById(R.id.person_parent_list_item_expand_arrow);
    }
}
