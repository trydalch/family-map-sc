package ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.trevorrydalch.familymap.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.Event;
import model.EventTypeData;
import model.Filter;
import model.Model;
import model.Person;
import model.Settings;

/**

 */
public class MapFragment extends Fragment  implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {


    private static final String TAG_EVNT = "EVNT";
    private static final String SELECTED_PERSON = "PRSN";
    private static final String TAG_MENU = "MENU";

    private GoogleMap mMap;
    private MapView m_mapView;
    private Polyline m_plLifeStory;
    private Polyline m_plSpouse;
    private List<Polyline> m_plFamilyTree = new ArrayList<>();

    private TextView m_tvPersonName;
    private TextView m_tvEventDetails;
    private ImageView m_ivPerson;
    private RelativeLayout m_rlPersEventDetails;

    private List<Event>  m_eventsList;

    private Event m_SelectedEvent;

    private final float m_POLYLINE_CONSTANT = 30;

    private String m_EventIDParam;
    private String m_MenuParam;

    private OnFragmentInteractionListener mListener;

    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param eventID Parameter 1.
     * @param menuType Parameter 2.
     * @return A new instance of fragment MapFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapFragment newInstance(String eventID, String menuType) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putString(TAG_EVNT, eventID);
        args.putString(TAG_MENU, menuType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            m_EventIDParam = getArguments().getString(TAG_EVNT);
            m_MenuParam = getArguments().getString(TAG_MENU);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        m_mapView = (MapView) view.findViewById(R.id.map);
        m_mapView.onCreate(savedInstanceState);
        m_mapView.onResume();
        m_mapView.getMapAsync(this);

        m_tvPersonName = (TextView) view.findViewById(R.id.view_name_map_fragment);
        m_tvEventDetails = (TextView) view.findViewById(R.id.view_eventType_Location);
        m_ivPerson = (ImageView) view.findViewById(R.id.image_person);

        m_rlPersEventDetails = (RelativeLayout) view.findViewById(R.id.rel_layout_pers_event_details);

        m_rlPersEventDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (m_SelectedEvent != null) {
                    Intent intent = new Intent(getActivity(), PersonActivity.class);
                    intent.putExtra(TAG_EVNT, m_SelectedEvent.getEventID());
                    intent.putExtra(SELECTED_PERSON, m_SelectedEvent.getPersonID());
                    startActivity(intent);
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ClearLines();
        Log.i("MapFragment", "onResume");
        if (Model.SINGLETON.getSettings() != null && mMap != null)
        {
            DrawMarkersForEvents();
            UpdateDisplayWithSettings(Model.SINGLETON.getSettings());
            UpdateFilters(Model.SINGLETON.getFilter());
        }
    }

    private void UpdateDisplayWithSettings(Settings settings)
    {
        SetMapType(settings.getMapType());
        DrawLines(settings);
    }

    private void DrawLines(Settings settings)
    {
        if (m_SelectedEvent != null)
        {
            if (settings.isShowLifeStoryLines()) {
                DrawLifeStoryLines(settings);
            }
            if (settings.isShowFamTreeLines())
            {
                DrawFamTreeLines(settings);
            }
            if (settings.isShowSpouseLines()) {
                DrawSpouseLines(settings);
            }
        }
    }



    private void DrawFamTreeLines(Settings settings) {
        Person selected_person = Model.SINGLETON.getPersonFromEvent(m_SelectedEvent);
        DrawFamTreeLines(settings, selected_person);
    }

    private void DrawFamTreeLines(Settings settings, Person pers)
    {
        if (pers.getFather() == null)
        {
            return;
        }
        else
        {
            Event curr_person_event;
            if (m_SelectedEvent.getPersonID().equals(pers.getPersonID()))
            {
                curr_person_event = m_SelectedEvent;
            }
            else
            {
                curr_person_event = Model.SINGLETON.getEventFromPerson(pers, "birth");
            }
            Person father = Model.SINGLETON.getPersonFromID(pers.getFather());
            Person mother = Model.SINGLETON.getPersonFromID(pers.getMother());
            if (Model.SINGLETON.getFilteredPersons().contains(father.getPersonID()))
            {
                Event event_father = Model.SINGLETON.getEventFromPerson(father, "birth");
                if (event_father == null)
                {
                    event_father = Model.SINGLETON.getEarliestEventFromPerson(father);
                }
                PolylineOptions toFather = new PolylineOptions()
                        .width(m_POLYLINE_CONSTANT / (father.getGeneration() + 1))
                        .color(GetAndroidColor(settings.getFamilyTreeColor()))
                        .add(new LatLng(curr_person_event.getLatitude(), curr_person_event.getLongitude()))
                        .add(new LatLng(event_father.getLatitude(), event_father.getLongitude()));
                m_plFamilyTree.add(mMap.addPolyline(toFather));
            }

            if (Model.SINGLETON.getFilteredPersons().contains(mother.getPersonID())) {
                Event event_mother = Model.SINGLETON.getEventFromPerson(mother, "birth");
                if (event_mother == null) {
                    event_mother = Model.SINGLETON.getEarliestEventFromPerson(mother);
                }

                PolylineOptions toMother = new PolylineOptions()
                        .color(GetAndroidColor(settings.getFamilyTreeColor()))
                        .width(m_POLYLINE_CONSTANT / (mother.getGeneration() + 1))
                        .add(new LatLng(curr_person_event.getLatitude(), curr_person_event.getLongitude()))
                        .add(new LatLng(event_mother.getLatitude(), event_mother.getLongitude()));
                m_plFamilyTree.add(mMap.addPolyline(toMother));
            }

            DrawFamTreeLines(settings, father);
            DrawFamTreeLines(settings, mother);
        }
    }

    private void DrawSpouseLines(Settings settings)
    {
        Person selected_person = Model.SINGLETON.getPersonFromEvent(m_SelectedEvent);
        Person spouse = Model.SINGLETON.getPersonFromID(selected_person.getSpouse());

        if (spouse != null && CheckShowBothSexes())
        {
            Event spouse_birth = Model.SINGLETON.getEarliestEventFromPerson(spouse);

            PolylineOptions spouseLineOptions = new PolylineOptions()
                    .width(m_POLYLINE_CONSTANT/ 2)
                    .color(GetAndroidColor(settings.getSpouseLinesColor()))
                    .add(new LatLng(spouse_birth.getLatitude(), spouse_birth.getLongitude()))
                    .add(new LatLng(m_SelectedEvent.getLatitude(), m_SelectedEvent.getLongitude()));
            m_plSpouse = mMap.addPolyline(spouseLineOptions);
        }
    }

    private void DrawLifeStoryLines(Settings settings)
    {
        List<Event> events = Model.SINGLETON.getPersonEvents().get(m_SelectedEvent.getPersonID());
        Event[] array_event  = new Event[events.size()];
        events.toArray(array_event);

        // Put into chronological order
        for (int i = 0; i < array_event.length; i++)
        {
            for (int j = 0; j < array_event.length; j++)
            {
                if (array_event[i].getYear() >= array_event[j].getYear())
                {
                    Event temp = array_event[j];
                    array_event[j] = array_event[i];
                    array_event[i] = temp;
                }
            }
        }

        PolylineOptions lifeStoryOptions = new PolylineOptions().color(GetAndroidColor(settings.getLifeStoryLinesColor()));
        for (Event e : array_event)
        {
            lifeStoryOptions
                    .width(m_POLYLINE_CONSTANT / 2)
                    .add(new LatLng(e.getLatitude(), e.getLongitude()));
        }
        m_plLifeStory = mMap.addPolyline(lifeStoryOptions);
    }

    private int GetAndroidColor(Settings.Color input) {
        int outColor;
        switch(input)
        {
            case GREEN:
                outColor = Color.GREEN;
                break;
            case BLUE:
                outColor = Color.BLUE;
                break;
            case RED:
                outColor = Color.RED;
                break;
            case YELLOW:
                outColor = Color.YELLOW;
                break;
            case WHITE:
                outColor = Color.WHITE;
                break;
            default:
                outColor = Color.GRAY;
        }
        return outColor;
    }


    private void SetMapType(Settings.MapType mapType)
    {
        switch (mapType)
        {
            case NORMAL:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case HYBRID:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case SATELLITE:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case TERRAIN:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            default:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        DrawMarkersForEvents();

        if (m_EventIDParam != null)
        {
            m_SelectedEvent = Model.SINGLETON.getEventFromEventID(m_EventIDParam);
            Person pers = Model.SINGLETON.getPersonFromEvent(m_SelectedEvent);
            UpdatePersonInfo(pers);
            UpdateEventInfo(m_SelectedEvent);
            LatLng eventLocation = new LatLng(m_SelectedEvent.getLatitude(), m_SelectedEvent.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLng(eventLocation));
        }
        UpdateDisplayWithSettings(Model.SINGLETON.getSettings());

        mMap.setOnMarkerClickListener(this);
    }

    private void UpdateFilters(Filter filter)
    {
        Model.SINGLETON.getFilteredPersons().clear();
        if (Model.SINGLETON.getFilter().isShowPaternal())
        {
            Model.SINGLETON.getFilteredPersons().addAll(Model.SINGLETON.getPaternalAncestors());
        }
        if (Model.SINGLETON.getFilter().isShowMaternal())
        {
            Model.SINGLETON.getFilteredPersons().addAll(Model.SINGLETON.getMaternalAncestors());
        }
        ApplyMaleFemaleFilter();
    }

    private void DrawMarkersForEvents()
    {
        mMap.clear();
        List<String> personIDs = new ArrayList<>();
        personIDs.add(Model.SINGLETON.getUser().getPersonID());
        Model.SINGLETON.getFilteredPersons().clear();
        Model.SINGLETON.getFilteredPersons().add(Model.SINGLETON.getUser().getPersonID());
        if (Model.SINGLETON.getFilter().isShowPaternal())
        {
            personIDs.addAll(Model.SINGLETON.getPaternalAncestors());
            Model.SINGLETON.getFilteredPersons().addAll(Model.SINGLETON.getPaternalAncestors());
        }
        if (Model.SINGLETON.getFilter().isShowMaternal())
        {
            personIDs.addAll(Model.SINGLETON.getMaternalAncestors());
            Model.SINGLETON.getFilteredPersons().addAll(Model.SINGLETON.getMaternalAncestors());
        }
        ApplyMaleFemaleFilter();
        if (Model.SINGLETON.getUserSpouse().getPersonID() != null)
        {
            personIDs.add(Model.SINGLETON.getUserSpouse().getPersonID());
        }
        List<Person> persons = new ArrayList<>();
        for (String id : personIDs)
        {
            persons.add(Model.SINGLETON.getPersonFromID(id));
        }

        List<Event> events = new ArrayList<>();
        for (Person person : persons)
        {
            if (person.getGender().toLowerCase().equals("m") && Model.SINGLETON.getFilter().isShowMale())
            {
                events.addAll(Model.SINGLETON.getPersonEvents().get(person.getPersonID()));

            }
            if (person.getGender().toLowerCase().equals("f") && Model.SINGLETON.getFilter().isShowFemale())
            {
                events.addAll(Model.SINGLETON.getPersonEvents().get(person.getPersonID()));
            }
        }

        ApplyEventTypeFilter(events);
        for (Event e : events)
        {
            LatLng location = new LatLng(e.getLatitude(), e.getLongitude());
            mMap.addMarker(new MarkerOptions()
                                .position(location)
                                .icon(BitmapDescriptorFactory.defaultMarker(GetEventColor(e)))
                                .title(e.getCity() + ", " + e.getCountry()))
                                .setTag(e);
        }
    }

    private void ApplyEventTypeFilter(List<Event> events)
    {
        List<EventTypeData> filters = Model.SINGLETON.getFilter().getTypeFilters();
        for (EventTypeData typeData : filters)
        {
            if (!typeData.isShow())
            {
                for (Event event : Model.SINGLETON.getAllEvents())
                {
                    if (event.getEventType().equals(typeData.getType())) {
                        events.remove(event);
                    }
                }
            }
        }
    }

    private void ApplyMaleFemaleFilter() {
        Set<String> personsToCheck = new HashSet<String>(Model.SINGLETON.getFilteredPersons());
        for (String id : personsToCheck)
        {
            if (Model.SINGLETON.getPersonFromID(id).getGender().toLowerCase().equals("m") && !Model.SINGLETON.getFilter().isShowMale())
            {
                Model.SINGLETON.getFilteredPersons().remove(id);
            }
            if (Model.SINGLETON.getPersonFromID(id).getGender().toLowerCase().equals("f") && !Model.SINGLETON.getFilter().isShowFemale())
            {
                Model.SINGLETON.getFilteredPersons().remove(id);
            }
        }
        if (Model.SINGLETON.getFilter().isShowMale())
        {
            if (Model.SINGLETON.getUserSpouse().getGender() != null) {
                if (Model.SINGLETON.getUserSpouse().getGender().toLowerCase().equals("m")) {
                    Model.SINGLETON.getFilteredPersons().add(Model.SINGLETON.getUserSpouse().getPersonID());
                } else if (Model.SINGLETON.getUser().getGender().toLowerCase().equals("m")) {
                    Model.SINGLETON.getFilteredPersons().add(Model.SINGLETON.getUser().getPersonID());

                }
            }
        }
        if (Model.SINGLETON.getFilter().isShowFemale()) {
            if (Model.SINGLETON.getUserSpouse().getGender() != null) {
                if (Model.SINGLETON.getUserSpouse().getGender().toLowerCase().equals("f")) {
                    Model.SINGLETON.getFilteredPersons().add(Model.SINGLETON.getUserSpouse().getPersonID());
                } else if (Model.SINGLETON.getUser().getGender().toLowerCase().equals("f")) {
                    Model.SINGLETON.getFilteredPersons().add(Model.SINGLETON.getUser().getPersonID());
                }
        }
        }
    }


    private Integer GetEventColor(Event event) {
        return Model.SINGLETON.GetEventColor(event);
    }

    @Override
    public boolean onMarkerClick(Marker marker)
    {
        Event event = (Event) marker.getTag();
        Person persToDisplay = Model.SINGLETON.getPersonFromEvent(event);
        UpdatePersonInfo(persToDisplay);
        UpdateEventInfo(event);
        m_SelectedEvent = event;
        ClearLines();
        DrawLines(Model.SINGLETON.getSettings());
        return false;
    }

    private void ClearLines() {
        if (m_plLifeStory != null)
        {
            m_plLifeStory.remove();
        }
        if (m_plSpouse != null)
        {
            m_plSpouse.remove();
        }
        if (m_plFamilyTree.size() > 0)
        {
            for(Polyline poly : m_plFamilyTree)
            {
                poly.remove();
            }
        }
    }

    private boolean CheckShowBothSexes()
    {
        return Model.SINGLETON.getFilter().isShowMale() && Model.SINGLETON.getFilter().isShowFemale();
    }

    private void UpdatePersonInfo(Person person)
    {
        if (person.getGender().toUpperCase().equals("M"))
        {
            m_ivPerson.setImageResource(R.mipmap.ic_pers_male);
        }
        else
        {
            m_ivPerson.setImageResource(R.mipmap.ic_pers_female);
        }
        m_tvPersonName.setText(person.getFirstName() + " " + person.getLastName());
    }

    private void UpdateEventInfo(Event event)
    {
        m_tvEventDetails.setText(event.getEventType() + ": " + event.getCity() + ", " + event.getCountry() + "(" + event.getYear() + ")");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (m_MenuParam != null && m_MenuParam.equals("UP"))
        {
            inflater.inflate(R.menu.up_button_menu, menu);
        }
        else
        {
            inflater.inflate(R.menu.main_activity_map_frag, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_settings)
        {
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(intent);
        }
        else if (item.getItemId() == R.id.menu_filter)
        {
            Intent intent = new Intent(getActivity(), FilterActivity.class);
            startActivity(intent);
        }
        else if (item.getItemId() == R.id.menu_search)
        {
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
