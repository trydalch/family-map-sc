package ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.trevorrydalch.familymap.R;

import net.DataSyncTask;
import net.LoginTask;
import net.RegisterTask;

import commrequests.LoginRequest;
import commrequests.RegisterRequest;

/**
 * Created by trevor.rydalch on 6/7/17.
 */

/*
Fragment that contains text fields for the User to enter their information.

Calls either a Login or Register task, depending on the user input and which button is pressed.

On a successful Login or Register, a Datasync task starts and retrieves the data for the user who signed in.

On an unsuccessful login or Register, the user is allowed to try again.
 */

public class LoginFragment extends Fragment implements LoginTask.CallBack, DataSyncTask.CallBack, RegisterTask.CallBack
{
    private static String TAG = "LoginFragment";

    private EditText m_etServerHost;
    private EditText m_etServerPort;
    private EditText m_etUserName;
    private EditText m_etPassword;
    private EditText m_etFirstName;
    private EditText m_etLastName;
    private EditText m_etEmail;
    private RadioGroup m_rgGenderButtons;
    private Button m_bSignIn;
    private Button m_bRegister;

    private String m_ServerPort;
    private String m_ServerHost;
    private String m_UserName;
    private String m_Password;
    private String m_FirstName;
    private String m_LastName;
    private String m_Email;
    private String m_Gender;


    private Listener m_listener;


    public interface Listener {
        void onLogin();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    // Can be used to call methods on the hosting context
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Listener)
        {
            m_listener = (Listener) context;
        }
        else
        {
            throw new RuntimeException(context.toString() + " must implement LoginFragment.Listener");
        }
    }

    /*
    Inflates the views.

    Sets listeners.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        m_etServerHost = (EditText) view.findViewById(R.id.server_host_box);
        m_etServerPort = (EditText) view.findViewById(R.id.server_port_box);
        m_etUserName = (EditText) view.findViewById(R.id.userName_box);
        m_etPassword = (EditText) view.findViewById(R.id.password_box);
        m_etFirstName = (EditText) view.findViewById(R.id.fname_box);
        m_etLastName = (EditText) view.findViewById(R.id.lname_box);
        m_etEmail = (EditText) view.findViewById(R.id.email_box);
        m_rgGenderButtons = (RadioGroup) view.findViewById(R.id.group_gender);
        m_bSignIn = (Button) view.findViewById(R.id.sign_in_button);
        m_bRegister = (Button) view.findViewById(R.id.register_button);

        m_listener = (Listener) getActivity();

        m_bSignIn.setEnabled(false);
        m_bRegister.setEnabled(false);

<<<<<<< HEAD
        m_etServerHost.setText("10.21.21.167");
        m_etServerPort.setText("8080");
        m_etUserName.setText("anan");
        m_etPassword.setText("password");
=======
//        m_etServerHost.setText("10.24.67.41");
//        m_etServerPort.setText("8080");
//        m_etUserName.setText("sheila");
//        m_etPassword.setText("parker");
>>>>>>> 1b25358f25afd51c78c5a939bdf153b19b00c9db

        if (checkLoginRequirements())
        {
            m_bSignIn.setEnabled(true);
        }
        if (checkRegisterRequirements())
        {
            m_bRegister.setEnabled(true);
        }

        m_bSignIn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v)
            {

                m_ServerHost = m_etServerHost.getText().toString();
                m_ServerPort = m_etServerPort.getText().toString();

                setUserNamePassword();

                LoginRequest request = new LoginRequest();
                request.setUserName(m_UserName);
                request.setPassword(m_Password);

                startLoginTask(request);
            }
        });

        m_bRegister.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                m_ServerHost = m_etServerHost.getText().toString();
                m_ServerPort = m_etServerPort.getText().toString();
                setUserNamePassword();
                m_FirstName = m_etFirstName.getText().toString();
                m_LastName = m_etLastName.getText().toString();
                m_Email = m_etEmail.getText().toString();
                Log.d(TAG, "Server Host: " + m_ServerHost);
                int i_gen = m_rgGenderButtons.getCheckedRadioButtonId();
                if (i_gen == R.id.male_button)
                {
                    m_Gender = "m";
                    RegisterRequest request = makeRegisterRequest();
                    startRegisterTask(request);
                }
                else if (i_gen == R.id.female_button)
                {
                    m_Gender = "f";
                    RegisterRequest request = makeRegisterRequest();
                    startRegisterTask(request);
                }
                else if (i_gen == -1)
                {
                    int id = R.string.err_blank_gender;
                    makeToast(id);
                }
            }
        });

        m_etServerHost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //do nothing
            }

            // Check if fields are filled for Login and/or Register
            @Override
            public void afterTextChanged(Editable s) {
                if (checkLoginRequirements())
                {
                    m_bSignIn.setEnabled(true);
                }
                else
                {
                    m_bSignIn.setEnabled(false);
                }
                if (checkRegisterRequirements())
                {
                    m_bRegister.setEnabled(true);
                }
                else
                {
                    m_bRegister.setEnabled(false);
                }
            }
        });

        m_etServerPort.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //do nothing
            }

            // Check if fields are filled for Login and/or Register
            @Override
            public void afterTextChanged(Editable s) {
                if (checkLoginRequirements())
                {
                    m_bSignIn.setEnabled(true);
                }
                else
                {
                    m_bSignIn.setEnabled(false);
                }
                if (checkRegisterRequirements())
                {
                    m_bRegister.setEnabled(true);
                }
                else
                {
                    m_bRegister.setEnabled(false);
                }
            }
        });

        m_etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            // Check if fields are filled for Login and/or Register
            @Override
            public void afterTextChanged(Editable s) {
                if (checkLoginRequirements())
                {
                    m_bSignIn.setEnabled(true);
                }
                else
                {
                    m_bSignIn.setEnabled(false);
                }
                if (checkRegisterRequirements())
                {
                    m_bRegister.setEnabled(true);
                }
                else
                {
                    m_bRegister.setEnabled(false);
                }
            }
        });

        m_etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            // Check if fields are filled for Login and/or Register
            @Override
            public void afterTextChanged(Editable s) {
                if (checkLoginRequirements())
                {
                    m_bSignIn.setEnabled(true);
                }
                else
                {
                    m_bSignIn.setEnabled(false);
                }
                if (checkRegisterRequirements())
                {
                    m_bRegister.setEnabled(true);
                }
                else
                {
                    m_bRegister.setEnabled(false);
                }
            }
        });

        m_etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            // Check if fields are filled for Login and/or Register
            @Override
            public void afterTextChanged(Editable s) {
                if (checkRegisterRequirements())
                {
                    m_bRegister.setEnabled(true);
                }
                else
                {
                    m_bRegister.setEnabled(false);
                }
            }
        });

        m_etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            // Check if fields are filled for Login and/or Register
            @Override
            public void afterTextChanged(Editable s) {
                if (checkRegisterRequirements())
                {
                    m_bRegister.setEnabled(true);
                }
                else
                {
                    m_bRegister.setEnabled(false);
                }
            }
        });

        m_etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            // Check if fields are filled for Login and/or Register
            @Override
            public void afterTextChanged(Editable s) {
                if (checkRegisterRequirements())
                {
                    m_bRegister.setEnabled(true);
                }
                else
                {
                    m_bRegister.setEnabled(false);
                }
            }
        });

        m_rgGenderButtons.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkRegisterRequirements())
                {
                    m_bRegister.setEnabled(true);
                }
                else
                {
                    m_bRegister.setEnabled(false);
                }
            }
        });

        return view;
    }

    /*
    Makes the request for the RegisterTask
     */
    private RegisterRequest makeRegisterRequest()
    {
        RegisterRequest request = new RegisterRequest();

        request.setEmail(m_Email);
        request.setGender(m_Gender);
        request.setFirstName(m_FirstName);
        request.setLastName(m_LastName);
        request.setUserName(m_UserName);
        request.setPassword(m_Password);
        return request;
    }

    private void setUserNamePassword()
    {
        m_UserName = m_etUserName.getText().toString();
        m_Password = m_etPassword.getText().toString();
    }

    /*
    Starts an Async Login Task
     */
    private void startLoginTask(LoginRequest request)
    {
        LoginTask loginTask = new LoginTask(this, m_ServerHost, m_ServerPort);
        loginTask.execute(request);
    }

    /*
    Starts an Async Register Task
     */
    private void startRegisterTask(RegisterRequest request)
    {
        RegisterTask registerTask = new RegisterTask(this, m_ServerHost, m_ServerPort);
        registerTask.execute(request);
        registerTask.getStatus();
    }

    /*
    Checks if the required fields for Login have values

    if true, the Sign-In button is enabled.
    Otherwise, it remains disabled.
     */
    private boolean checkLoginRequirements()
    {
        boolean allRequirements = false;
        if (!m_etServerHost.getText().toString().isEmpty() && !m_etServerPort.getText().toString().isEmpty() &&
                !m_etUserName.getText().toString().isEmpty() && !m_etPassword.getText().toString().isEmpty())
        {
            allRequirements = true;
        }
        return allRequirements;
    }

    /*
    Checks if the required fields for Login have values

    if true, the Register button is enabled.
    Otherwise, it remains disabled.
     */
    private boolean checkRegisterRequirements()
    {
        boolean allRequirements = false;
        if (checkLoginRequirements() && !m_etFirstName.getText().toString().isEmpty() && !m_etLastName.getText().toString().isEmpty() && !m_etEmail.getText().toString().isEmpty() && m_rgGenderButtons.getCheckedRadioButtonId() > 0)
        {
            allRequirements = true;
        }
        return allRequirements;
    }

    public void makeToast(int id)
    {
        Toast.makeText(this.getActivity(), id, Toast.LENGTH_SHORT).show();
    }

    public void onDataSync()
    {
        m_listener.onLogin();
    }
}
