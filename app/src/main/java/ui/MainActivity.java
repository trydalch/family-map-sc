package ui;

import android.net.Uri;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.trevorrydalch.familymap.R;

import model.Model;

/*
Hosts the Login and Map fragments, instantiates the Model.SINGLETON
 */
public class MainActivity extends AppCompatActivity implements LoginFragment.Listener, MapFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragManager = getSupportFragmentManager();
        Fragment fragment = fragManager.findFragmentById(R.id.fragment_container_main);

        // Instantiate Model.Singleton
        Model instance = Model.getInstance();

        if (Model.SINGLETON.getUser().getPersonID() == null)
        {
            fragment = new LoginFragment();
            fragManager.beginTransaction().add(R.id.fragment_container_main, fragment).commit();
        }
        else
        {
            fragment = new MapFragment();
            fragManager.beginTransaction().replace(R.id.fragment_container_main, fragment).commit();
        }
    }

    public void onLogin()
    {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container_main);

        if (fragment == null)
        {

        }
        else if (fragment.getClass().equals(LoginFragment.class))
        {
            fragment = new MapFragment();
            fm.beginTransaction().replace(R.id.fragment_container_main, fragment).commit();
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
