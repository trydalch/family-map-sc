package ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.trevorrydalch.familymap.R;

import net.DataSyncTask;

import commrequests.EventRequest;
import commrequests.PersonRequest;
import model.Model;
import model.Settings;

public class SettingsActivity extends AppCompatActivity  implements  DataSyncTask.CallBack {

    Switch m_switchLifeStoryLines;
    Switch m_switchSpouseLines;
    Switch m_switchFamilyTreeLines;
    Spinner m_spinLifeStoryColor;
    Spinner m_spinSpouseLines;
    Spinner m_spinFamilyTreeColor;
    Spinner m_spinMapType;
    RelativeLayout m_rellayoutReSyncData;
    RelativeLayout m_rellayoutLogout;

    Context m_context = this;
    DataSyncTask.CallBack m_DataCallBack = this;

    Settings m_settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        m_switchLifeStoryLines = (Switch) findViewById(R.id.switch_life_story);
        m_switchSpouseLines = (Switch) findViewById(R.id.switch_spouse_lines);
        m_switchFamilyTreeLines = (Switch) findViewById(R.id.switch_family_tree_lines);
        m_spinLifeStoryColor = (Spinner) findViewById(R.id.spin_life_story_lines_color);
        m_spinSpouseLines = (Spinner) findViewById(R.id.spin_spouse_lines);
        m_spinFamilyTreeColor = (Spinner) findViewById(R.id.spin_fam_tree_lines_color);
        m_spinMapType = (Spinner) findViewById(R.id.spin_map_type);
        m_rellayoutReSyncData = (RelativeLayout) findViewById(R.id.rel_layout_resync);
        m_rellayoutLogout = (RelativeLayout) findViewById(R.id.rel_layout_logout);


        m_rellayoutReSyncData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataSyncTask dataSyncTask = new DataSyncTask(m_DataCallBack, Model.SINGLETON.getHost(), Model.SINGLETON.getPort());
                PersonRequest personRequest = new PersonRequest();
                EventRequest eventRequest = new EventRequest();
                personRequest.setAuthToken(Model.SINGLETON.getAuthToken());
                eventRequest.setAuthToken(Model.SINGLETON.getAuthToken());
                dataSyncTask.execute(personRequest, eventRequest);
            }
        });

        m_rellayoutLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Model.SINGLETON.getUser().setPersonID(null);

                Intent intent = new Intent(m_context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        UpdateSettings();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SaveSettings();
        Log.i("SettingsActivity", "onPause");
    }

    private void UpdateSettings()
    {
        m_settings = Model.SINGLETON.getSettings();
        SetLifeStoryColor(m_settings.getLifeStoryLinesColor());
        SetSpouseLinesColor(m_settings.getSpouseLinesColor());
        SetFamilyTreeColor(m_settings.getFamilyTreeColor());
        SetShowLifeStoryLines(m_settings.isShowLifeStoryLines());
        SetShowSpouseLines(m_settings.isShowSpouseLines());
        SetShowFamTreeLines(m_settings.isShowFamTreeLines());
        SetMapType(m_settings.getMapType());
    }

    private void SetSpouseLinesColor(Settings.Color spouseLinesColor)
    {
        m_spinSpouseLines.setSelection(getIntPositionFromColorEnum(spouseLinesColor));
    }

    private int getIntPositionFromColorEnum(Settings.Color color) {
        switch (color)
        {
            case GREEN:
                return 0;
            case BLUE:
                return 1;
            case RED:
                return 2;
            case YELLOW:
                return 3;
            case WHITE:
                return 4;
            default:
                return 0;
        }

    }

    private void SetMapType(Settings.MapType type) {
        switch(type)
        {
            case NORMAL:
                m_spinMapType.setSelection(0);
                break;
            case HYBRID:
                m_spinMapType.setSelection(1);
                break;
            case SATELLITE:
                m_spinMapType.setSelection(2);
                break;
            case TERRAIN:
                m_spinMapType.setSelection(3);
                break;
            default:
                m_spinMapType.setSelection(0);
        }
    }

    private void SetShowFamTreeLines(boolean showFamTreeLines)
    {
        m_switchFamilyTreeLines.setChecked(showFamTreeLines);
    }

    private void SetShowLifeStoryLines(boolean showLifeStoryLines)
    {
        m_switchLifeStoryLines.setChecked(showLifeStoryLines);
    }

    private void SetShowSpouseLines(boolean showSpouseLines)
    {
        m_switchSpouseLines.setChecked(showSpouseLines);
    }

    private void SetFamilyTreeColor(Settings.Color color)
    {
        m_spinFamilyTreeColor.setSelection(getIntPositionFromColorEnum(color));
    }

    private void SetLifeStoryColor(Settings.Color color)
    {
        m_spinLifeStoryColor.setSelection(getIntPositionFromColorEnum(color));
    }

    private void SaveSettings()
    {
        m_settings.setShowLifeStoryLines(m_switchLifeStoryLines.isChecked());
        m_settings.setShowSpouseLines(m_switchSpouseLines.isChecked());
        m_settings.setShowFamTreeLines(m_switchFamilyTreeLines.isChecked());
        m_settings.setLifeStoryLines(getColorFromIntPosition(m_spinLifeStoryColor));
        m_settings.setSpouseLinesColor(getColorFromIntPosition(m_spinSpouseLines));
        m_settings.setFamilyTreeColor(getColorFromIntPosition(m_spinFamilyTreeColor));
        m_settings.setMapType(getMapType(m_spinMapType));

        Model.SINGLETON.setSettings(m_settings);
    }

    private Settings.MapType getMapType(Spinner spin)
    {
        int id = spin.getSelectedItemPosition();
        switch(id)
        {
            case 0:
                return Settings.MapType.NORMAL;
            case 1:
                return Settings.MapType.HYBRID;
            case 2:
                return Settings.MapType.SATELLITE;
            case 3:
                return Settings.MapType.TERRAIN;
            default:
                return Settings.MapType.NORMAL;
        }
    }

    private Settings.Color getColorFromIntPosition(Spinner spin)
    {
        int id = spin.getSelectedItemPosition();
        switch(id)
        {
            case 0:
                return Settings.Color.GREEN;
            case 1:
                return Settings.Color.BLUE;
            case 2:
                return Settings.Color.RED;
            case 3:
                return Settings.Color.YELLOW;
            case 4:
                return Settings.Color.WHITE;
        }
        return null;
    }


    @Override
    public void makeToast(int id) {
        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onDataSync() {
        finish();
    }
}
