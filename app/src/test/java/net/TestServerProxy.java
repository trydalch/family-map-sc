package net;

import com.google.gson.Gson;

import org.junit.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

import commrequests.EventRequest;
import commrequests.LoginRequest;
import commrequests.PersonRequest;
import commrequests.RegisterRequest;
import commresults.EventResult;
import commresults.LoginResult;
import commresults.PersonResult;
import commresults.RegisterResult;

import static org.junit.Assert.*;

/**
 * Created by trevor.rydalch on 6/19/17.
 */

public class TestServerProxy
{
<<<<<<< HEAD
=======
    private String m_Host;
    private String m_Port;
    private String m_AuthToken = "FAKE";
    private static final int m_NUM_PPL_4GENERATIONS = 31;
    private static final int m_NUM_EVNT_4GENERATIONS = 153;

    @Before
    public void startUp()
    {
        try
        {
            m_Host = Inet4Address.getLocalHost().getHostAddress().toString();
            m_Port = "8080";

        }
        catch (UnknownHostException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void testRegister() throws MalformedURLException
    {

        //This user is used every time. Successfully registered once, but now returns the message
        // "username is taken" and the auth token is null.
        RegisterRequest request = new RegisterRequest();
        request.setUserName("test");
        request.setPassword("test");
        request.setFirstName("test");
        request.setLastName("test");
        request.setEmail("test@test.test");
        request.setGender("m");

        RegisterResult result = new RegisterResult();
        try
        {
            //Create a URL indicating where the server is running and which web API operation to call
            URL url = new URL("http://" + m_Host + ":" + m_Port + "/user/register");

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            connection.setRequestMethod("POST");

            // Indicate that this request will contain an HTTP request body
            connection.setDoOutput(true);

            // Specify that we would like to receive the server's response in JSON
            // format by putting an HTTP "Accept" header on the request (this is not
            // necessary because our server only returns JSON responses, but it
            // provides one more example of how to add a header to an HTTP request).
            connection.addRequestProperty("Accept", "application/json");

            // Connect to the server and send the HTTP request
//            connection.connect();

            Gson gson = new Gson();
            String reqData = gson.toJson(request);

            // Get the output stream containing the HTTP request body
            OutputStream reqBody = connection.getOutputStream();
            // Write the JSON data to the request body
            writeString(reqData, reqBody);
            // Close the request body output stream, indicating that the
            // request is complete
            reqBody.close();

            // By the time we get here, the HTTP response has been received from the server.
            // Check to make sure that the HTTP response from the server contains a 200
            // status code, which means "success".  Treat anything else as a failure.
            int Response = connection.getResponseCode();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // The HTTP response status code indicates success
                // Get the input stream containing the HTTP response body
                InputStream respBody = connection.getInputStream();
                // Extract JSON data from the HTTP response body
                String respData = readString(respBody);
                result = gson.fromJson(respData, RegisterResult.class);
            }
            else {
                // The HTTP response status code indicates an error
                // occurred, so print out the message from the HTTP response
                System.out.println("ERROR: " + connection.getResponseMessage());
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        assertEquals(null, result.getAuthToken());
    }

    @Test
    public void testLogin()
    {
        LoginRequest request = new LoginRequest();
        request.setUserName("test");
        request.setPassword("test");

        LoginResult result = new LoginResult();
        try
        {
            // Create a URL indicating where the server is running, and which
            // web API operation we want to call
            URL url = new URL("http://" + m_Host + ":" + m_Port + "/user/login");

            // Start constructing our HTTP request
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            connection.setRequestMethod("POST");

            // Indicate that this request will contain an HTTP request body
            connection.setDoOutput(true);

            // Specify that we would like to receive the server's response in JSON
            // format by putting an HTTP "Accept" header on the request (this is not
            // necessary because our server only returns JSON responses, but it
            // provides one more example of how to add a header to an HTTP request).
            connection.addRequestProperty("Accept", "application/json");

            // Connect to the server and send the HTTP request
//            connection.connect();

            Gson gson = new Gson();
            String reqData = gson.toJson(request);

            // Get the output stream containing the HTTP request body
            OutputStream reqBody = connection.getOutputStream();
            // Write the JSON data to the request body
            writeString(reqData, reqBody);
            // Close the request body output stream, indicating that the
            // request is complete
            reqBody.close();

            // By the time we get here, the HTTP response has been received from the server.
            // Check to make sure that the HTTP response from the server contains a 200
            // status code, which means "success".  Treat anything else as a failure.
            int Response = connection.getResponseCode();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // The HTTP response status code indicates success
                // Get the input stream containing the HTTP response body
                InputStream respBody = connection.getInputStream();
                // Extract JSON data from the HTTP response body
                String respData = readString(respBody);
                result = gson.fromJson(respData, LoginResult.class);
            }
            else {
                // The HTTP response status code indicates an error
                // occurred, so print out the message from the HTTP response
                System.out.println("ERROR: " + connection.getResponseMessage());
            }
        }
        catch(IOException e)
        {

            System.out.println(e.getMessage());
        }

        m_AuthToken = result.getAuthToken();
        assertNotEquals(null, result.getAuthToken());
    }

    @Test
    public void testPerson()
    {
        testLogin();
        PersonRequest request = new PersonRequest();
        request.setAuthToken(m_AuthToken);

        PersonResult result = new PersonResult();
        try
        {
            // Create a URL indicating where the server is running, and which
            // web API operation we want to call
            URL url = new URL("http://" + m_Host + ":" + m_Port + "/person");

            // Start constructing our HTTP request
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            connection.setRequestMethod("GET");

            connection.addRequestProperty("Authorization", request.getAuthToken());

            // Indicate that this request will contain an HTTP request body
            connection.setDoOutput(false);

            // Specify that we would like to receive the server's response in JSON
            // format by putting an HTTP "Accept" header on the request (this is not
            // necessary because our server only returns JSON responses, but it
            // provides one more example of how to add a header to an HTTP request).
            connection.addRequestProperty("Accept", "application/json");
            Gson gson = new Gson();
//            String reqData = gson.toJson(request);

            // Get the output stream containing the HTTP request body
//            OutputStream reqBody = connection.getOutputStream();
            // Write the JSON data to the request body
//            writeString(reqData, reqBody);
            // Close the request body output stream, indicating that the
            // request is complete
//            reqBody.close();

            connection.connect();

            // By the time we get here, the HTTP response has been received from the server.
            // Check to make sure that the HTTP response from the server contains a 200
            // status code, which means "success".  Treat anything else as a failure.
//            int Response = connection.getResponseCode();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // The HTTP response status code indicates success
                // Get the input stream containing the HTTP response body
                InputStream respBody = connection.getInputStream();
                // Extract JSON data from the HTTP response body
                String respData = readString(respBody);
                result = gson.fromJson(respData, PersonResult.class);
            }
            else {
                // The HTTP response status code indicates an error
                // occurred, so print out the message from the HTTP response
                System.out.println("ERROR: " + connection.getResponseMessage());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        if (!m_AuthToken.equals("FAKE"))
        {
            assertEquals(m_NUM_PPL_4GENERATIONS, result.getData().length);
        }
    }

    @Test
    public void testEvent()
    {
        testLogin();
        EventRequest request = new EventRequest();
        request.setAuthToken(m_AuthToken);

        EventResult result = new EventResult();
        try
        {
            // Create a URL indicating where the server is running, and which
            // web API operation we want to call
            URL url = new URL("http://" + m_Host + ":" + m_Port + "/event");

            // Start constructing our HTTP request
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            connection.setRequestMethod("GET");

            connection.addRequestProperty("Authorization", request.getAuthToken());

            // Indicate that this request will contain an HTTP request body
            connection.setDoOutput(false);

            // Specify that we would like to receive the server's response in JSON
            // format by putting an HTTP "Accept" header on the request (this is not
            // necessary because our server only returns JSON responses, but it
            // provides one more example of how to add a header to an HTTP request).
            connection.addRequestProperty("Accept", "application/json");
            Gson gson = new Gson();
//            String reqData = gson.toJson(request);

            // Get the output stream containing the HTTP request body
//            OutputStream reqBody = connection.getOutputStream();
            // Write the JSON data to the request body
//            writeString(reqData, reqBody);
            // Close the request body output stream, indicating that the
            // request is complete
//            reqBody.close();

            // By the time we get here, the HTTP response has been received from the server.
            // Check to make sure that the HTTP response from the server contains a 200
            // status code, which means "success".  Treat anything else as a failure.
            int Response = connection.getResponseCode();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // The HTTP response status code indicates success
                // Get the input stream containing the HTTP response body
                InputStream respBody = connection.getInputStream();
                // Extract JSON data from the HTTP response body
                String respData = readString(respBody);
                result = gson.fromJson(respData, EventResult.class);
            }
            else {
                // The HTTP response status code indicates an error
                // occurred, so print out the message from the HTTP response
                System.out.println("ERROR: " + connection.getResponseMessage());
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        assertEquals(m_NUM_EVNT_4GENERATIONS, result.getData().length);

    }

    /*
    The readString method shows how to read a String from an InputStream.
*/
    private static String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    /*
        The writeString method shows how to write a String to an OutputStream.
    */
    private static void writeString(String str, OutputStream os) throws IOException {
        OutputStreamWriter sw = new OutputStreamWriter(os);
        sw.write(str);
        sw.flush();
    }
>>>>>>> 1b25358f25afd51c78c5a939bdf153b19b00c9db

}
