drop table if exists User;
drop table if exists Person;
drop table if exists Event;
drop table if exists AuthToken;

create table if not exists User
(
    username text not null primary key,
    password text not null,
    email text not null,
    firstName text not null,
    lastName text not null,
    gender text,
    personID text not null unique
);

create table if not exists Person
(
    firstName text not null,
    lastName text not null,
    gender text,
    personID text not null primary key,
    father text,
    mother text,
    spouse text,
    descendant text,
    generation number
);

create table if not exists Event
(
    eventID text not null primary key,
    descendant text not null,
    personID text not null,
    latitude double,
    longitude double,
    country text,
    city text,
    eventType text,
    year number
);

create table if not exists AuthToken
(
    authToken text not null primary key,
    personId text not null,
    timeCreated number
);
