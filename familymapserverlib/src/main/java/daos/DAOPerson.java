package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

import exceptions.DatabaseException;
import model.Person;

/**
 * A data access object to add, delete, get, and clear Person objects from the SQLite database
 */

public class DAOPerson
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private Connection m_conn;

    public Connection getM_conn() {
        return m_conn;
    }

    public void setM_conn(Connection m_conn) {
        this.m_conn = m_conn;
    }
    /**
     * Adds a person to the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid Person object with all members initialized.
     * </pre>
     * @param pers the Person object to add to database
     * @return true if successfully added Person to database, false otherwise
     */
    public boolean addPerson(Person pers) throws DatabaseException
    {
        logger.entering("DAOPerson", "addPerson");
        assert m_conn != null;
        PreparedStatement stmt = null;
        try
        {
            String sql = "insert into Person " +"values (?,?,?,?,?,?,?,?,?)";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, pers.getFirstName());
            stmt.setString(2, pers.getLastName());
            stmt.setString(3, pers.getGender());
            stmt.setString(4, pers.getPersonID());
            stmt.setString(5, pers.getFather());
            stmt.setString(6, pers.getMother());
            stmt.setString(7, pers.getSpouse());
            stmt.setString(8, pers.getDescendant());
            stmt.setInt(9, pers.getGeneration());

            if (stmt.executeUpdate() == 1)
            {
                logger.exiting("DAOPerson", "addPerson");
                return true;
            }
            else
            {
                logger.exiting("DAOPerson", "addPerson");
                return false;
            }
        }
        catch (SQLException e)
        {
            logger.exiting("DAOPerson", "addPerson");
            if (e.getMessage().contains("gender may not be NULL"))
            {
                throw new DatabaseException("gender is NULL", e);
            }
            throw new DatabaseException("addPerson failed", e);
        }
    }

    /**
     * Deletes a person from the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid Person object with all members initialized.
     * </pre>
     * @param sPersID the id of the Person to delete
     * @return true if successfully deleted Person from database, false otherwise
     */
    public boolean deletePerson(String sPersID)
    {
        logger.entering("DAOPerson", "deletePerson");
        logger.exiting("DAOPerson", "deletePerson");
        return false;
    }

    public boolean deletePersonByUserName(String username) throws DatabaseException
    {
        logger.entering("DAOPerson", "deltePersonByUserName");
        PreparedStatement stmt = null;
        try
        {
            String sql = "DELETE FROM Person WHERE descendant = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.execute();
            logger.exiting("DAOPerson", "deletePersonByUserName");
            return true;
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Internal Server Error", e);
        }
    }

    public Person[] getPersonsByUserName(String userName) throws  DatabaseException
    {
        logger.entering("DAOPerson", "getPersonsByUserName");
        ArrayList<Person> persons = new ArrayList<>();
        Person[] personArray = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try
        {
            String sql = "SELECT personID, firstName, lastName, gender, father, mother, spouse, descendant, generation FROM Person WHERE descendant = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, userName);

            resultSet = stmt.executeQuery();
            while (resultSet.next())
            {
                Person pers = new Person();
                pers.setPersonID(resultSet.getString(1));
                pers.setFirstName(resultSet.getString(2));
                pers.setLastName(resultSet.getString(3));
                pers.setGender(resultSet.getString(4));
                pers.setFather(resultSet.getString(5));
                pers.setMother(resultSet.getString(6));
                pers.setSpouse(resultSet.getString(7));
                pers.setDescendant(resultSet.getString(8));
                pers.setGeneration(resultSet.getInt(9));

                persons.add(pers);
            }

            int i = 0;
            personArray = new Person[persons.size()];
            for (Person p : persons)
            {
                personArray[i] = p;
                i++;
            }
        }
        catch (SQLException e)
        {
            logger.warning("");
            throw new DatabaseException("getPersonsByUserName failed", e);
        }
        logger.exiting("DAOPerson", "getPersonsByUserName");
        return personArray;
    }

    /**
     * Gets a Person from the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid Person object with all members initialized.
     * </pre>
     * @param sPersID the id of the Person to delete
     * @return the Person object, initialized with values from database. Null if Person does not exist.
     */
    public Person getPerson(String sPersID) throws DatabaseException
    {
        logger.entering("DAOPerson", "getPerson");
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        Person pers = new Person();
        try
        {
            String sql = "SELECT firstName, lastName, gender, personID, father, mother, spouse, descendant, generation FROM Person WHERE personID = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, sPersID);

            resultSet = stmt.executeQuery();

            pers.setFirstName(resultSet.getString(1));
            pers.setLastName(resultSet.getString(2));
            pers.setGender(resultSet.getString(3));
            pers.setPersonID(resultSet.getString(4));
            pers.setFather(resultSet.getString(5));
            pers.setMother(resultSet.getString(6));
            pers.setSpouse(resultSet.getString(7));
            pers.setDescendant(resultSet.getString(8));
            pers.setGeneration(resultSet.getInt(9));

            resultSet.close();
            stmt.close();
        }
        catch (SQLException e)
        {
            logger.exiting("DAOPerson", "getPerson");
            if (e.getMessage().equals("ResultSet closed"))
            {
                throw new DatabaseException("Invalid PersonID", e);
            }
            throw new DatabaseException("getPerson failed", e);
        }
        logger.exiting("DAOPerson", "getPerson");
        return pers;
    }

    /**
     * Clears all Person from database
     * <pre>
     *     <b>Constraints on the input</b>:
     *     None
     * </pre>
     * @return true if all Person were deleted, false otherwise.
     */
    public boolean clear() throws DatabaseException
    {
        logger.entering("DAOPerson", "clear");
        PreparedStatement query = null;
        PreparedStatement delete = null;
        try
        {
            String sqlQuery = "SELECT personID FROM Person";
            query = m_conn.prepareStatement(sqlQuery);
            ResultSet resultSet = query.executeQuery();
            if (resultSet.next())
            {
                String sqlDelete = "DELETE FROM Person";
                delete = m_conn.prepareStatement(sqlDelete);
                if (delete.executeUpdate() > 0)
                {
                    logger.exiting("DAOPerson", "clear");
                    return true;
                }
                else
                {
                    logger.exiting("DAOPerson", "clear");
                    return false;
                }
            }
            else
            {
                logger.exiting("DAOPerson", "clear");
                return true;
            }
        }
        catch (SQLException e)
        {
            logger.exiting("DAOPerson", "clear");
            throw new DatabaseException("Internal Server Error: clear DAOPerson", e);
        }
    }
}
