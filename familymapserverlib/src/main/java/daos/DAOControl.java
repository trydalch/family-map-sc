package daos;

import java.io.File;
import java.sql.*;
import java.sql.Connection;
import java.util.logging.Logger;

import exceptions.DatabaseException;
import model.*;

/**
 * This class is used to control and share connections among all DAO classes in order to make service operations atomic when multiple DAO objects are used.
 */
public class DAOControl
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    static
    {
        try {
            final String driver = "org.sqlite.JDBC";
            Class.forName(driver);
        }
        catch(ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }
    private DAOAuthToken m_daoAuth = new DAOAuthToken();
    private DAOEvent m_daoEvent = new DAOEvent();
    private DAOPerson m_daoPers = new DAOPerson();
    private DAOUser m_daoUsr = new DAOUser();

    private File m_DBCreateFile = new File("dbSchema.txt");

    // DOCUMENT HERE
    private Connection m_conn;

    private String m_dbName = "data" + File.separator + "fmsdb.sqlite";

    public String getDbName()
    {
        return m_dbName;
    }

    public void setDbName(String dbName)
    {
        this.m_dbName = dbName;
    }

    /**
     * Opens a connection to the database, then shares that connection with a
     * DAOUser object, DAOAuthToken object, DAOEvent object, and a DAOPerson object.
     *
     * @throws DatabaseException
     */
    public void openConnection() throws DatabaseException
    {
        try
        {
            final String CONNECTION_URL = "jdbc:sqlite:" + m_dbName;

            // Open a database connection
            m_conn = DriverManager.getConnection(CONNECTION_URL);

            // Start a transaction
            m_conn.setAutoCommit(false);

            m_daoUsr.setM_conn(m_conn);
            m_daoAuth.setM_conn(m_conn);
            m_daoEvent.setM_conn(m_conn);
            m_daoPers.setM_conn(m_conn);
        }
        catch(SQLException e)
        {
            throw new DatabaseException("openConnection failed", e);
        }
    }

    /**
     * Closes the connection opened in the openConnection method.
     *
     * Because it is shared with the DAO subclasses, they no longer have a connection to the Database.
     * @param commit
     * @throws DatabaseException
     */
    public void closeConnection(boolean commit)
    {
        try
        {
            if (commit)
            {
                m_conn.commit();
            }
            else
            {
                m_conn.rollback();
            }

            m_conn.close();
            m_conn = null;
        }
        catch(SQLException e)
        {

        }
    }


    public boolean isConnectionOpen()
    {
        try
        {
            if (m_conn == null)
            {
                return false;
            }
            return !m_conn.isClosed();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @param usr
     * @return
     * @throws DatabaseException
     */
    public boolean addUser(User usr) throws DatabaseException
    {
        return m_daoUsr.addUser(usr);
    }

    public boolean deleteUser(User usr) throws  DatabaseException
    {
        return m_daoUsr.deleteUser(usr);
    }

    public User getUser(String usrName) throws DatabaseException
    {
        return m_daoUsr.getUser(usrName);
    }

    public User getUserByID(String id) throws DatabaseException
    {
        return m_daoUsr.getUserByID(id);
    }

    public boolean addPerson(Person pers) throws DatabaseException
    {
        return m_daoPers.addPerson(pers);
    }

    public Person getPerson(String persID) throws DatabaseException
    {
        return m_daoPers.getPerson(persID);
    }

    public boolean deletePersonByUserName(String username) throws DatabaseException
    {
        return m_daoPers.deletePersonByUserName(username);
    }

    public boolean addAuthToken(AuthToken authToken) throws DatabaseException
    {
        return  m_daoAuth.addAuthToken(authToken);
    }

    public AuthToken getAuthToken(String authToken) throws DatabaseException
    {
        return m_daoAuth.getAuthToken(authToken);
    }

    public boolean deleteAuthToken(AuthToken authToken) throws DatabaseException
    {
        return m_daoAuth.deleteAuthToken(authToken);
    }

    public boolean addEvent(Event event) throws DatabaseException
    {
        return m_daoEvent.addEvent(event);
    }


    public Event getEvent(String eventID) throws DatabaseException
    {
        return m_daoEvent.getEvent(eventID);
    }

    public boolean deleteEventByEventID(String eventID) throws DatabaseException
    {
        return m_daoEvent.deleteEventByEventID(eventID);
    }

    public boolean deleteEventsByUserName(String username) throws DatabaseException
    {
        return m_daoEvent.deleteEventByUserName(username);
    }

    public boolean clear() throws DatabaseException
    {
        boolean eventClearSuccess = m_daoEvent.clear();
        boolean usrClearSuccess = m_daoUsr.clear();
        boolean persClearSuccess = m_daoPers.clear();
        boolean authClearSuccess = m_daoAuth.clear();

        return (eventClearSuccess && usrClearSuccess && persClearSuccess && authClearSuccess);
    }

    public Person[] getPersonsByUserName(String userName) throws DatabaseException
    {
        return m_daoPers.getPersonsByUserName(userName);
    }

    public Event[] getEventsByPersId(String persId) throws DatabaseException
    {
        return m_daoEvent.getEventsByPersId(persId);
    }
}
