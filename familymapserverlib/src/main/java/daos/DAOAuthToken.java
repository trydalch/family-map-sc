package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import exceptions.DatabaseException;
import model.AuthToken;

/**
 * A data access class to add, delete, get, and clear AuthToken objects from the SQLite database
 */

public class DAOAuthToken
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private Connection m_conn;

    public Connection getM_conn() {
        return m_conn;
    }

    public void setM_conn(Connection m_conn) {
        this.m_conn = m_conn;
    }
    /**
     * Adds a AuthToken to the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid AuthToken object with all members initialized.
     * </pre>
     * @param authToken the AuthToken object to add to database
     * @return true if successfully added AuthToken to database, false otherwise
     */
    public boolean addAuthToken(AuthToken authToken) throws DatabaseException
    {
        logger.entering("DAOAuthToken", "addAuthToken");
        assert this.m_conn != null;
        assert authToken != null;
        PreparedStatement stmt = null;
        try
        {
            String sql = "INSERT INTO AuthToken (authToken, personId, timeCreated) values (?,?,?)";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, authToken.getAuthId());
            stmt.setString(2, authToken.getPersId());
            stmt.setString(3, String.valueOf(authToken.getTimeCreated()));
            if (stmt.executeUpdate() == 1)
            {
                logger.exiting("DAOAuthToken", "addAuthToken");
                return true;
            }
            else
            {
                logger.exiting("DAOAuthToken", "addAuthToken");
                return false;
            }
        }
        catch (SQLException e)
        {
            logger.exiting("DAOAuthToken", "addAuthToken");
            throw new DatabaseException("addAuthToken Failed", e);
        }
    }

    /**
     * Deletes a AuthToken from the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid AuthToken object with all members initialized.
     * </pre>
     * @param authToken the AuthToken to delete
     * @return true if successfully deleted AuthToken from database, false otherwise
     */
    public boolean deleteAuthToken(AuthToken authToken) throws DatabaseException
    {
        logger.entering("DAOAuthToken", "deleteAuthToken");

        String id = authToken.getAuthId();
        PreparedStatement stmt = null;
        try
        {
            String sql = "DELETE FROM AuthToken WHERE authToken = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, id);
            if (stmt.executeUpdate() == 1)
            {
                logger.exiting("DAOAuthToken", "deleteAuthToken");
                return true;
            }
            else
            {
                logger.exiting("DAOAuthToken", "deleteAuthToken");
                return false;
            }
        }
        catch (SQLException e)
        {
            logger.warning("SQLException when deleting authtoken. Message: " + e.getMessage());
            logger.exiting("DAOAuthToken", "deleteAuthToken");
            throw new DatabaseException("deleteAuthToken failed", e);
        }
    }

    /**
     * Gets a AuthToken from the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid AuthToken object with all members initialized.
     * </pre>
     * @param sAuthTokID the id of the AuthToken to delete
     * @return the AuthToken object, initialized with values from database. Null if AuthToken does not exist.
     */
    public AuthToken getAuthToken(String sAuthTokID) throws DatabaseException
    {
        logger.entering("DAOAuthToken", "getAuthToken");
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        AuthToken authToken = new AuthToken();
        try
        {
            String sql = "SELECT authToken, personId, timeCreated FROM AuthToken WHERE authToken = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, sAuthTokID);

            resultSet = stmt.executeQuery();

            authToken.setAuthId(resultSet.getString(1));
            authToken.setPersId(resultSet.getString(2));
            authToken.setTimeCreated(resultSet.getLong(3));

            resultSet.close();
            stmt.close();
        }
        catch (SQLException e)
        {
            DatabaseException exception = new DatabaseException("");
            if (e.getMessage().equals("ResultSet closed"))
            {
                logger.exiting("DAOAuthToken", "getAuthToken");
                throw new DatabaseException("Invalid Authorization Token", e);
            }
            logger.exiting("DAOAuthToken", "getAuthToken");
            throw new DatabaseException("getAuthTokenFailed", e);
        }
        logger.exiting("DAOAuthToken", "getAuthToken");
        return authToken;
    }

    /**
     * Clears all AuthToken from database
     * <pre>
     *     <b>Constraints on the input</b>:
     *     None
     * </pre>
     * @return true if all AuthToken were deleted, false otherwise.
     */
    public boolean clear() throws  DatabaseException
    {
        logger.entering("DAOAuthToken", "clear");
        PreparedStatement query = null;
        PreparedStatement delete = null;
        try
        {
            String sqlQuery = "SELECT authToken FROM AuthToken";
            query = m_conn.prepareStatement(sqlQuery);
            ResultSet resultSet = query.executeQuery();
            if (resultSet.next())
            {
                String sqlDelete = "DELETE FROM AuthToken";
                delete = m_conn.prepareStatement(sqlDelete);
                if (delete.executeUpdate() > 0)
                {
                    logger.exiting("DAOAuthToken", "clear");
                    return true;
                }
                else
                {
                    logger.exiting("DAOAuthToken", "clear");
                    return false;
                }
            }
            else
            {
                logger.exiting("DAOAuthToken", "clear");
                return true;
            }
        }
        catch (SQLException e)
        {
            logger.exiting("DAOAuthToken", "clear");
            throw new DatabaseException("Internal Server Error: clear DAOAuthToken", e);
        }
    }
}
