package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

import exceptions.DatabaseException;
import model.Event;

/**
 * A data access class to add, delete, get, and clear Event objects from the SQLite database.
 */

public class DAOEvent
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private Connection m_conn;

    public Connection getM_conn() {
        return m_conn;
    }

    public void setM_conn(Connection m_conn) {
        this.m_conn = m_conn;
    }
    /**
     * Adds a Event to the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid Event object with all members initialized.
     * </pre>
     * @param event the Event object to add to database
     * @return true if successfully added Event to database, false otherwise
     */
    public boolean addEvent(Event event) throws DatabaseException
    {
//        logger.entering("DAOEvent", "addEvent");
        PreparedStatement stmt = null;
        try
        {
            String sql = "INSERT INTO Event (eventID, descendant, personID, latitude, longitude, country, city, eventType, year) values (?,?,?,?,?,?,?,?,?)";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, event.getEventID());
            stmt.setString(2, event.getDescendant());
            stmt.setString(3, event.getPersonID());
            stmt.setString(4, String.valueOf(event.getLatitude()));
            stmt.setString(5, String.valueOf(event.getLongitude()));
            stmt.setString(6, event.getCountry());
            stmt.setString(7, event.getCity());
            stmt.setString(8, event.getEventType());
            stmt.setString(9, String.valueOf(event.getYear()));

            int num = stmt.executeUpdate();
            if (num == 1)
            {
//                logger.exiting("DAOEvent", "addEvent");
                return true;
            }
            else
            {
//                logger.exiting("DAOEvent", "addEvent");
                return false;
            }
        }
        catch (SQLException e)
        {
            logger.warning("SQLException when adding event" + e.getStackTrace());
            logger.exiting("DAOEvent", "addEvent");
            throw new DatabaseException("addEvent failed", e);
        }
    }

    /**
     * Deletes a Event from the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid Event object with all members initialized.
     * </pre>
     * @param sEventID the id of the Event to delete
     * @return true if successfully deleted Event from database, false otherwise
     */
    public boolean deleteEventByEventID(String sEventID)
    {
        logger.entering("DAOEvent", "deleteEventByEventID");
        logger.exiting("DAOEvent", "deleteEventByEventID");
        return false;
    }

    public boolean deleteEventByUserName(String username) throws DatabaseException
    {
        logger.entering("DAOEvent", "deleteEventByUserName");
        PreparedStatement stmt = null;
        try
        {
            String sql = "DELETE FROM Event WHERE descendant = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.execute();
            logger.exiting("DAOEvent", "deleteEventByUserName");
            return true;
        }
        catch (SQLException e)
        {
            throw new DatabaseException("Internal Server Error", e);
        }
    }

    /**
     * Gets a Event from the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid Event object with all members initialized.
     * </pre>
     * @param sEventID the id of the Event to delete
     * @return the Event object, initialized with values from database. Null if Event does not exist.
     */
    public Event getEvent(String sEventID) throws DatabaseException
    {
        logger.entering("DAOEvent", "getEvent");
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        Event evnt = new Event();
        try
        {
            String sql = "SELECT eventID, descendant, personID, latitude, longitude, country, city, eventType, year FROM Event WHERE eventID = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, sEventID);

            resultSet = stmt.executeQuery();
            evnt.setEventID(resultSet.getString(1));
            evnt.setDescendant(resultSet.getString(2));
            evnt.setPersonID(resultSet.getString(3));
            evnt.setLatitude(resultSet.getDouble(4));
            evnt.setLongitude(resultSet.getDouble(5));
            evnt.setCountry(resultSet.getString(6));
            evnt.setCity(resultSet.getString(7));
            evnt.setEventType(resultSet.getString(8));
            evnt.setYear(resultSet.getInt(9));
        }
        catch (SQLException e)
        {
            throw new DatabaseException("getEvent failed" , e);
        }
        logger.exiting("DAOEvent", "getEvent");
        return evnt;
    }

    /**
     * Clears all Event from database
     * <pre>
     *     <b>Constraints on the input</b>:
     *     None
     * </pre>
     * @return true if all Event were deleted, false otherwise.
     */
    public boolean clear() throws DatabaseException
    {
        logger.entering("DAOEvent", "clear");
        PreparedStatement query = null;
        PreparedStatement delete = null;
        try
        {
            String sqlQuery = "SELECT eventID FROM Event";
            query = m_conn.prepareStatement(sqlQuery);
            ResultSet resultSet = query.executeQuery();
            if (resultSet.next())
            {
                String sqlDelete = "DELETE FROM Event";
                delete = m_conn.prepareStatement(sqlDelete);
                if (delete.executeUpdate() > 0)
                {
                    logger.exiting("DAOEvent", "clear");
                    return true;
                }
                else
                {
                    logger.exiting("DAOEvent", "clear");
                    return false;
                }
            }
            else
            {
                logger.exiting("DAOEvent", "clear");
                return true;
            }
        }
        catch (SQLException e)
        {
            logger.exiting("DAOEvent", "clear");
            throw new DatabaseException("Internal Server Error: DAOClearEvent", e);
        }
    }

    public Event[] getEventsByPersId(String persId) throws DatabaseException
    {
        logger.entering("DAOEvent", "getEventsByPersId");
        ArrayList<Event> events = new ArrayList<>();
        Event[] eventArray = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try
        {
            String sql = "SELECT eventID, descendant, personID, latitude, longitude, country, city, eventType, year FROM Event WHERE descendant = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, persId);

            resultSet = stmt.executeQuery();

            while (resultSet.next())
            {
                Event event = new Event();
                event.setEventID(resultSet.getString(1));
                event.setDescendant(resultSet.getString(2));
                event.setPersonID(resultSet.getString(3));
                event.setLatitude(resultSet.getLong(4));
                event.setLongitude(resultSet.getDouble(5));
                event.setCountry(resultSet.getString(6));
                event.setCity(resultSet.getString(7));
                event.setEventType(resultSet.getString(8));
                event.setYear(resultSet.getInt(9));

                events.add(event);
            }

            int i = 0;
            eventArray = new Event[events.size()];
            for (Event e : events)
            {
                eventArray[i] = e;
                i++;
            }
        }
        catch (SQLException e)
        {
            throw new DatabaseException("getEventsByPersId failed", e);
        }
        logger.exiting("DAOEvent", "getEventsByPersId");
        return eventArray;
    }
}
