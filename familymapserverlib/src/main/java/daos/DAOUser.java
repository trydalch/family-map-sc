package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import exceptions.DatabaseException;
import model.User;

/**
 * A data access object used to add, delete, get , and clear users from the SQLite database.
 */
public class DAOUser
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private Connection m_conn = null;

    public Connection getM_conn() {
        return m_conn;
    }

    public void setM_conn(Connection m_conn) {
        this.m_conn = m_conn;
    }

    /**
     * Adds a user to the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid User object with all members initialized.
     * </pre>
     * @param uUsr the User object to add to the database
     * @return true if successfully added User to database, false otherwise
     */
    public boolean addUser(User uUsr) throws DatabaseException
    {
//        logger.entering("DAOUser", "addUser");
        assert m_conn != null;
        PreparedStatement stmt = null;
        try
        {
            String sql = "insert into User (username, password, email, firstName, lastName, gender, personID) " +
                    "values ('" + uUsr.getUserName() + "', '" + uUsr.getPassword() + "', '" + uUsr.getEmail() + "', '" + uUsr.getFirstName() + "', '" +
                    uUsr.getLastName() + "', '" + uUsr.getGender() + "', '" + uUsr.getPersonID() + "');";
            stmt = m_conn.prepareStatement(sql);
            if (stmt.executeUpdate() == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            logger.warning("SQLException when adding user. Message: " + e.getMessage());
            logger.exiting("DAOUser", "addUser");
            String usrNameNotUnique = "username is not unique";
            if (e.getMessage().contains(usrNameNotUnique))
            {
                throw new DatabaseException("Username is taken");
            }
            throw new DatabaseException("Internal Server Error", e);
        }

    }

    /**
     * Deletes a user from the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid User object with all members initialized.
     * </pre>
     * @param usr the User to delete
     * @return true if successfully deleted User from database, false otherwise
     */
    public boolean deleteUser(User usr) throws DatabaseException
    {
//        logger.entering("DAOUser", "deleteUser");
        assert m_conn != null;
        assert usr != null;

        String sUsrName = usr.getUserName();
        assert sUsrName != null;
        PreparedStatement stmt = null;
        try
        {
            assert m_conn.isClosed() != true;
            String sql = "DELETE FROM User" + " WHERE username = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, sUsrName);
            if (stmt.executeUpdate() == 1)
            {
//                logger.exiting("DAOUser", "deleteUser");
                return true;
            }
            else
            {
//                logger.exiting("DAOUser", "deleteUser");
                return false;
            }
        }
        catch (SQLException e)
        {
            logger.warning("SQLException when deleting user. Message: " + e.getMessage());
            logger.exiting("DAOUser", "deleteUser");
            throw new DatabaseException("deleteUser failed", e);
        }
    }

    /**
     * Gets a user from the database.
     * <pre>
     *     <b>Constraints on the input</b>:
     *     Valid User object with all members initialized.
     * </pre>
     * @param sUsrName the username of the User to delete
     * @return the User object, initialized with values from database. Null if user does not exist.
     */
    public User getUser(String sUsrName) throws DatabaseException
    {
//        logger.entering("DAOUser", "getUser");
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        User usr = new User();
        try
        {
            String sql = "SELECT username, password, email, firstName, lastName, gender, personID FROM User WHERE username = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, sUsrName);
            resultSet = stmt.executeQuery();

            usr.setUserName(sUsrName);

//            while (resultSet.next()) {
                usr.setPassword(resultSet.getString(2));
                usr.setEmail(resultSet.getString(3));
                usr.setFirstName(resultSet.getString(4));
                usr.setLastName(resultSet.getString(5));
                usr.setGender(resultSet.getString(6));
                usr.setPersonID(resultSet.getString(7));
                if (resultSet.getString(1).equals(sUsrName)) {
//                    break;
                }
//            }

            resultSet.close();
            stmt.close();
        }
        catch (SQLException e)
        {
            logger.warning("Invalid Login username");
            throw new DatabaseException("Invalid Username", e);
        }
//        logger.exiting("DAOUser", "getUser");
        return usr;
    }

    public User getUserByID(String id) throws DatabaseException
    {
//        logger.entering("DAOUser", "getUserByID");
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        User usr = new User();

        try
        {
            String sql = "SELECT username, password, email, firstName, lastName, gender, personID FROM User WHERE personID = ?";
            stmt = m_conn.prepareStatement(sql);
            stmt.setString(1, id);
            resultSet = stmt.executeQuery();

            usr.setUserName(resultSet.getString(1));
            usr.setPassword(resultSet.getString(2));
            usr.setEmail(resultSet.getString(3));
            usr.setFirstName(resultSet.getString(4));
            usr.setLastName(resultSet.getString(5));
            usr.setGender(resultSet.getString(6));
            usr.setPersonID(resultSet.getString(7));

            resultSet.close();
            stmt.close();
        }
        catch (SQLException e)
        {
            logger.warning("SQLException when getting user by id. Message: " + e.getMessage());
            throw new DatabaseException("getUserByID failed", e);
        }

//        logger.exiting("DAOUser", "getUserByID");
        return usr;
    }

    /**
     * Clears all users from database
     * <pre>
     *     <b>Constraints on the input</b>:
     *     None
     * </pre>
     * @return true if all Users were deleted, false otherwise.
     */
    public boolean clear() throws DatabaseException
    {
//        logger.entering("DAOUser", "clear");
        assert m_conn != null;
        PreparedStatement query = null;
        PreparedStatement delete = null;
        try
        {
            assert !m_conn.isClosed();

//            String sqlQuery = "SELECT personID FROM User";
//            query = m_conn.prepareStatement(sqlQuery);
//            ResultSet resultSet = query.executeQuery();
//            if (resultSet.next())
            {
                String sqlDelete = "DELETE FROM User";
                delete = m_conn.prepareStatement(sqlDelete);
                int num = delete.executeUpdate();
                if (num > 0)
                {
//                    logger.exiting("DAOUser", "clear");
                    return true;
                }
                else
                {
//                    logger.exiting("DAOUser", "clear");
                    return false;
                }
            }
        }
        catch (SQLException e)
        {
            logger.warning("SQLException when clearing user table. Message: " + e.getMessage());
            logger.exiting("DAOUser", "clear");
            throw new DatabaseException("Internal Server Error: clear DAOUser", e);
        }
    }
}
