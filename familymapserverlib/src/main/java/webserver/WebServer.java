package webserver;
import java.io.*;
import java.net.*;

import com.sun.net.httpserver.*;
import java.util.logging.*;

import handlers.ClearHandler;
import handlers.DefaultHandler;
import handlers.FillHandler;
import handlers.LoadHandler;
import handlers.LoginHandler;
import handlers.PersonHandler;
import handlers.RegisterHandler;
import handlers.EventHandler;


/**
 * Created by Trevor.Rydalch on 5/24/2017.
 */

public class WebServer
{
    // STATIC MEMBERS
    private static final int MAX_WAITING_CONNECTIONS = 12;

    private static Logger logger;

    static {
        try {
            initLog();
        }
        catch (IOException e) {
            System.out.println("Could not initialize log: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static int AuthTokenTimeoutSeconds;

    private static void initLog() throws IOException {

        Level logLevel = Level.FINEST;

        logger = Logger.getLogger("fmslogger");
        logger.setLevel(logLevel);
        logger.setUseParentHandlers(false);

        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(logLevel);
        consoleHandler.setFormatter(new SimpleFormatter());
        logger.addHandler(consoleHandler);

        FileHandler fileHandler = new FileHandler("fmslog.txt", false);
        fileHandler.setLevel(logLevel);
        fileHandler.setFormatter(new SimpleFormatter());
        logger.addHandler(fileHandler);
    }


    // NON-STATIC MEMBERS
    private HttpServer server;

    private void run(String portNumber) {

        logger.info("Initializing HTTP Server");
        try {
            System.out.println("Address:" + Inet4Address.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try {
            server = HttpServer.create(
                    new InetSocketAddress(Integer.parseInt(portNumber)),
                    MAX_WAITING_CONNECTIONS);
        }
        catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            return;
        }

        server.setExecutor(null); // use the default executor

        logger.info("Creating contexts");
        server.createContext("/", new DefaultHandler());
        server.createContext("/user/register", new RegisterHandler());
        server.createContext("/user/login", new LoginHandler());
        server.createContext("/clear", new ClearHandler());
        server.createContext("/fill", new FillHandler());
        server.createContext("/load", new LoadHandler());
        server.createContext("/person", new PersonHandler());
        server.createContext("/event", new EventHandler());
        logger.info("Starting HTTP server");
        server.start();
    }

    public static void main(String[] args) {
        String portNumber = args[0];
        new WebServer().run(portNumber);
        AuthTokenTimeoutSeconds = Integer.parseInt(args[1]);


    }

}
