package handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.Scanner;
import java.util.logging.Logger;

import commrequests.EventEventIDRequest;
import commresults.EventEventIDResult;
import services.EventEventIDService;

/**
 * Created by trevor on 5/19/17.
 */

public class EventEventIDHandler implements HttpHandler
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private EventEventIDService m_eventEventIDService = new EventEventIDService();


    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        logger.entering("EventEventIDHandler", "handle");

        URI uri = httpExchange.getRequestURI();
        Scanner scanner = new Scanner(uri.toString());
        scanner.useDelimiter("/");

        // Move cursor past 'event' and to the eventID
        scanner.next();
        // Because the httpExchange was handled first by the EventHandler, we know there is an ID.
        String eventID = scanner.next();
        try
        {
            EventEventIDResult result;
            if (httpExchange.getRequestMethod().toUpperCase().equals("GET"))
            {
                // Check for authorization token
                if (httpExchange.getRequestHeaders().containsKey("Authorization"))
                {
                    // Prepare request for EventEventIDService
                    EventEventIDRequest request = new EventEventIDRequest();
                    request.setAuthToken(httpExchange.getRequestHeaders().getFirst("Authorization"));
                    request.setEventID(eventID);
                    // Process request
                    result = m_eventEventIDService.eventEventID(request);
                    // Convert result to Json
                    Gson gson = new Gson();
                    String jsonStr = gson.toJson(result);
                    // Send headers and responsebody containing requested event or an error message (contained the JSON string created from the result).
                    httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                    OutputStream respBody = httpExchange.getResponseBody();
                    writeString(jsonStr, respBody);

                    respBody.close();
                }
            }
        }
        catch (IOException e)
        {

        }

        logger.exiting("EventEventIDHandler", "handle");
    }

    /*
The readString method shows how to read a String from an InputStream.
*/
    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }
}
