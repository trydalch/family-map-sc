package handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.Scanner;
import java.util.logging.Logger;

import commrequests.PersonPersonIDRequest;
import commresults.PersonPersonIDResult;
import services.PersonPersonIDService;

/**
 * Created by trevor on 5/19/17.
 */

public class PersonPersonIDHandler implements HttpHandler
{
    private PersonPersonIDService m_PersonPersonIDService = new PersonPersonIDService();
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        logger.entering("PersonPersonIDHandler", "handle");

        //Get URL
        URI uri = httpExchange.getRequestURI();
        Scanner scanner = new Scanner(uri.toString());
        scanner.useDelimiter("/");

        // Move curser past /person/ to location of ID parameter
        scanner.next();
        // Because httpExchange was processed first by PersonHandler, we know there is a value in the ID position.
        String persID = scanner.next();
        try
        {
            PersonPersonIDResult result;
            if (httpExchange.getRequestMethod().toUpperCase().equals("GET"))
            {
                Headers reqHeaders = httpExchange.getRequestHeaders();
                // Check for authorization token
                if (reqHeaders.containsKey("Authorization"))
                {
                    String sAuthToken = reqHeaders.getFirst("Authorization");
                    if (!sAuthToken.isEmpty())
                    {
                        // User has an authorization token. Pass parameters to request
                        PersonPersonIDRequest request = new PersonPersonIDRequest();
                        request.setPersonID(persID);
                        request.setAuthToken(sAuthToken);

                        // Process request in PersonPersonIDService, convert result to String containing JSON objects
                        result = m_PersonPersonIDService.personPersonID(request);
                        Gson gson = new Gson();
                        String jsonStr = gson.toJson(result);

                        // Check for error message
                        if (result.getMessage() != null)
                        {
                            if (result.getMessage().equals("Internal Server Error"))
                            {
                                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
                            }
                            else if (result.getMessage().equals("Invalid Authorization Token") || result.getMessage().equals("Authorization Token has expired"))
                            {
                                // Either the Authorization token does not exist or it has expired and the user must log back in
                                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_FORBIDDEN, 0);
                            }

                        }
                        // Send response headers and body containing result string of JSON objects
                        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                        OutputStream respBody = httpExchange.getResponseBody();
                        writeString(jsonStr, respBody);
                        respBody.close();
                    }
                    else
                    {
                        // User did not enter an Authorization token.
                        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                        String invalidAuth = "No Authorization Token entered";
                        OutputStream respBody = httpExchange.getResponseBody();
                        writeString(invalidAuth, respBody);
                        respBody.close();
                    }
                }
            }
        }
        catch(IOException e)
        {
            // Should not ever get here. All error handling is handled before writeString which throws an IOException.
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            OutputStream respBody = httpExchange.getResponseBody();
            writeString(e.getMessage(), respBody);
            respBody.close();
        }
        logger.exiting("PersonPersonIDHandler", "handle");
    }

    /*
The readString method shows how to read a String from an InputStream.
*/
    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }
}
