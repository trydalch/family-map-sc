package handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

import com.google.gson.*;

import commrequests.LoginRequest;
import commresults.LoginResult;
import services.LoginService;


/**
 * Created by Trevor.Rydalch on 5/17/2017.
 */

public class LoginHandler implements HttpHandler
{
    private LoginService m_LoginService = new LoginService();
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        logger.entering("LoginHandler", "handle");
        try
        {
            String reqMethod = httpExchange.getRequestMethod().toString().toUpperCase();
            if (reqMethod.equals("POST"))
            {
                // Get requestBody containing username and password and convert to String
                Gson gson = new Gson();
                InputStream reqBody = httpExchange.getRequestBody();
                String reqData = readString(reqBody);

                // Convert from a String to LoginRequest Java object
                LoginRequest logRequest = gson.fromJson(reqData, LoginRequest.class);

                // Process LoginRequest
                LoginResult loginResult = m_LoginService.login(logRequest);
                String jsonStr = gson.toJson(loginResult);

                // Send headers and responseBody containing JSON String of LoginResult object
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK,0);
                OutputStream respBody = httpExchange.getResponseBody();
                writeString(jsonStr, respBody);

                respBody.close();
            }
        }
        catch (IOException e)
        {
            // Shouldn't ever get here. All error handling before writeString() which throws IOException. But just in case.
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST,0);
        }
        logger.exiting("LoginHandler", "handle");
    }

    /*
    The readString method shows how to read a String from an InputStream.
*/
    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }
}
