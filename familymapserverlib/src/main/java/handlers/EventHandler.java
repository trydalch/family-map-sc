package handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.Scanner;
import java.util.logging.Logger;

import commrequests.EventRequest;
import commresults.EventResult;
import services.EventService;

/**
 * Created by trevor on 5/19/17.
 */

public class EventHandler implements HttpHandler
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private EventService m_eventService = new EventService();

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        logger.entering("EventHandler", "handle");

        // Get full path from URL
        URI uri = httpExchange.getRequestURI();
        Scanner scanner = new Scanner(uri.toString());
        scanner.useDelimiter("/");

        // Move cursor past 'event' to check for an eventID
        scanner.next();
        if (scanner.hasNext())
        {
            // User has requested a specific event. Pass the request to the EventEventID handler
            EventEventIDHandler eventEventIDHandler = new EventEventIDHandler();
            eventEventIDHandler.handle(httpExchange);
        }
        else
        {
            // User has requested all their events.
            try
            {
                if (httpExchange.getRequestMethod().toUpperCase().equals("GET"))
                {
                    // Check for authorization token
                    if (httpExchange.getRequestHeaders().containsKey("Authorization"))
                    {
                        EventRequest request = new EventRequest();
                        request.setAuthToken(httpExchange.getRequestHeaders().getFirst("Authorization"));

                        // Process request
                        EventResult result = m_eventService.event(request);

                        // Convert to Json
                        Gson gson = new Gson();
                        String jsonStr = gson.toJson(result);

                        // Send headers and responsebody containing either the requested data or an error message.
                        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                        OutputStream respBody = httpExchange.getResponseBody();
                        writeString(jsonStr, respBody);

                        respBody.close();                    }
                }
            }
            catch (IOException ignored)
            {
                // All exceptions are handled in service class
            }
        }

        logger.exiting("EventHandler", "handle");
    }

    /*
The readString method shows how to read a String from an InputStream.
*/
    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }
}
