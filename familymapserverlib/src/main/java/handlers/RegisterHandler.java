package handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

import commrequests.RegisterRequest;
import commresults.RegisterResult;
import model.*;
import services.RegisterService;

/**
 * Created by Trevor.Rydalch on 5/17/2017.
 */

public class RegisterHandler implements HttpHandler
{
    private RegisterService m_RegisterService = new RegisterService();
    private File m_fFNames = new File("data\\json\\fnames.json");
    private File m_fMNames = new File("data\\json\\mnames.json");
    private File m_fLNames = new File("data\\json\\snames.json");
    private File m_fLctns = new File("data\\json\\locations.json");
    private File m_fEvntType = new File("data\\json\\eventTypes.json");

    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    public RegisterHandler() {
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        logger.entering("RegisterHandler", "handle");

        String reqMethod = httpExchange.getRequestMethod().toUpperCase();
        if (reqMethod.equals("POST"))
        {
            Gson gson = new Gson();
            InputStream reqBody = httpExchange.getRequestBody();

            String reqData = readString(reqBody);
            RegisterRequest registerRequest = gson.fromJson(reqData, RegisterRequest.class);
            registerRequest.setFNames(gson.fromJson(readString(new FileInputStream(m_fFNames)), FemaleNameData.class));
            registerRequest.setMNames(gson.fromJson(readString(new FileInputStream(m_fMNames)), MaleNameData.class));
            registerRequest.setLNames(gson.fromJson(readString(new FileInputStream(m_fLNames)), LastNameData.class));
            registerRequest.setLocations(gson.fromJson(readString(new FileInputStream(m_fLctns)), LocationData.class));

            RegisterResult registerResult = m_RegisterService.register(registerRequest);
            String jsonStr = gson.toJson(registerResult);

            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            OutputStream respBody = httpExchange.getResponseBody();
            writeString(jsonStr, respBody);

            respBody.close();
        }

        logger.exiting("RegisterHandler", "handle");
    }

    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }
}
