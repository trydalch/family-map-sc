package handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

import commrequests.LoadRequest;
import commresults.LoadResult;
import services.LoadService;

/**
 * Created by Trevor.Rydalch on 5/17/2017.
 */

public class LoadHandler implements HttpHandler
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    // Used to process LoadRequests
    private LoadService m_loadService = new LoadService();

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        logger.entering("LoadHandler", "handle");
        try
        {
            String reqMethod = httpExchange.getRequestMethod();
            if (reqMethod.toUpperCase().equals("POST"))
            {
                // Get data from requestbody and convert to a string.
                Gson gson = new Gson();
                InputStream reqBody = httpExchange.getRequestBody();
                String reqData = readString(reqBody);

                // Convert String containing JSON objects to a LoadRequest Java object
                LoadRequest loadRequest = gson.fromJson(reqData, LoadRequest.class);

                // Process Request and convert result to String containing JSON object of result
                LoadResult result = m_loadService.load(loadRequest);
                String jsonStr = gson.toJson(result);

                // Send headers and responsebody containing JSON string with result object data
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                OutputStream respBody = httpExchange.getResponseBody();
                writeString(jsonStr, respBody);

                respBody.close();
            }
        }
        catch (IOException e)
        {
            // Should never get here. All error handling before writeString could throw an IOException.
            e.printStackTrace();
        }
        logger.exiting("LoadHandler", "handle");
    }

    /*
The readString method shows how to read a String from an InputStream.
*/
    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }
}
