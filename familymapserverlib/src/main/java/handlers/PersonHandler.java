package handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.Scanner;
import java.util.logging.Logger;

import commrequests.PersonRequest;
import commresults.PersonResult;
import services.PersonService;

/**
 * Created by trevor on 5/16/17.
 */

public class PersonHandler implements HttpHandler
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private PersonService m_personService = new PersonService();

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        logger.entering("PersonHandler", "handle");

        // Get URL
        URI uri = httpExchange.getRequestURI();

        // Parse URL to identify parameters
        Scanner scanner = new Scanner(uri.toString());
        scanner.useDelimiter("/");

        // Move curser past /person/ to check for an ID
        scanner.next();
        if (scanner.hasNext())
        {
            // User entered a value for ID parameter. Pass httpExchange to PersonPersonIDHandler
            PersonPersonIDHandler personPersonIDHandler = new PersonPersonIDHandler();
            personPersonIDHandler.handle(httpExchange);
            httpExchange.getResponseBody().close();
        }
        else
        {
            // User did not enter a value for ID parameter. Requests all their ancestors.
            try
            {
                PersonResult result;
                if (httpExchange.getRequestMethod().toUpperCase().equals("GET"))
                {
                    Headers reqHeaders = httpExchange.getRequestHeaders();

                    // Check for authoriztion token
                    if (reqHeaders.containsKey("Authorization"))
                    {
                        // pass token to request
                        PersonRequest request = new PersonRequest();
                        request.setAuthToken(reqHeaders.getFirst("Authorization"));
                        // pass request to service
                        result = m_personService.person(request);

                        // to Json
                        Gson gson = new Gson();
                        String jsonStr = gson.toJson(result);

                        // Send response headers and body back with result
                        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                        OutputStream respBody = httpExchange.getResponseBody();
                        writeString(jsonStr, respBody);

                        respBody.close();
                    }
                }
            }
            catch (IOException e)
            {

            }

        }

        logger.exiting("PersonHandler", "handle");
    }

    /*
    The readString method shows how to read a String from an InputStream.
*/
    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }
}
