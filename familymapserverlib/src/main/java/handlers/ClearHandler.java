package handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

import commrequests.ClearRequest;
import commresults.ClearResult;
import services.ClearService;

/**
 * Created by Trevor.Rydalch on 5/17/2017.
 */

public class ClearHandler implements HttpHandler
{
    private ClearService m_ClearService = new ClearService();
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        logger.entering("ClearHandler", "handle");

        String reqMethod = httpExchange.getRequestMethod();
        if (reqMethod.toUpperCase().equals("POST"))
        {
            // No input required to clear the DB. Passing in blank ClearRequest.
            ClearResult result = m_ClearService.clear(new ClearRequest());

            Gson gson = new Gson();
            String jsonStr = gson.toJson(result);

            if (result.getMessage().equals("Clear succeeded"))
            {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            }
            else   // The database failed to be cleared of data. Internal error.
            {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
            }

            // Sends message. Message contains "Clear succeeded" for a successful clear, "Internal Server Error" for error.
            OutputStream respBody = httpExchange.getResponseBody();
            writeString(jsonStr, respBody);

            respBody.close();
        }

        logger.exiting("ClearHandler", "handle");
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }

}
