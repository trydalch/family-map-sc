package handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.Scanner;
import java.util.logging.Logger;

import commrequests.FillRequest;
import commresults.FillResult;
import model.*;
import services.FillService;

/**
 * Created by Trevor.Rydalch on 5/17/2017.
 */

public class FillHandler implements HttpHandler
{
    private static Logger logger;
    private FillService m_fillService = new FillService();
    // JSON File containing female names
    private File m_fFNames = new File("data\\json\\fnames.json");
    // JSON File containing male names
    private File m_fMNames = new File("data\\json\\mnames.json");
    // JSON File containing last names
    private File m_fLNames = new File("data\\json\\snames.json");
    // JSON File containing locations.
    private File m_fLctns = new File("data\\json\\locations.json");

    static
    {
        // Family Map Server Logger
        logger = Logger.getLogger("fmslogger");
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        logger.entering("FillHandler", "handle");

        // Get URL path
        URI uri = httpExchange.getRequestURI();
        // Parse
        Scanner scanner = new Scanner(uri.toString());
        scanner.useDelimiter("/");
        // Move cursor from "fill" to generations and check for value. If there is no value, default fill 4 generations.
        scanner.next();
        String userName = scanner.next();
        Integer generations;
        if (!scanner.hasNext())
        {
            generations = 4;
        }
        else if (scanner.hasNextInt())
        {
            generations = scanner.nextInt();
        }
        else
        {
            // is not an int. Not valid. Set generations = -1 so that it fails the following if condition,
            // resulting in an "Invalid generation" response body being sent back.
            generations = -1;
        }

        if (generations >= 0)
        {
            // Valid number of generations.
            {
                if (httpExchange.getRequestMethod().toUpperCase().equals("POST")) {

                    // Prepare request with values from URL
                    FillRequest fillRequest = new FillRequest();
                    fillRequest.setUserName(userName);
                    fillRequest.setGenerations(generations);

                    // Convert contents from JSON files to Java objects and then prepare request with converted values.
                    Gson gson = new Gson();
                    fillRequest.setFNames(gson.fromJson(readString(new FileInputStream(m_fFNames)), FemaleNameData.class));
                    fillRequest.setMNames(gson.fromJson(readString(new FileInputStream(m_fMNames)), MaleNameData.class));
                    fillRequest.setLNames(gson.fromJson(readString(new FileInputStream(m_fLNames)), LastNameData.class));
                    fillRequest.setLocations(gson.fromJson(readString(new FileInputStream(m_fLctns)), LocationData.class));

                    // Process request
                    FillResult fillResult = m_fillService.fill(fillRequest);

                    // Convert result to a String in JSON format. (Prepared to send in responsebody)
                    String jsonStr = gson.toJson(fillResult);

                    // Send headers and responsebody containing either a success or error message.
                    httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                    OutputStream respBody = httpExchange.getResponseBody();
                    writeString(jsonStr, respBody);

                    respBody.close();
                }
            }
        }
        else
        {
            // Invalid parameter entered for generations. Prepare request with error message.

            FillResult fillResult = new FillResult();
            fillResult.setMessage("Invalid generations parameter.");

            // Convert result to String in JSON format
            Gson gson = new Gson();
            String jsonStr = gson.toJson(fillResult);

            // Send headers and responsebody containing result.
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            OutputStream respBody = httpExchange.getResponseBody();
            writeString(jsonStr, respBody);
            respBody.close();
        }
        logger.exiting("FillHandler", "handle");
    }

    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void writeString(String str, OutputStream outputStream) throws IOException
    {
        OutputStreamWriter sw = new OutputStreamWriter(outputStream);
        sw.write(str);
        sw.flush();
    }
}
