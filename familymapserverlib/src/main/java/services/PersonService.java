package services;

import java.util.logging.Logger;

import commrequests.PersonRequest;
import commresults.PersonResult;
import daos.DAOControl;
import exceptions.DatabaseException;
import model.AuthToken;
import model.Person;
import model.User;

/**
 * A service class to handle Person requests. Returns all persons for a user/person
 */

public class PersonService
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private DAOControl m_daoControl = new DAOControl();
    private AuthTokenValidatorService authTokenValidatorService = new AuthTokenValidatorService();

    /**
     * The person method to return all persons for a user.
     * <pre>
     *     <b>Constraints on the input</b>
     *          Must be valid PersonRequest object
     *     <b>Result</b>:
     *     PersonResult obj
     * </pre>
     * @param request the request made by the PersonHandler, a PersonRequest obj
     * @return a PersonResult object
     */
    public PersonResult person(PersonRequest request)
    {
        PersonResult result = new PersonResult();
        Person[] persArray = null;
        String error = null;
        try
        {
            m_daoControl.openConnection();
            AuthToken authToken = m_daoControl.getAuthToken(request.getAuthToken());
            if (authTokenValidatorService.checkAuthTokenTimeOut(authToken, m_daoControl))
            {
                User usr = m_daoControl.getUserByID(authToken.getPersId());

                persArray = m_daoControl.getPersonsByUserName(usr.getUserName());
                result.setData(persArray);
            }
            else
            {
                error = "Authorization Token has expired.";
            }
        }
        catch (DatabaseException e)
        {
            result.setMessage(e.getMessage());
        }
        finally {
            result.setMessage(error);
            m_daoControl.closeConnection(false);
        }
        return result;
    }
}
