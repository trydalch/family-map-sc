package services;

import java.util.logging.Logger;

import commrequests.EventRequest;
import commresults.EventResult;
import daos.DAOControl;
import exceptions.DatabaseException;
import model.AuthToken;
import model.Event;
import model.User;

/**
 * A service class to handle EventRequests. Returns all events related to a specified person.
 */
public class EventService
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private DAOControl m_daoControl = new DAOControl();
    private AuthTokenValidatorService authTokenValidatorService = new AuthTokenValidatorService();


    /**
     * The event method to return all events related to a specified person in the EventRequest obj param.
     * <pre>
     *     <b>Constraints on the input</b>
     *          Must be valid EventRequest object
     *     <b>Result</b>:
     *     EventResult obj
     * </pre>
     * @param request the request made by the EventHandler, a EventRequest obj
     * @return an EventResult object
     */
    public EventResult event(EventRequest request)
    {
        EventResult result = new EventResult();
        try
        {
            // Open a connection to the database
            m_daoControl.openConnection();

            // Get the auth token from the authID in the request.
            AuthToken authToken = m_daoControl.getAuthToken(request.getAuthToken());

            // Is it a valid auth token?
            if (authTokenValidatorService.checkAuthTokenTimeOut(authToken, m_daoControl))
            {
                // yes. Get the user to whom the authToken belongs.
                User usr = m_daoControl.getUserByID(authToken.getPersId());

                // Get all events that belong to that user.
                Event[] evntArray = m_daoControl.getEventsByPersId(usr.getUserName());

                // Pass events to result
                result.setData(evntArray);
            }
            else
            {
                // No, Authorization token has expired.
                result.setMessage("Authorization Token has expired.");
            }
        }
        catch (DatabaseException e)
        {
            // Some error occured when accessing the database. Could be internal, or user.
            // Exception contains an informative error message.
            result.setMessage(e.getMessage());
        }
        finally
        {
            // Close the database connection
            m_daoControl.closeConnection(false);
        }
        return result;
    }

}
