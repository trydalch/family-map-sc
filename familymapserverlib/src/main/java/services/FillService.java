package services;

import java.util.HashSet;
import java.util.logging.Logger;

import commrequests.FillRequest;
import commresults.FillResult;
import daos.DAOControl;
import exceptions.DatabaseException;
import model.*;

/**
 * A service class to process FillRequests to populate a user's tree.
 */
public class FillService
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private DAOControl m_daoControl = new DAOControl();

    // Used to generate generations of family history information randomly
    private FamilyTreeGeneration m_treeGenerator = new FamilyTreeGeneration();

    /**
     * The fill method populates the database with generated data for the username contained in the Request obj.
     * <pre>
     *     <b>Constraints on the input</b>
     *          Must be valid FillRequest object
     *     <b>Result</b>:
     *     FillResult obj
     * </pre>
     * @param request the request made by the FillHandler, a FillRequest obj
     * @return a FillResult object
     */
    public FillResult fill(FillRequest request)
    {
        FillResult fillResult = new FillResult();
        int generations = 0;
        if (request.getGenerations() == 0)
        {
            // The default number of generations to generate is 4
            generations = 4;
        }
        else if (request.getGenerations() < 0)
        {
            // Invalid value entered for generations
            fillResult.setMessage("Generations must be a non-negative integer");
        }
        else
        {
            // valid value entered for generations
            generations = request.getGenerations();
        }

        // Generate random information for root person (also the same person as the User)
        Person pers = new Person();
        pers.genRandomID();
        pers.setGeneration(0);

        boolean addPers = false;
        boolean addEvnt = false;
        try
        {
            m_daoControl.openConnection();

            // Check for valid username. Throws an exception if username is invalid.
            User usr = m_daoControl.getUser(request.getUserName());

            // Create person object to match user
            pers.setFirstName(usr.getFirstName());
            pers.setLastName(usr.getLastName());
            pers.setDescendant(usr.getUserName());
            pers.setGender(usr.getGender());

            // Generate ancestors randomly
            HashSet<Person> persons = m_treeGenerator.generateTree(request.getFNames(), request.getMNames(), request.getLNames(), generations, pers, request.getUserName());
            // Generate events for all ancestors and root person.
            HashSet<Event> events = m_treeGenerator.generateEvents(persons, request.getLocations());

            // Delete all persons and events currently in database that pertain to this user
            boolean deletePersons = m_daoControl.deletePersonByUserName(request.getUserName());
            boolean deleteEvents = m_daoControl.deleteEventsByUserName(request.getUserName());

            // Add newly created persons to database
            for (Person p : persons)
            {
                addPers = m_daoControl.addPerson(p);
            }

            // Add newly created events to database
            for (Event e : events)
            {
                addEvnt = m_daoControl.addEvent(e);
            }

            // Set return message that will be sent to user.
            fillResult.setMessage("Successfully added " + persons.size() + " persons and " + events.size() + " events to the database");
        }
        catch(DatabaseException e)
        {
            // An error occured when accessing the database.
            fillResult.setMessage(e.getMessage());
        }
        finally
        {
            // Close connection to database. Commit if both Persons and Events were added successfully
            m_daoControl.closeConnection(addPers && addEvnt);
        }

        return fillResult;
    }

    /**
     * Used to set database control object. Used for testing purposes to switch to test database
     * @param m_daoControl
     */
    public void setM_daoControl(DAOControl m_daoControl)
    {
        this.m_daoControl = m_daoControl;
    }
}
