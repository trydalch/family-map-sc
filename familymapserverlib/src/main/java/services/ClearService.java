package services;

import java.util.logging.Logger;

import commrequests.ClearRequest;
import commresults.ClearResult;
import daos.DAOControl;
import exceptions.DatabaseException;

/**
 * A service class to process ClearRequests to clear the database of all data.
 */
public class ClearService
{
    private DAOControl m_daoControl = new DAOControl();

    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    /**
     * The clear method to clear the database of all data.
     * <pre>
     *     <b>Constraints on the input</b>
     *          Must be valid ClearRequest object
     *     <b>Result</b>:
     *     ClearResult obj
     * </pre>
     * @param request the request made by the ClearHandler, a ClearRequest obj
     * @return a ClearResult object
     */
    public ClearResult clear(ClearRequest request)
    {
        ClearResult result = new ClearResult();

        boolean successfulClear = false;
        try
        {
            // Open a connection to the database
            m_daoControl.openConnection();

            // Process clear request
            successfulClear = m_daoControl.clear();

            if (successfulClear)
            {
                // Clear succeeded
                result.setMessage("Clear succeeded");
            }
            else
            {
                // Should not ever get here. But if it did, the database indicated the clear failed, w/o throwing an exception.
                result.setMessage("Clear failed");
            }
        }
        catch(DatabaseException e)
        {
            // There was an error when attempting to clear the database. The exception contains an informative error message.
            result.setMessage(e.getMessage());
        }
        finally
        {
            // Close the connection to the database
            m_daoControl.closeConnection(successfulClear);
        }
        return result;
    }

    /**
     * Set the DAOControl object. Used only in unit testing to switch to a 'test' database
     * @param m_daoControl
     */
    public void setM_daoControl(DAOControl m_daoControl)
    {
        this.m_daoControl = m_daoControl;
    }
}
