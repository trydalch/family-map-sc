package services;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import commrequests.LoadRequest;
import commresults.LoadResult;
import daos.DAOControl;
import exceptions.DatabaseException;
import model.*;

/**
 * A service class to handle LoadRequests. Clears database and loads new data.
 */
public class LoadService
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private DAOControl m_daoControl = new DAOControl();

    /**
     * The load method to clear the database and load fresh new data.
     * <pre>
     *     <b>Constraints on the input</b>
     *          Must be valid LoadRequest object
     *     <b>Result</b>:
     *     LoadResult obj
     * </pre>
     * @param request the request made by the LoadHandler, a LoadRequest obj
     * @return a LoadResult object
     */
    public LoadResult load(LoadRequest request)
    {
        // Get data from LoadRequest
        LoadResult result = new LoadResult();
        User[] users = request.getUsers();
        Person[] persons = request.getPersons();
        Event[] events = request.getEvents();

        boolean userAdd = false;
        boolean personAdd = false;
        boolean eventAdd = false;
        try
        {
            // Check Users for necessary values
            validateUsers(users);
            // Check Persons for necessary Values
            validatePersons(persons);
            // Check Events for necessary Values
            validateEvents(events);

            m_daoControl.openConnection();

            // Clear data in database
            m_daoControl.clear();
            // Add loaded users to database
            for (User u : users)
            {
                userAdd = m_daoControl.addUser(u);
                if (!userAdd)
                {
                    logger.log(Level.SEVERE, "Failed to add user");
                }
            }

            // Add loaded Persons to database
            for (Person p : persons)
            {
                personAdd = m_daoControl.addPerson(p);
                if (!personAdd)
                {
                    logger.log(Level.SEVERE, "Failed to add person");
                }
            }

            // Add loaded Events to database
            for (Event e : events)
            {
                eventAdd = m_daoControl.addEvent(e);
                if (!eventAdd)
                {
                    logger.log(Level.SEVERE, "Failed to add event");
                }
            }
            if (userAdd && personAdd && eventAdd)
            {
                result.setMessage("Successfully added " + users.length + " users, " + persons.length + " persons, and " + events.length + " events to the database");
            }
            else
            {
                result.setMessage("Failed to add users, persons, and events to database.");
            }
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            result.setMessage(e.getMessage());
        }
        finally
        {
            m_daoControl.closeConnection(userAdd && personAdd && eventAdd);
        }
        return result;
    }

    private void validateEvents(Event[] events) throws IOException
    {
        for (Event e : events)
        {
            if (e.getPersonID() == null)
            {
                throw new IOException("Invalid event data");
            }
            if (e.getDescendant() == null)
            {
                throw new IOException("Invalid event data");
            }
            if (e.getEventID() == null)
            {
                throw new IOException("Invalid event data");
            }
            if (e.getEventType() == null)
            {
                throw new IOException("Invalid event data");
            }
        }
    }

    private void validatePersons(Person[] persons) throws IOException
    {
        for (Person p : persons)
        {
            if (p.getDescendant() == null || p.getDescendant().isEmpty())
            {
                throw new IOException("Invalid person data");

            }
            if (p.getPersonID() == null || p.getPersonID().isEmpty())
            {
                throw new IOException("Invalid person data");
            }
            if (p.getGender() == null || p.getGender().isEmpty())
            {
                throw new IOException("Invalid person data");
            }
            if (p.getFirstName() == null || p.getFirstName().isEmpty())
            {
                throw new IOException("Invalid person data");
            }
            if (p.getLastName() == null || p.getLastName().isEmpty())
            {
                throw new IOException("Invalid person data");
            }
        }
    }

    private void validateUsers(User[] users) throws IOException
    {
        for (User u : users)
        {
            if (u.getUserName() == null)
            {
                throw new IOException("Invalid user data");
            }
            if (u.getPassword() == null)
            {
                throw new IOException("Invalid user data");
            }
            if(u.getPersonID() == null)
            {
                throw new IOException("Invalid user data");
            }
            if (u.getFirstName() == null)
            {
                throw new IOException("Invalid user data");
            }
            if (u.getLastName() == null)
            {
                throw new IOException("Invalid user data");
            }
            if (u.getEmail() == null)
            {
                throw new IOException("Invalid user data");
            }
        }
    }

}
