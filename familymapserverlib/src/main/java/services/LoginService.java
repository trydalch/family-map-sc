package services;

import java.util.logging.Logger;

import commrequests.LoginRequest;
import commresults.LoginResult;
import daos.DAOControl;
import exceptions.DatabaseException;
import model.AuthToken;
import model.User;

/**
 * A service class to process login requests.
 */
public class LoginService
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private DAOControl m_DAOControl = new DAOControl();

    /**
     * The login method to login a user.
     * <pre>
     *     <b>Constraints on the input</b>
     *          Must be valid LoginRequest object
     *     <b>Result</b>:
     *     LoginResult obj
     * </pre>
     * @param request the request made by the LoginHandler, a LoginRequest obj
     * @return a LoginResult object
     */
    public LoginResult login(LoginRequest request)
    {
        LoginResult result = new LoginResult();
        String loginUsername = request.getUserName();

        boolean addAuth = false;

        try
        {
            if (request.getUserName() == null || request.getPassword() == null || request.getUserName().isEmpty() || request.getPassword().isEmpty())
            {
                result.setMessage("Please enter both a username and password");
                return result;
            }
            m_DAOControl.openConnection();
            if (!validUser(request.getUserName()))
            {
                result.setMessage("Invalid username");
                return result;
            }
            User usr = m_DAOControl.getUser(loginUsername);
            if (usr.getPassword().equals(request.getPassword()))
            {
                // Usr provided valid credentials. Generate a new Auth Token, fill LoginResult members
                AuthToken authToken = new AuthToken();
                authToken.genRandomAuthToken();
                authToken.setPersId(usr.getPersonID());
                addAuth = m_DAOControl.addAuthToken(authToken);
                result.setUserName(usr.getUserName());
                result.setPersonId(usr.getPersonID());
                result.setAuthToken(authToken.getAuthId());
            }
            else
            {
                result.setMessage("Invalid Password");
            }
        }
        catch (DatabaseException e)
        {
            result.setMessage(e.getMessage());
        }
        finally
        {
            m_DAOControl.closeConnection(addAuth);
        }

        return result;
    }

    private boolean validUser(String userName) {
        try
        {
            User usr = m_DAOControl.getUser(userName);
            return true;
        }
        catch (DatabaseException e)
        {
            return false;
        }
    }

    public DAOControl getM_DAOControl()
    {
        return m_DAOControl;
    }

    public void setM_DAOControl(DAOControl m_DAOControl) {
        this.m_DAOControl = m_DAOControl;
    }
}
