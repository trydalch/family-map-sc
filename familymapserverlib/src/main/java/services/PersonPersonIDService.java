package services;

import java.util.logging.Logger;

import commrequests.PersonPersonIDRequest;
import commresults.PersonPersonIDResult;
import daos.DAOControl;
import exceptions.DatabaseException;

import model.*;

/**
 * A service class to handle all requests for a single Person
 */

public class PersonPersonIDService
{
    private DAOControl m_daoControl = new DAOControl();
    private AuthTokenValidatorService authTokenValidatorService = new AuthTokenValidatorService();

    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }
    /**
     * The PersonPersonID method to search for a specific person and return their information.
     * <pre>
     *     <b>Constraints on the input</b>
     *          Must be valid PersonPersonIDRequest object
     *     <b>Result</b>:
     *     PersonPersonIDResult obj
     * </pre>
     * @param request the request made by the PersonPersonIDHandler, a PersonPersonIDRequest obj
     * @return a PersonPersonIDResult object
     */
    public PersonPersonIDResult personPersonID(PersonPersonIDRequest request)
    {
        logger.entering("PersonPersonIDService" , "personPersonID");
        PersonPersonIDResult result = new PersonPersonIDResult();

        String error = null;
        try {
            if (validAuthToken(request.getAuthToken())) {

                m_daoControl.openConnection();
                AuthToken authToken = m_daoControl.getAuthToken(request.getAuthToken());
                User usr = m_daoControl.getUserByID(authToken.getPersId());

                Person pers = m_daoControl.getPerson(request.getPersonID());
                if (!pers.getDescendant().equals(usr.getUserName()))
                {
                    throw new DatabaseException("Requested person does not belong to User");
                }
                result.setDescendant(pers.getDescendant());
                result.setPersonID(pers.getPersonID());
                result.setFirstName(pers.getFirstName());
                result.setLastName(pers.getLastName());
                result.setGender(pers.getGender());
                result.setFather(pers.getFather());
                result.setMother(pers.getMother());
                result.setSpouse(pers.getSpouse());

            }
            else
            {
                error = "Authorization Token has expired";
            }
        }
        catch (DatabaseException e)
        {
            error = e.getMessage();
        }
        finally
        {
            result.setMessage(error);
            m_daoControl.closeConnection(false);
        }
        logger.exiting("PersonPersonIDService" , "personPersonID");

        return result;
    }

    private boolean validAuthToken(String sAuthToken) throws DatabaseException
    {
        boolean isValid = false;
        try
        {
            m_daoControl.openConnection();
            AuthToken auth = m_daoControl.getAuthToken(sAuthToken);

            if (authTokenValidatorService.checkAuthTokenTimeOut(auth, m_daoControl))
            {
                isValid = true;
            }
            else
            {
                isValid = false;
            }
        }
        catch (DatabaseException e)
        {
            throw e;
        }
        finally
        {
            m_daoControl.closeConnection(false);
        }
        return isValid;

    }
}
