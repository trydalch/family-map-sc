package services;

import java.util.logging.Logger;

import commrequests.EventEventIDRequest;
import commresults.EventEventIDResult;
import daos.DAOControl;
import exceptions.DatabaseException;
import model.*;

/**
 * A service class to search for a specified Event by id that returns that Event object.
 */
public class EventEventIDService
{
    private static Logger logger;
    static
    {
        logger = Logger.getLogger("fmslogger");
    }

    private DAOControl m_daoControl = new DAOControl();
    private AuthTokenValidatorService authTokenValidatorService = new AuthTokenValidatorService();


    /**
     * The eventEventID method proecesses an EventEventIDRequest containing a specified EventID and returns the matching Event object wrapped in an EventEventIDResult object.
     * <pre>
     *     <b>Constraints on the input</b>
     *          Must be valid EventEventIDRequest object
     *     <b>Result</b>:
     *     EventEventIDResult obj
     * </pre>
     * @param request the request made by the EventEventIDHandler, a EventEventIDRequest obj
     * @return an EventEventIDResult object
     */
    public EventEventIDResult eventEventID(EventEventIDRequest request)
    {
        logger.entering("EventEventIDService", "eventEventID");
        EventEventIDResult result = new EventEventIDResult();
        try
        {
            // Open a connection to the database;
            m_daoControl.openConnection();

            // Get the authorization token object in order to access the UserID and identify which user has requested an event.
            AuthToken authToken = m_daoControl.getAuthToken(request.getAuthToken());

            // Use AuthToken to identify user
            User usr = m_daoControl.getUserByID(authToken.getPersId());

            // Is the auth token expired?
            if (authTokenValidatorService.checkAuthTokenTimeOut(authToken, m_daoControl))
            {
                // Auth token is still valid. Get the event requested.
                Event event = m_daoControl.getEvent(request.getEventID());

                // Does the event belong to the user?
                if (usr.getPersonID().equals(event.getPersonID()))
                {
                    // Yes. Send event values back in result object
                    result.setEventID(event.getEventID());
                    result.setDescendant(event.getDescendant());
                    result.setPersonID(event.getPersonID());
                    result.setEventType(event.getEventType());
                    result.setCity(event.getCity());
                    result.setCountry(event.getCountry());
                    result.setLatitude(event.getLatitude());
                    result.setLongitude(event.getLongitude());
                    result.setYear(String.valueOf(event.getYear()));
                }
                else
                {
                    // No, event does not belong to user.
                    result.setMessage("Event does not belong to user");
                }
            }
            else
            {
                // No, auth token has expired.
                result.setMessage("Authorization Token has expired");
            }
        }
        catch (DatabaseException e)
        {
            // There was an error when accessing the database. Could be internal or caused by user. Exception contains
            // an informative error message.
            result.setMessage(e.getMessage());
        }
        finally
        {
            // Close the database connection
            m_daoControl.closeConnection(false);
        }
        logger.exiting("EventEventIDService", "eventEventID");
        return result;
    }
}
