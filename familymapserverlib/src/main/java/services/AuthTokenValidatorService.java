package services;

import daos.DAOControl;
import exceptions.DatabaseException;
import model.AuthToken;
import webserver.WebServer;

/**
 * Class used to check if an authorization token is valid. It must be passed an AuthToken object with
 * all fields filled, as well as a DAOcontrol object with an open connection.
 */

public class AuthTokenValidatorService
{
    final int millisecondsPerSecond = 1000;

    public boolean checkAuthTokenTimeOut(AuthToken authToken, DAOControl m_daoControl)
    {
        // Convert static seconds value to milliseconds.
        int timeoutMilliseconds = WebServer.AuthTokenTimeoutSeconds * millisecondsPerSecond;

        // Calculate difference between the time the token was created and the current time.
        int difference = (int)Math.abs(authToken.getTimeCreated() - System.currentTimeMillis());

        // Is the difference larger than the time an Authorization Token is considered valid?
        if (difference > timeoutMilliseconds)
        {
            // The authorization token has expired. User must login to get another.
            boolean deleteSuccess = false;
            try {
                deleteSuccess = m_daoControl.deleteAuthToken(authToken);
            }
            catch (DatabaseException e)
            {
                e.printStackTrace();
            }

            return false;
        }
        else
        {
            // The authorization token is still valid
            return true;
        }
    }
}
