package dao;

import org.junit.*;
import static org.junit.Assert.*;

import java.io.File;


import daos.DAOControl;
import exceptions.DatabaseException;
import model.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestDAOControl
{
    private DAOControl m_daoControl;
    private String m_dbName = "data" + File.separator + "test.sqlite";
    private User m_TestUsr = new User("testUSR", "passwd", "trevor.rydalch@outlook.com", "Trevor", "Rydalch", "m", "testID");

    @Before
    public void setUp()
    {
        m_daoControl = new DAOControl();
        m_daoControl.setDbName(m_dbName);
        try
        {
            m_daoControl.openConnection();
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
    }

    @After
    public void cleanUp()
    {
        m_daoControl.closeConnection(false);
    }

    @Test
    public void testAddValidUser()
    {
        User usr = new User("testUSR", "passwd", "trevor.rydalch@outlook.com", "Trevor", "Rydalch", "m", "testID");
        boolean addResult = false;
        try
        {
            addResult = m_daoControl.addUser(m_TestUsr);
//            m_daoControl.closeConnection(false);
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        assertEquals(true, addResult);
    }

    @Test
    public void testGetUser()
    {
        String usrName = "testUSR";
        String password = "passwd";
        User usrFromDB = new User();
        boolean addResult = false;
        try
        {
            addResult = m_daoControl.addUser(m_TestUsr);
            usrFromDB = m_daoControl.getUser(m_TestUsr.getUserName());

        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        assertEquals(true, addResult);
        assertEquals(m_TestUsr.getPersonID(), usrFromDB.getPersonID());
    }

    @Test
    public void testGetUserById()
    {
        boolean addResult = false;
        User usrFromDB = new User();
        try
        {
            addResult = m_daoControl.addUser(m_TestUsr);
            usrFromDB = m_daoControl.getUserByID(m_TestUsr.getPersonID());
        }
        catch (DatabaseException e)
        {

        }
        assertEquals(true, addResult);
        assertEquals(m_TestUsr.getPersonID(), usrFromDB.getPersonID());
        assertEquals(m_TestUsr.getPassword(), usrFromDB.getPassword());
    }

    @Test
    public void testDeleteUser()
    {
        User usr = new User("testUSR", "passwd", "trevor.rydalch@outlook.com", "Trevor", "Rydalch", "m", "testID");

        boolean addResult = false;
        boolean deleteResult = false;
        try
        {

            addResult = m_daoControl.addUser(m_TestUsr);
            deleteResult = m_daoControl.deleteUser(m_TestUsr);
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        assertEquals(true, addResult);
        assertEquals(true, deleteResult);
    }

    @Test
    public void testAddPerson()
    {
        Person pers = new Person();
        pers.setFirstName("fname");
        pers.setLastName("lname");
        pers.genRandomID();

        boolean addResult = false;

        try
        {
            addResult = m_daoControl.addPerson(pers);
//            m_daoControl.closeConnection(false);
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        assertEquals(true, addResult);
    }

    @Test
    public void testGetPerson()
    {
        Person pers = new Person();
        pers.setFirstName("fname");
        pers.setLastName("lname");
        pers.genRandomID();

        boolean addResult = false;

        Person persFromDB = new Person();

        try
        {
            addResult = m_daoControl.addPerson(pers);
            persFromDB = m_daoControl.getPerson(pers.getPersonID());
        }
        catch (DatabaseException e) {
            e.printStackTrace();
        }
        assertEquals(pers.getPersonID(), persFromDB.getPersonID());
    }

    @Test
    public void testGetPersonsByUserName()
    {

    }

    @Test
    public void testAddAuthToken()
    {
        AuthToken authToken = new AuthToken();
        authToken.genRandomAuthToken();
        authToken.setPersId("testID");
        boolean addResult = false;
        try
        {
            addResult = m_daoControl.addAuthToken(authToken);
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        assertEquals(true, addResult);
    }

    @Test
    public void testGetAuthToken()
    {
        boolean addResult = false;
        AuthToken fromDB = new AuthToken();
        AuthToken authToken = new AuthToken();
        authToken.setPersId("test");
        authToken.genRandomAuthToken();
        try
        {
            addResult = m_daoControl.addAuthToken(authToken);
            fromDB = m_daoControl.getAuthToken(authToken.getAuthId());
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        assertEquals(true, addResult);
        assertEquals("test", fromDB.getPersId());
        assertEquals(authToken.getAuthId(), fromDB.getAuthId());
        assertEquals(authToken.getTimeCreated(), fromDB.getTimeCreated());
    }

    @Test
    public void testDeleteAuthToken()
    {
        boolean deleteSuccess = false;
        boolean addSuccess = false;
        try
        {
            AuthToken auth = new AuthToken();
            auth.genRandomAuthToken();
            auth.setPersId("test");
            addSuccess = m_daoControl.addAuthToken(auth);
            deleteSuccess = m_daoControl.deleteAuthToken(auth);
        }
        catch (DatabaseException e)
        {

        }
        assertEquals(true, addSuccess);
        assertEquals(true, deleteSuccess);
    }

    @Test
    public void testAddEvent()
    {
        boolean addSuccess = false;
        try
        {
            Event event = new Event();
            event.genRandomID();
            event.setYear(2017);
            event.setEventType("Birth");
            event.setCity("Provo");
            event.setCountry("USA");
            event.setLatitude(40.234);
            event.setLongitude(111.659);
            event.setDescendant("alice");
            event.setPersonID("alice");

            addSuccess = m_daoControl.addEvent(event);
        }
        catch (DatabaseException e)
        {

        }
        assertEquals(true, addSuccess);
    }

    @Test
    public void testGetEvent()
    {
        Event fromDB = new Event();
        Event event = new Event();
        boolean addSuccess = false;
        try
        {
            event.genRandomID();
            event.setYear(2017);
            event.setEventType("Birth");
            event.setCity("Provo");
            event.setCountry("USA");
            event.setLatitude(40.234);
            event.setLongitude(111.659);
            event.setDescendant("alice");
            event.setPersonID("alice");

            addSuccess = m_daoControl.addEvent(event);
            m_daoControl.closeConnection(true);
            m_daoControl.openConnection();
            fromDB = m_daoControl.getEvent(event.getEventID());
            m_daoControl.deleteEventByEventID(event.getEventID());
            m_daoControl.closeConnection(true);
            m_daoControl.openConnection();
        }
        catch (DatabaseException e)
        {

        }
        assertEquals(true, addSuccess);
        assertEquals(event.getEventID(), fromDB.getEventID());
        assertEquals(event.getPersonID(), fromDB.getPersonID());
        assertEquals(event.getEventType(), fromDB.getEventType());
    }

//     @Test
//     public void testGetEventsByPersId()
    {

    }

//    @Test
//    public void deleteEventByEventID()
    {

    }

//    @Test
//    public void testClear()
    {

    }
}
