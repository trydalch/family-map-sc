package dao;

import org.junit.*;

import daos.DAOUser;
import exceptions.DatabaseException;
import model.User;

import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestDAOUser
{
    private DAOUser daoUser;


    @Before
    public void setUp()
    {
        daoUser = new DAOUser();
    }

//    @Test
    public void testAddValidUser()
    {
        User usr = new User("trydalch", "passwd", "trevor.rydalch@outlook.com", "Trevor", "Rydalch", "m", "9999");
        boolean addResult = false;
        try
        {
            addResult = daoUser.addUser(usr);
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        assertEquals(true, addResult);
    }
}
