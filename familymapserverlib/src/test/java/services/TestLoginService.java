package services;

import org.junit.*;

import java.io.File;

import commrequests.LoginRequest;
import commresults.LoginResult;
import daos.DAOControl;
import exceptions.DatabaseException;
import model.User;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestLoginService
{
    private LoginService m_loginService = new LoginService();
    private LoginRequest m_request;
    private LoginResult m_result;

    private DAOControl m_daoControl;
    private String m_dbName = "data" + File.separator + "test.sqlite";
    private User m_TestUsr = new User("testUSR", "passwd", "trevor.rydalch@outlook.com", "Trevor", "Rydalch", "m", "testID");

    @Before
    public void setup()
    {
        m_request = new LoginRequest();
        m_result = new LoginResult();
        m_daoControl = new DAOControl();
        m_loginService.setM_DAOControl(m_daoControl);
        m_daoControl.setDbName(m_dbName);
        try
        {
            m_daoControl.openConnection();
            m_daoControl.addUser(m_TestUsr);
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
    }

    @After
    public void cleanUp()
    {
        try
        {
            if (!m_daoControl.isConnectionOpen())
            {
                m_daoControl.openConnection();
            }
            m_daoControl.clear();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        m_daoControl.closeConnection(true);
    }

    @Test
    public void testInvalidUsernameLogin()
    {
        m_request.setUserName("testUsr");
        m_request.setPassword("testPswd");
        m_result = m_loginService.login(m_request);
        assertEquals("Invalid username", m_result.getMessage());
    }

    @Test
    public void testValidLogin()
    {
        m_request.setUserName(m_TestUsr.getUserName());
        m_request.setPassword(m_TestUsr.getPassword());
        m_daoControl.closeConnection(true);
        m_result = m_loginService.login(m_request);

        assertEquals(null, m_result.getMessage());
    }

    @Test
    public void testMissingPassword()
    {
        m_request.setUserName("testUName");
        m_result = m_loginService.login(m_request);
        assertEquals("Please enter both a username and password", m_result.getMessage());
    }

    @Test
    public void testMissingUserName()
    {
        m_request.setPassword(m_TestUsr.getPassword());
        m_result = m_loginService.login(m_request);
        assertEquals("Please enter both a username and password", m_result.getMessage());
    }
}
