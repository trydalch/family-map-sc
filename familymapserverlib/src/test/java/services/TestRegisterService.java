package services;

import com.google.gson.Gson;

import org.junit.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import commrequests.RegisterRequest;
import commresults.RegisterResult;
import daos.DAOControl;
import exceptions.DatabaseException;

import model.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestRegisterService
{
    private RegisterService m_service = new RegisterService();
    private RegisterRequest m_request;
    private RegisterResult m_result;
    private FemaleNameData m_fnames;
    private MaleNameData m_mnames;
    private LastNameData m_lnames;
    private LocationData m_lctns;
    private File m_fFNames = new File("data\\json\\fnames.json");
    private File m_fMNames = new File("data\\json\\mnames.json");
    private File m_fLNames = new File("data\\json\\snames.json");
    private File m_fLctns = new File("data\\json\\locations.json");

    private DAOControl m_daoControl = new DAOControl();
    private String m_dbName = "data" + File.separator + "test.sqlite";
    private User m_TestUsr = new User("testUSR", "passwd", "trevor.rydalch@outlook.com", "Trevor", "Rydalch", "m", "testID");

    @Before
    public void setup() {
        m_request = new RegisterRequest();
        m_result = new RegisterResult();
        m_daoControl = new DAOControl();
        m_daoControl.setDbName(m_dbName);
        m_service.setM_daoControl(m_daoControl);

        Gson gson = new Gson();

        try
        {
            m_fnames =  gson.fromJson(readString(new FileInputStream(m_fFNames)), FemaleNameData.class);
            m_mnames = gson.fromJson(readString(new FileInputStream(m_fMNames)), MaleNameData.class);
            m_lnames = gson.fromJson(readString(new FileInputStream(m_fLNames)), LastNameData.class);
            m_lctns = gson.fromJson(readString(new FileInputStream(m_fLctns)), LocationData.class);

            m_daoControl.openConnection();
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @After
    public void cleanUp() {
        try
        {
            if (!m_daoControl.isConnectionOpen())
            {
                m_daoControl.openConnection();
            }
            m_daoControl.clear();
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        m_daoControl.closeConnection(true);
    }

    @Test
    public void testValidRegister()
    {
        m_request.setUserName(m_TestUsr.getUserName());
        m_request.setPassword(m_TestUsr.getPassword());
        m_request.setEmail(m_TestUsr.getEmail());
        m_request.setGender(m_TestUsr.getGender());
        m_request.setFirstName(m_TestUsr.getFirstName());
        m_request.setLastName(m_TestUsr.getLastName());
        m_request.setFNames(m_fnames);
        m_request.setLNames(m_lnames);
        m_request.setLocations(m_lctns);
        m_request.setMNames(m_mnames);

        m_result = m_service.register(m_request);

        assertEquals(null, m_result.getMessage());
        assertNotEquals(null, m_result.getAuthToken());
        assertNotEquals(null, m_result.getUserName());
        assertNotEquals(null, m_result.getPersonId());
    }

    @Test
    public void testMissingUserName()
    {
        m_request.setPassword(m_TestUsr.getPassword());
        m_request.setEmail(m_TestUsr.getEmail());
        m_request.setGender(m_TestUsr.getGender());
        m_request.setFirstName(m_TestUsr.getFirstName());
        m_request.setLastName(m_TestUsr.getLastName());
        m_request.setFNames(m_fnames);
        m_request.setLNames(m_lnames);
        m_request.setLocations(m_lctns);
        m_request.setMNames(m_mnames);

        m_result = m_service.register(m_request);

        assertEquals("Please enter all required fields.", m_result.getMessage());
    }

    @Test
    public void testMissingPassword()
    {
        m_request.setUserName(m_TestUsr.getUserName());
        m_request.setEmail(m_TestUsr.getEmail());
        m_request.setGender(m_TestUsr.getGender());
        m_request.setFirstName(m_TestUsr.getFirstName());
        m_request.setLastName(m_TestUsr.getLastName());
        m_request.setFNames(m_fnames);
        m_request.setLNames(m_lnames);
        m_request.setLocations(m_lctns);
        m_request.setMNames(m_mnames);

        m_result = m_service.register(m_request);
        assertEquals("Please enter all required fields.", m_result.getMessage());

    }

    @Test
    public void testMissingGender()
    {
        m_request.setUserName(m_TestUsr.getUserName());
        m_request.setPassword(m_TestUsr.getPassword());
        m_request.setEmail(m_TestUsr.getEmail());
        m_request.setFirstName(m_TestUsr.getFirstName());
        m_request.setLastName(m_TestUsr.getLastName());
        m_request.setFNames(m_fnames);
        m_request.setLNames(m_lnames);
        m_request.setLocations(m_lctns);
        m_request.setMNames(m_mnames);

        m_result = m_service.register(m_request);
        assertEquals(null, m_result.getMessage());
        assertNotEquals(null, m_result.getAuthToken());
        assertNotEquals(null, m_result.getUserName());
        assertNotEquals(null, m_result.getPersonId());
    }

    @Test
    public void testUserNameTaken()
    {
        try
        {
            m_daoControl.addUser(m_TestUsr);
            m_daoControl.closeConnection(true);
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        m_request.setUserName(m_TestUsr.getUserName());
        m_request.setPassword(m_TestUsr.getPassword());
        m_request.setEmail(m_TestUsr.getEmail());
        m_request.setGender(m_TestUsr.getGender());
        m_request.setFirstName(m_TestUsr.getFirstName());
        m_request.setLastName(m_TestUsr.getLastName());
        m_request.setFNames(m_fnames);
        m_request.setLNames(m_lnames);
        m_request.setLocations(m_lctns);
        m_request.setMNames(m_mnames);

        m_result = m_service.register(m_request);

        assertEquals("Username is taken", m_result.getMessage());
    }

    private String readString(InputStream is) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0)
        {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }
}
