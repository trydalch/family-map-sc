package services;

import com.google.gson.Gson;

import org.junit.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import commrequests.FillRequest;
import commresults.FillResult;
import daos.DAOControl;
import exceptions.DatabaseException;
import model.FemaleNameData;
import model.LastNameData;
import model.LocationData;
import model.MaleNameData;
import model.User;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestFillService
{
    FillRequest m_request;
    FillResult m_result;
    FillService m_service = new FillService();

    private FemaleNameData m_fnames;
    private MaleNameData m_mnames;
    private LastNameData m_lnames;
    private LocationData m_lctns;
    private File m_fFNames = new File("data\\json\\fnames.json");
    private File m_fMNames = new File("data\\json\\mnames.json");
    private File m_fLNames = new File("data\\json\\snames.json");
    private File m_fLctns = new File("data\\json\\locations.json");
    private File m_fEvntType = new File("data\\json\\eventTypes.json");

    private DAOControl m_daoControl = new DAOControl();
    private String m_dbName = "data" + File.separator + "test.sqlite";
    private User m_TestUsr = new User("testUSR", "passwd", "trevor.rydalch@outlook.com", "Trevor", "Rydalch", "m", "testID");

    @Before
    public void setup() {
        m_request = new FillRequest();
        m_result = new FillResult();
        m_daoControl = new DAOControl();
        m_daoControl.setDbName(m_dbName);
        m_service.setM_daoControl(m_daoControl);

        Gson gson = new Gson();

        try
        {
            m_fnames =  gson.fromJson(readString(new FileInputStream(m_fFNames)), FemaleNameData.class);
            m_mnames = gson.fromJson(readString(new FileInputStream(m_fMNames)), MaleNameData.class);
            m_lnames = gson.fromJson(readString(new FileInputStream(m_fLNames)), LastNameData.class);
            m_lctns = gson.fromJson(readString(new FileInputStream(m_fLctns)), LocationData.class);

            m_daoControl.openConnection();
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @After
    public void cleanUp() {
        try
        {
            if (!m_daoControl.isConnectionOpen())
            {
                m_daoControl.openConnection();
            }
            m_daoControl.clear();
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        m_daoControl.closeConnection(true);
    }

    @Test
    public void testValidFillRequestDefaultGenerations()
    {
        addTestUser();
        m_request.setUserName(m_TestUsr.getUserName());
        m_request.setFNames(m_fnames);
        m_request.setLNames(m_lnames);
        m_request.setLocations(m_lctns);
        m_request.setMNames(m_mnames);

        m_result = m_service.fill(m_request);

        assertEquals("Successfully added 31 persons and 153 events to the database", m_result.getMessage());
    }

    @Test
    public void testValidFillRequestEightGenerations()
    {
        addTestUser();
        m_request.setUserName(m_TestUsr.getUserName());
        m_request.setFNames(m_fnames);
        m_request.setLNames(m_lnames);
        m_request.setLocations(m_lctns);
        m_request.setMNames(m_mnames);
        m_request.setGenerations(8);
        m_result = m_service.fill(m_request);

        assertEquals("Successfully added 511 persons and 2553 events to the database", m_result.getMessage());
    }

    @Test
    public void testMissingUserName()
    {
        addTestUser();
        m_request.setFNames(m_fnames);
        m_request.setLNames(m_lnames);
        m_request.setLocations(m_lctns);
        m_request.setMNames(m_mnames);
        m_request.setGenerations(8);
        m_result = m_service.fill(m_request);

        assertEquals("Invalid Username", m_result.getMessage());
    }

    @Test
    public void testInvalidUserName()
    {
        m_request.setFNames(m_fnames);
        m_request.setLNames(m_lnames);
        m_request.setLocations(m_lctns);
        m_request.setMNames(m_mnames);
        m_request.setGenerations(8);
        m_result = m_service.fill(m_request);

        assertEquals("Invalid Username", m_result.getMessage());
    }

    private String readString(InputStream is) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0)
        {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    private void addTestUser()
    {
        try
        {
            m_daoControl.addUser(m_TestUsr);
            m_daoControl.closeConnection(true);
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
    }
}
