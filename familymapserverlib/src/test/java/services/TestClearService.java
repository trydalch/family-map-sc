package services;

import com.google.gson.Gson;

import org.junit.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import daos.DAOControl;
import exceptions.DatabaseException;
import model.*;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestClearService
{
    private ClearService m_ClearService = new ClearService();
    private ClearRequest m_ClearRequest = new ClearRequest();
    private ClearResult m_ClearResult = new ClearResult();
    private RegisterService m_RegService = new RegisterService();
    private RegisterRequest m_RegisterRequest;
    private RegisterResult m_RegisterResult;
    private FemaleNameData m_fnames;
    private MaleNameData m_mnames;
    private LastNameData m_lnames;
    private LocationData m_lctns;
    private File m_fFNames = new File("data\\json\\fnames.json");
    private File m_fMNames = new File("data\\json\\mnames.json");
    private File m_fLNames = new File("data\\json\\snames.json");
    private File m_fLctns = new File("data\\json\\locations.json");

    private DAOControl m_daoControl = new DAOControl();
    private String m_dbName = "data" + File.separator + "test.sqlite";
    private User m_TestUsr = new User("testUSR", "passwd", "trevor.rydalch@outlook.com", "Trevor", "Rydalch", "m", "testID");

    @Before
    public void setup() {
        m_RegisterRequest = new RegisterRequest();
        m_RegisterResult = new RegisterResult();
        m_ClearRequest = new ClearRequest();
        m_ClearResult = new ClearResult();
        m_daoControl = new DAOControl();
        m_daoControl.setDbName(m_dbName);
        m_ClearService.setM_daoControl(m_daoControl);
        m_RegService.setM_daoControl(m_daoControl);

        Gson gson = new Gson();

        try
        {
            m_fnames =  gson.fromJson(readString(new FileInputStream(m_fFNames)), FemaleNameData.class);
            m_mnames = gson.fromJson(readString(new FileInputStream(m_fMNames)), MaleNameData.class);
            m_lnames = gson.fromJson(readString(new FileInputStream(m_fLNames)), LastNameData.class);
            m_lctns = gson.fromJson(readString(new FileInputStream(m_fLctns)), LocationData.class);

            m_daoControl.openConnection();
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @After
    public void cleanUp() {
        try
        {
            if (!m_daoControl.isConnectionOpen())
            {
                m_daoControl.openConnection();
            }
            m_daoControl.clear();
        }
        catch (DatabaseException e)
        {
            e.printStackTrace();
        }
        m_daoControl.closeConnection(true);
    }

    @Test
    public void testClearEmptyDataBase()
    {
        m_ClearResult = m_ClearService.clear(m_ClearRequest);
        assertEquals("Clear succeeded", m_ClearResult.getMessage());
    }

    @Test
    public void testClearDataBaseWithData()
    {
        m_RegisterRequest.setUserName(m_TestUsr.getUserName());
        m_RegisterRequest.setPassword(m_TestUsr.getPassword());
        m_RegisterRequest.setEmail(m_TestUsr.getEmail());
        m_RegisterRequest.setGender(m_TestUsr.getGender());
        m_RegisterRequest.setFirstName(m_TestUsr.getFirstName());
        m_RegisterRequest.setLastName(m_TestUsr.getLastName());
        m_RegisterRequest.setFNames(m_fnames);
        m_RegisterRequest.setLNames(m_lnames);
        m_RegisterRequest.setLocations(m_lctns);
        m_RegisterRequest.setMNames(m_mnames);

        m_RegisterResult = m_RegService.register(m_RegisterRequest);

        assertEquals(null, m_RegisterResult.getMessage());
        assertNotEquals(null, m_RegisterResult.getAuthToken());
        assertNotEquals(null, m_RegisterResult.getUserName());
        assertNotEquals(null, m_RegisterResult.getPersonId());

        m_ClearResult = m_ClearService.clear(m_ClearRequest);
        assertEquals("Clear succeeded", m_ClearResult.getMessage());
    }


    private String readString(InputStream is) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0)
        {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }
}
