package commrequests;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestPersonPersonIDRequest
{
    private PersonPersonIDRequest m_request;

    @Before
    public void setUp()
    {
        m_request = new PersonPersonIDRequest();
    }

    @Test
    public void testSetGetAuthToken()
    {
        String expected = "auth";
        m_request.setAuthToken(expected);
        String actual = m_request.getAuthToken();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetEventID()
    {
        String expected = "myID";
        m_request.setPersonID(expected);
        String actual = m_request.getPersonID();
        assertEquals(expected, actual);
    }
}
