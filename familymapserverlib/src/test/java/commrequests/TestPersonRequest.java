package commrequests;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestPersonRequest
{
    private PersonRequest m_request;

    @Before
    public void setUp()
    {
        m_request = new PersonRequest();
    }

    @Test
    public void testSetGetAuthTokenString()
    {
        String expected = "auth";
        m_request.setAuthToken(expected);
        String actual = m_request.getAuthToken();
        assertEquals(expected, actual);
    }
}
