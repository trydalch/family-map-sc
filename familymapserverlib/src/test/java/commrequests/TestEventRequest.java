package commrequests;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestEventRequest
{
    private EventRequest m_request;

    @Before
    public void setUp()
    {
        m_request = new EventRequest();
    }

    @Test
    public void testSetGetAuthTokenString()
    {
        String expected = "auth";
        m_request.setAuthToken(expected);
        String actual = m_request.getAuthToken();
        assertEquals(expected, actual);
    }
}
