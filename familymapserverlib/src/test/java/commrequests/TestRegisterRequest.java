package commrequests;

import org.junit.*;
import static org.junit.Assert.*;

import model.FemaleNameData;
import model.LastNameData;
import model.MaleNameData;


/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestRegisterRequest {
    private RegisterRequest m_request;

    @Before
    public void setUp() {
        m_request = new RegisterRequest();
    }

    @Test
    public void testSetGetUserName() {
        String expected = "trevor";
        m_request.setUserName(expected);
        String actual = m_request.getUserName();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetPassword()
    {
        String expected = "pword";
        m_request.setPassword(expected);
        String actual = m_request.getPassword();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetFirstName()
    {
        String expected = "trevor";
        m_request.setFirstName(expected);
        String actual = m_request.getFirstName();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetLastName()
    {
        String expected = "rydalch";
        m_request.setLastName(expected);
        String actual = m_request.getLastName();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetEmail()
    {
        String expected = "email@domain.com";
        m_request.setEmail(expected);
        String actual = m_request.getEmail();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetGender()
    {
        String expected = "m";
        m_request.setGender(expected);
        String actual = m_request.getGender();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetFemaleNameData()
    {
        FemaleNameData expected = new FemaleNameData();
        String[] data = new String[10];
        data[0] = "alice";
        data[1] = "trevor";
        data[3] = "lexi";
        expected.setData(data);
        m_request.setFNames(expected);
        FemaleNameData actual = m_request.getFNames();
        assertEquals(expected.getData().length, actual.getData().length);
    }

    @Test
    public void testSetGetMaleNameData()
    {
        MaleNameData expected = new MaleNameData();
        String[] data = {"alice", "lexi", "trevor"};
        expected.setData(data);
        m_request.setMNames(expected);
        MaleNameData actual = m_request.getMNames();
        assertEquals(expected.getData().length, actual.getData().length);
    }

    @Test
    public void testSetGetLastNameData()
    {
        LastNameData expected = new LastNameData();
        String[] data = {"rydalch", "eaton", "lind", "cluff"};
        expected.setData(data);
        m_request.setLNames(expected);
        LastNameData actual = m_request.getLNames();
        assertEquals(expected.getData().length, actual.getData().length);
    }
}
