package commrequests;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestLoginRequest
{
    private LoginRequest m_request = new LoginRequest();

    @Test
    public void testSetGetUserName()
    {
        String expected = "alice";
        m_request.setUserName(expected);
        String actual = m_request.getUserName();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetPassword()
    {
        String expected = "alice";
        m_request.setPassword(expected);
        String actual = m_request.getPassword();
        assertEquals(expected, actual);
    }
}
