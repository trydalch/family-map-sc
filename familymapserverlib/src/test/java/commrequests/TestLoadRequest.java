package commrequests;

import org.junit.*;

import model.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestLoadRequest
{
    private User[] m_users = new User[10];
    private Person[] m_persons = new Person[10];
    private Event[] m_events = new Event[10];

    private LoadRequest m_request;

    @Before
    public void setUp()
    {
        m_request = new LoadRequest();
    }


    @Test
    public void testSetGetUsers()
    {
        User usr = new User("trevor", "pword", "email@domain.com", "fname", "lname", "m", "myId");
        m_users[0] = usr;
        m_request.setUsers(m_users);
        assertEquals(m_users.length, m_request.getUsers().length);
    }

    @Test
    public void testSetGetPersons()
    {
        Person pers = new Person("myId", "fname", "lname");
        m_persons[0] = pers;
        m_request.setPersons(m_persons);
        assertEquals(m_persons.length, m_request.getPersons().length);
    }

    @Test
    public void testSetGetEvents()
    {
        Event event = new Event();
        event.setEventID("myEventID");
        m_events[0] = event;
        m_request.setEvents(m_events);
        assertEquals(m_events.length, m_request.getEvents().length);
    }

}
