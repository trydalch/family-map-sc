package commrequests;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestEventEventIDRequest
{
    private EventEventIDRequest m_request;

    @Before
    public void setUp()
    {
        m_request = new EventEventIDRequest();
    }

    @Test
    public void testSetGetAuthToken()
    {
        String expected = "auth";
        m_request.setAuthToken(expected);
        String actual = m_request.getAuthToken();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetEventID()
    {
        String expected = "myID";
        m_request.setEventID(expected);
        String actual = m_request.getEventID();
        assertEquals(expected, actual);
    }
}
