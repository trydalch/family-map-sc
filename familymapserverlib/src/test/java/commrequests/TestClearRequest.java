package commrequests;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestClearRequest
{
    private ClearRequest m_request = new ClearRequest();


    @Test
    public void testSetGetUserName()
    {
        String expected = "alice";
        m_request.setUsrName(expected);
        String actual = m_request.getUsrName();
        assertEquals(expected, actual);
    }
}
