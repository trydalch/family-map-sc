package commresults;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestPersonPersonIDResult
{
    private PersonPersonIDResult m_result;

    @Before
    public void setUp()
    {
        m_result = new PersonPersonIDResult();
    }

    @Test
    public void testSetGetDescendant()
    {
        String exp = "desc";
        m_result.setDescendant(exp);
        String act = m_result.getDescendant();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetPersID()
    {
        String exp = "desc";
        m_result.setPersonID(exp);
        String act = m_result.getPersonID();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetFirstName()
    {
        String exp = "desc";
        m_result.setFirstName(exp);
        String act = m_result.getFirstName();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetLastName()
    {
        String exp = "desc";
        m_result.setLastName(exp);
        String act = m_result.getLastName();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetFather()
    {
        String exp = "desc";
        m_result.setFather(exp);
        String act = m_result.getFather();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetMother()
    {
        String exp = "desc";
        m_result.setMother(exp);
        String act = m_result.getMother();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetSpouse()
    {
        String exp = "desc";
        m_result.setSpouse(exp);
        String act = m_result.getSpouse();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetGender()
    {
        String exp = "desc";
        m_result.setGender(exp);
        String act = m_result.getGender();
        assertEquals(exp, act);
    }

    @Test
    public void tesSetGetMessage()
    {
        String exp = "desc";
        m_result.setMessage(exp);
        String act = m_result.getMessage();
        assertEquals(exp, act);
    }
}
