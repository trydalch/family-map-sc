package commresults;

import org.junit.Before;
import org.junit.Test;

import model.Event;

import static org.junit.Assert.assertEquals;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestEventResult
{
    private EventResult m_result;

    @Before
    public void setUp()
    {
        m_result = new EventResult();
    }

    @Test
    public void testSetGetData()
    {
        Event[] data = new Event[2];
        Event event = new Event();
        event.setEventID("id");
        data[0] = event;
        m_result.setData(data);
        assertEquals(data.length, m_result.getData().length);
    }

    @Test
    public void testSetGetMessage()
    {
        String exp = "message";
        m_result.setMessage(exp);
        String act = m_result.getMessage();
        assertEquals(exp, act);
    }
}
