package commresults;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestEventEventIDResult
{
    private EventEventIDResult m_result;

    @Before
    public void setUp()
    {
        m_result = new EventEventIDResult();
    }

    @Test
    public void testSetGetDescendant()
    {
        String exp = "message";
        m_result.setDescendant(exp);
        String act = m_result.getDescendant();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetEventID()
    {
        String exp = "id";
        m_result.setEventID(exp);
        String act = m_result.getEventID();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetPersID()
    {
        String exp = "id";
        m_result.setPersonID(exp);
        String act = m_result.getPersonID();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetLatitude()
    {
        double exp = 100.11;
        m_result.setLatitude(exp);
        double act = m_result.getLatitude();
        assertEquals(exp, act, 0.5);
    }

    @Test
    public void testSetGetLongitude()
    {
        double exp = 100.11;
        m_result.setLongitude(exp);
        double act = m_result.getLongitude();
        assertEquals(exp, act, 0.5);
    }

    @Test
    public void testSetGetCountry()
    {
        String exp = "country";
        m_result.setCountry(exp);
        String act = m_result.getCountry();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetCity()
    {
        String exp = "city";
        m_result.setCity(exp);
        String act = m_result.getCity();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetEventType()
    {
        String exp = "type";
        m_result.setEventType(exp);
        String act = m_result.getEventType();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetYear()
    {
        String exp = "2000";
        m_result.setYear(exp);
        String act = m_result.getYear();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetMessage()
    {
        String exp = "message";
        m_result.setMessage(exp);
        String act = m_result.getMessage();
        assertEquals(exp, act);
    }
}
