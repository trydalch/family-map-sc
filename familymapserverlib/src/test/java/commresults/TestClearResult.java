package commresults;

import org.junit.*;

import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestClearResult
{
    private ClearResult m_result;

    @Before
    public void setUp()
    {
        m_result = new ClearResult();
    }

    @Test
    public void testSetGetMessage()
    {
        String exp = "auth";
        m_result.setMessage(exp);
        String act = m_result.getMessage();
        assertEquals(exp, act);
    }
}
