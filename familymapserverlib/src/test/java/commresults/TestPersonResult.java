package commresults;

import org.junit.*;
import static org.junit.Assert.*;

import model.Person;



/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestPersonResult
{
    private PersonResult m_result;

    @Before
    public void setUp()
    {
        m_result = new PersonResult();
    }

    @Test
    public void testSetGetData()
    {
        Person[] data = new Person[2];
        Person p = new Person("id", "fname", "lname");
        data[0] = p;
        m_result.setData(data);
        assertEquals(data.length, m_result.getData().length);
    }

    @Test
    public void testSetGetMessage()
    {
        String exp = "message";
        m_result.setMessage(exp);
        String act = m_result.getMessage();
        assertEquals(exp, act);
    }

}
