package commresults;

import org.junit.*;

import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestFillResult
{
    private FillResult m_result;

    @Before
    public void setUp()
    {
        m_result = new FillResult();
    }

    @Test
    public void testSetGetMessage()
    {
        String exp = "auth";
        m_result.setMessage(exp);
        String act = m_result.getMessage();
        assertEquals(exp, act);
    }
}
