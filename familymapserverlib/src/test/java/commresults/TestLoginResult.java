package commresults;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestLoginResult
{
    private LoginResult m_result;

    @Before
    public void setUp()
    {
        m_result = new LoginResult();
    }

    @Test
    public void testSetGetAuthTokenString()
    {
        String exp = "auth";
        m_result.setAuthToken(exp);
        String act = m_result.getAuthToken();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetUserName()
    {
        String exp = "auth";
        m_result.setUserName(exp);
        String act = m_result.getUserName();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetPersId()
    {
        String exp = "auth";
        m_result.setPersonId(exp);
        String act = m_result.getPersonId();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetMessage()
    {
        String exp = "auth";
        m_result.setMessage(exp);
        String act = m_result.getMessage();
        assertEquals(exp, act);
    }
}
