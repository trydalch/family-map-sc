package commresults;

import org.junit.*;

import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestRegisterResult
{
    private RegisterResult m_result;

    @Before
    public void setUp()
    {
        m_result = new RegisterResult();
    }

    @Test
    public void testSetGetAuthToken()
    {
        String exp = "auth";
        m_result.setAuthToken(exp);
        String act = m_result.getAuthToken();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetUserName()
    {
        String exp = "usrname";
        m_result.setUserName(exp);
        String act = m_result.getUserName();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetPersonId()
    {
        String exp = "id";
        m_result.setPersonId(exp);
        String act = m_result.getPersonId();
        assertEquals(exp, act);
    }

    @Test
    public void testSetGetMessage()
    {
        String exp = "message";
        m_result.setMessage(exp);
        String act = m_result.getMessage();
        assertEquals(exp, act);
    }
}
