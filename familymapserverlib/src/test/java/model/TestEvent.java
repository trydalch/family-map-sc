package model;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestEvent
{
    private Event event;

    @Before
    public void SetUp()
    {
        event = new Event();
    }

    @Test
    public void testSetGetEventID()
    {
        String expected = "id";
        event.setEventID(expected);
        String actual = event.getEventID();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetDescendant()
    {
        String expected = "trydalch";
        event.setDescendant(expected);
        String actual = event.getDescendant();
        assertEquals(expected, actual);
    }
    @Test
    public void testSetGetPersonID()
    {
        String expected = "id";
        event.setPersonID(expected);
        String actual = event.getPersonID();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetLatitude()
    {
        double expected = 1.0;
        event.setLatitude(expected);
        double actual = event.getLatitude();
        assertEquals(expected, actual, 0.5);
    }

    @Test
    public void testSetGetLongitude()
    {
        double expected = 1.0;
        event.setLongitude(expected);
        double actual = event.getLongitude();
        assertEquals(expected, actual, 0.5);
    }

    @Test
    public void testSetGetCountry()
    {
        String expected = "test";
        event.setCountry(expected);
        String actual = event.getCountry();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetCity()
    {
        String expected = "test";
        event.setCity(expected);
        String actual = event.getCity();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetEventType()
    {
        String expected = "test";
        event.setEventType(expected);
        String actual = event.getEventType();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetYear()
    {
        int expected = 0;
        event.setYear(expected);
        int actual = event.getYear();
        assertEquals(expected, actual);
    }

    @Test
    public void testGenRandomID()
    {
        String initial = "test";
        event.setEventID(initial);
        event.genRandomID();
        assertNotEquals(initial, event.getEventID());
    }
}
