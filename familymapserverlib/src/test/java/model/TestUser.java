package model;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestUser
{
    private User user;

    @Before
    public void setUp()
    {
        user = new User();
    }

    @Test
    public void testSetGetUserName()
    {
        String expected = "trydalch";
        user.setUserName(expected);
        String actual = user.getUserName();
        assertEquals(expected, actual);
    }

    @Test
    public void testNullGetUsername()
    {
        String expected = null;
        String actual = user.getUserName();
        assertEquals(expected, actual);
    }

    @Test
    public void testFirstConstructor()
    {
        String expUserName = "trydalch";
        String expPass = "passwd";
        String expEmail = "trevor.rydalch@outlook.com";
        String expFName = "Trevor";
        String expLName = "Rydalch";
        user = new User(expUserName, expPass, expEmail, expFName, expLName);
        assertEquals(expUserName, user.getUserName());
        assertEquals(expPass, user.getPassword());
        assertEquals(expEmail, user.getEmail());
        assertEquals(expFName, user.getFirstName());
        assertEquals(expLName, user.getLastName());
    }

    @Test
    public void testSecondContsructor()
    {
        String expUserName = "trydalch";
        String expPass = "passwd";
        String expEmail = "trevor.rydalch@outlook.com";
        String expFName = "Trevor";
        String expLName = "Rydalch";
        String expGender = "m";
        user = new User(expUserName, expPass, expEmail, expFName, expLName, expGender);
        assertEquals(expUserName, user.getUserName());
        assertEquals(expPass, user.getPassword());
        assertEquals(expEmail, user.getEmail());
        assertEquals(expFName, user.getFirstName());
        assertEquals(expLName, user.getLastName());
        assertEquals(expGender, user.getGender());
    }


    @Test
    public void testSetPassword()
    {
        String expected = "password";
        user.setPassword(expected);
        String actual = user.getPassword();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetNullPassword()
    {
        String expected = null;
        String actual = user.getPassword();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetFirstName()
    {
        String expected = "trevor";
        user.setFirstName(expected);
        String actual = user.getFirstName();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetNullFirstName()
    {
        String expected = null;
        String actual = user.getFirstName();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetLastName()
    {
        String expected = "rydalch";
        user.setLastName(expected);
        String actual = user.getLastName();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetNullLastName()
    {
        String expected = null;
        String actual = user.getLastName();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGender()
    {
        String expected = "m";
        user.setGender(expected);
        String actual = user.getGender();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetNullGender()
    {
        String expected = null;
        String actual = user.getGender();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetPersonID()
    {
        String expected = "id";
        user.setPersonID(expected);
        String actual = user.getPersonID();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetNullPersonID()
    {
        String expected = null;
        String actual = user.getPersonID();
        assertEquals(expected, actual);
    }

    @Test
    public void testgenRandomID()
    {
        user.genRandomID();
        String initial = user.getPersonID();
        user.genRandomID();
        String last = user.getPersonID();
        assertNotEquals(initial, last);
    }
}
