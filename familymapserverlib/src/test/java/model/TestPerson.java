package model;

import org.junit.*;
import static org.junit.Assert.*;


/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestPerson
{
    private Person person;
    @Before
    public void setUp()
    {
        person = new Person();
    }

    @Test
    public void testSetGetPersonID()
    {
        String expected = "id";
        person.setPersonID(expected);
        String actual = person.getPersonID();
        assertEquals(expected, actual);
    }

    @Test
    public void testGenPersonID()
    {
        String initial = "id";
        person.setPersonID(initial);
        person.genRandomID();
        assertNotEquals(initial, person.getPersonID());
    }

    @Test
    public void testSetGetDescendant()
    {
        String expected = "trydalch";
        person.setDescendant(expected);
        String actual = person.getDescendant();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetFirstName()
    {
        String expected = "trevor";
        person.setFirstName(expected);
        String actual = person.getFirstName();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetGender()
    {
        String expected = "m";
        person.setGender(expected);
        String actual = person.getGender();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetFather()
    {
        String expected = "mark";
        person.setFather(expected);
        String actual = person.getFather();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetMother()
    {
        String expected = "amy";
        person.setMother(expected);
        String actual = person.getMother();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetSpouse()
    {
        String expected = "lexi";
        person.setSpouse(expected);
        String actual = person.getSpouse();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetGeneration()
    {
        int expected = 0;
        person.setGeneration(expected);
        int actual = person.getGeneration();
        assertEquals(expected, actual);
    }

    @Test
    public void testConstructor()
    {
        String expID = "id";
        String expFName = "trevor";
        String expLName = "rydalch";

        Person p = new Person(expID, expFName, expLName);

        assertEquals(expID, p.getPersonID());
        assertEquals(expFName, p.getFirstName());
        assertEquals(expLName, p.getLastName());
    }
}
