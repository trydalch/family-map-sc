package model;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by Trevor.Rydalch on 5/20/2017.
 */

public class TestAuthToken
{
    private AuthToken authToken;

    @Before
    public void SetUp()
    {
        authToken = new AuthToken();
    }

    @Test
    public void testSetGetPersId()
    {
        String expected = "test";
        authToken.setPersId(expected);
        String actual = authToken.getPersId();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetEventId()
    {
        String expected = "test";
        authToken.setAuthId(expected);
        String actual = authToken.getAuthId();
        assertEquals(expected, actual);
    }

    @Test
    public void testSetGetTimeCreated()
    {
        int expected = (int)System.currentTimeMillis() / 1000;
        authToken.setTimeCreated((long)expected);
        int actual = (int)authToken.getTimeCreated();
        assertEquals(expected, actual);
    }

    @Test
    public void testGenAuthToken()
    {
        String initial = "test";
        authToken.setAuthId(initial);
        authToken.genRandomAuthToken();
        assertNotEquals(initial, authToken.getAuthId());
    }
}
